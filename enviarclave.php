<?
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");

if( isset($_POST['email']) && !empty($_POST['email']) )
{ 
   $email=$_POST['email'];

   // validar si existe el email
   $sql="SELECT `email` FROM `persona_pyme` WHERE `email`='$email'";

   $consulta=mysqli_query(conexion::obtenerInstancia(), $sql);

   if(mysqli_num_rows($consulta) > 0)
   { 

       ////////////*si hay email generar una clave nueva para enviar *///////
       $longitud = 8; // longitud del password  
       $password = substr(rand(),0,$longitud); 
       
       //////////////////*enviar email*//////////////////////////////
        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        $from = "correo@servijus.com.ar";
        $to = $email;
        $subject = "Recuperar Clave - Servijus.com.ar";
        $message ='
            <!DOCTYPE html>
            <html lang="es">
            <head>
                <title>Asesor Online</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link type="text/css" rel="stylesheet" href="css/bootstrap_mat.css">
            </head>
            <body>
            <div class="container">
             <table class="table" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
             <tr>
              <td aling="center" style="color:blue;">
              <h1>SERVIJUS Consultoría Online</h1>
              </td>
             </tr>
             <tr>
             <td bgcolor="#ffffff">
             <p style="font-size: 16px">
            Tu nueva clave de acceso a Asesor Pyme es: </p> <p style="font-size: 18px; color: blue ; "> '.$password.'
            </p>
            <br>
            <p style="font-size: 16px">
            Para mayor seguridad, ingresa a tu cuenta y realiza el cambio de la misma, en tu perfil. Gracias!
            <br>
            <br>
            Saludos cordiales!
            <br> 
            El equipo de Servijus.
            </p>
            <br>
             </td>
             </tr>
             <tr>
             <td >
            <p style="text-align: justify;">
            SERVIJUS, te recuerda que con el fin de resguardar tu seguridad, nunca te solicitaremos datos personales ni datos
            de tu tarjeta de crédito a través de los e-mails que te enviemos. No respondas e-mail que soliciten información
            personal o incluyan links que te deriven a páginas en internet que te lo soliciten. Si deseas no recibir más
            comunicación de nuestros servicios o dar de baja a tu registro, accede desde el link de desuscripción siguiendo los
            pasos que allí te indicamos. La información de este mail es de uso personal y no puede ser compartida ni transmitida
            a terceros, quedando la misma protegida por las normas legales y los Términos y Condiciones que regulan el uso de
            nuestros servicios jurídicos online.
            </p>
            </td>
            </tr>
            </table>
            </div>  
            </body>
            </html>';
            $headers = "From:" . $from;
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            mail($to,$subject,$message, $headers);
            $password_md=md5($password);

            $sql="UPDATE `persona` SET `password`='$password_md' WHERE `email`='$email'";

                    
            $consulta=mysqli_query(conexion::obtenerInstancia(), $sql);
            if($consulta)
            {
                echo "<script language=Javascript> alert('Enviamos a tu cuenta de correo la nueva clave de ingreso a tu Asesor Pyme.'); </script>";
                echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
                  //header('Location: listado.php');
                  exit;
              } 
              else {
                    echo "<script language=Javascript> alert('No se pudo Guardar la información.'); </script>";
                    echo "<script language=Javascript> location.href=\"index.php\"; </script>";
                    exit();
                    }     
       
    }
    else
    { 
      echo "<script language=Javascript> alert('El Email no esta Registrado.'); </script>"; 
      echo "<script language=Javascript> location.href=\"index.php\"; </script>";
      exit();
      
    }// fin validar
  }// fin del if principal  

?>
<div class="acceso espacio gris">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h2 class="mb-4">
					Recuperar mi contraseña
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<hr/>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
				
				<form class="d-block mt-5" action="enviarclave.php" method="POST" >

				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Correo electrónico:</strong></label>
				   
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required autofocus name="parametro_email">

				    
				  </div>

				  <button type="submit" class="boton mt-3">Recuperar contraseña</button>

				</form>
				
			</div>
			<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
				<img src="_img/recuperar-contraseña.jpg" alt="" class="d-block w-100"/>
			</div>
		</div>
	</div>
</div>
<?

include ("pie.html");
?>  


