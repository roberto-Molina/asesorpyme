<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>

<div class="acceso espacio gris">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h2 class="mb-4">
					Recuperar mi contraseña
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<hr/>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
				
				<form class="d-block mt-5" action="validar_email.php" method="POST" >

				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Correo electrónico:</strong></label>
				   
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required autofocus name="parametro_email">

				    
				  </div>

				  <button type="submit" class="boton mt-3">Recuperar contraseña</button>

				</form>
				
			</div>
			<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
				<img src="_img/recuperar-contraseña.jpg" alt="" class="d-block w-100"/>
			</div>
		</div>
	</div>
</div>
<?php
//include("contadortemas.php");
include("pie.html");
?>
