
<?php
 include("sesion.php");
?>
<div class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light ">

  <a href="index.php"><img src="img/LOGO ASESOR PYMES.jpg" alt="Servijus" width="240"></a> 
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

   <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <? 
       if (isset($_SESSION['sesion_usuario']))
       {
         ?> 
         <li class="nav-item ">
          <a href="perfil.php" class="nav-link">
            <i class="fa fa-user"></i>
               <?php echo $_SESSION['sesion_nombre']; ?>
          </a>    
        </li>

        <li class="nav-item ">
          <a href="https://www.servijus.com.ar/centro-de-ayuda/" class="nav-link" target="_blank">
            <i class="fa fa-life-ring"></i>
               Ayuda
          </a>    
        </li>

        <li class="nav-item">
            <a href="salir.php" class="nav-link">
            <i class="fa fa-times"></i>
             Salir
             </a>
        </li>
        <?
          }
          else {
        ?>
         <li class="nav-item">
               <a href="iniciar-sesion.php" class="myButton">INICIAR SESION </a>
          </li>
          <?
          }
        ?>   
    </ul>
  </div>  
</nav>
