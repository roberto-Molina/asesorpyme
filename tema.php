<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");

$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';

//exit();

if( isset($_GET['subcat']) && !empty($_GET['subcat']) )
{   
  $id_cat=htmlspecialchars($_GET['cat'],ENT_QUOTES,'UTF-8');
  
  $id_subcat=htmlspecialchars($_GET['subcat'],ENT_QUOTES,'UTF-8'); 
  
   // registrar estadistica
  $fechahora = date("Y-m-d H:i:s");
  $reg="INSERT INTO `e_subcategoria_pyme`
            (`usuario_id`,`fechahora`,`subcategoria_id`)
            VALUES ('$ID','$fechahora','$id_subcat');";
  $registrado=mysqli_query(conexion::obtenerInstancia(), $reg);
 
 

   $subcategoria="select titulo FROM
                            `subcategoria` where id=$id_subcat";
   $listado=mysqli_query(conexion::obtenerInstancia(), $subcategoria);
   while( $item = mysqli_fetch_assoc($listado))
   {
     $nombre_subcategoria= $item['titulo'];
   }

      //$categorias="SELECT id,titulo from categoria WHERE `id`='$id_cat'";
     $categorias="SELECT
subcategoria.`titulo` AS subcategoria_titulo,
categoria.`titulo` AS categoria_titulo,
categoria.id AS categoria_id
FROM
    `subcategoria`
    INNER JOIN `categoria` 
        ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
        WHERE subcategoria.`id`='$id_subcat'";
        
     $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
	while( $item = mysqli_fetch_assoc($listado))
    {
      $nombre_categoria=$item['categoria_titulo'];
      $id_categoria=$item['categoria_id'];
     
     }
    

?>

<div class="container">
    <div class="row">
        	<div class="row mt-5 breadcrumbs">
		        <div cass="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<!--a href="#" class="pill pillblanco mr-1">&larr;&nbsp;&nbsp;Mi Familia( falta link categoria )</a-->

  			    	<a href="subcategoria.php?cat=<?php echo $id_categoria;?>" class="pill pillblanco mr-1">
					<? echo "&larr;&nbsp;&nbsp;".$nombre_categoria;?>
				</a>
			
	<br>
    <br>
    <br>
			</div>
    </div>
    <br>
    <br>
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h2 class="mb-4">
					Consultas disponibles sobre <br><span><? echo $nombre_subcategoria;?></span>
				</h2>
			</div>
	</div>
	
		<div class="row mt-5">
		<?
       $temas="SELECT                        
                subcategoria.`titulo` AS sc_titulo,
                   subcategoria.`id` AS sc_id,
                        tema.`foto` AS foto,
                        tema.`id` AS tema_id,
                        tema.`titulo` AS titulo,
                        tema.`miniresumen` AS miniresumen,
                        tema.`lectura` AS lectura,
                        tema.`costo` AS costo,
                        tema.`habilitada` AS habilitada,
                         tema.`tipo` AS tipo
                        FROM
                            `subcategoria`
                            INNER JOIN `tema` 
                                ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                                    where  subcategoria.`id`='$id_subcat' and tema.`habilitada`='SI'
                                      order by tema.`orden` asc";
                                     
      $listado=mysqli_query(conexion::obtenerInstancia(), $temas);
      while( $item = mysqli_fetch_assoc($listado))
      {
        if ($suscripcion =='SI')
        {
        ?>
          <div class="text-center col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
          <div class="caja subcategoria">
          <figure class="limit">
            
            <a href="consulta.php?tema=<?php echo $item['tema_id'];?>">
            <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" alt="" class="d-block w-100 grow"/>
          </a>
          
          </figure>
          <div class="subcategoria-tipo">
          </div>
      
          <div class="subcategoria-texto">
            <h4><a href="consulta.php?tema=<?php echo $item['tema_id'];?>">
              <?php echo utf8_decode($item['titulo']);?></a>
            </h4>
            <p> <?php echo utf8_decode($item['resumen']);?> </p>
          </div>
        </div>
      </div>
       <?
        }

        if ($suscripcion == 'NO' )
        {
         
         ?>
        <div class="text-center col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
        <div class="caja subcategoria">
          <figure class="limit">
            <?

             if ($item['tipo']=='GRATIS')
             {
             ?>
            <a href="consulta.php?tema=<?php echo $item['tema_id'];?>">
            <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" alt="" class="d-block w-100 grow"/>
          </a>
          <?
             }
          if ($item['tipo']=='PAGO' )
             {
             ?>
            <a href="consulta-paga.php?tema=<?php echo $item['tema_id'];?>">
            <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" alt="" class="d-block w-100 grow"/>
              <?
             }

            ?>

          </figure>
          <div class="subcategoria-tipo">
            <?
             if ($item['tipo']=='PAGO')
             {
               ?>
                 <div class="pill amarillo">
                      Pago
                  </div>
               <?
             }
              else
                {
               ?>
                 <div class="pill verde">
                      Gratis
                  </div>
               <?
             }
             ?>
            </div>

          
          <div class="subcategoria-texto">
             <?

             if ($item['tipo']=='GRATIS')
             {
             ?>
                <h4>
                  <a href="consulta.php?tema=<?php echo $item['tema_id'];?>"><?php echo utf8_decode($item['titulo']);?>  </a>
              </h4>
                    <p class="text-justify"><?php echo utf8_decode($item['miniresumen']);?>  </p>
                    <p class="text-justify"> Tiempo de Lectura: <?php echo utf8_decode($item['lectura']);?> </p>

                    <h4>Valor Consulta: $ <?php echo utf8_decode($item['costo']);?> </h4>
                    <br>
                            <a href="consulta.php?tema=<?php echo $item['tema_id'];?>"class ="boton botonazul">Leer Respuesta</a>
                    
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        </div>
                    </div>
 
              <?
             }

             if ($item['tipo']=='PAGO' )
             {
             ?>
                <h4>
                  <a href="consulta-paga.php?tema=<?php echo $item['tema_id'];?>"><?php echo utf8_decode($item['titulo']);?></a>
                </h4>
                    <p class="text-justify"><?php echo utf8_decode($item['miniresumen']);?>  </p>
                    <p class="text-justify"> Tiempo de Lectura: <?php echo utf8_decode($item['lectura']);?> </p>

                    <h4>Valor Consulta: $ <?php echo utf8_decode($item['costo']);?> </h4>
                    <br>
                            <a href="consulta-paga.php?tema=<?php echo $item['tema_id'];?>"class ="boton botonazul">Leer Respuesta</a>
                    
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        </div>
                    </div>
               
                
              <?
             }

            ?>
            <p>
              <?php echo utf8_decode($item['resumen']);?>
            </p>
          </div>
        </div>
       
			</div>
			<?
       }
        }//while
      ?>
		</div>
		    	<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		    	    <br>
		    	    <br>
				<h3 class="volanta">¿NO ENCUENTRAS LA RESPUESTA A TU PREGUNTA?</h3>
				<a href="enviarconsulta.php" class="botonHacerConsulta">Hacé tu consulta</a>
				<br>
				<br>
				<br>
                </div>
	</div>
</div>
<?php

}
include("pie.html");
?>