<?
include("cabecera.php");
?>

<div class="container">
 <img src="img/ENCABEZADO_T_Y_C.jpg" class="img-responsive ">
<br>
<br>
<p class="text-justify">
		TERMINOS Y CONDICIONES. <BR>
SERVIJUS, Servicios Jurídicos Online, agradece la visita de nuestro web
www.servijus.com.ar (en adelante el "Sitio") de propiedad de Consultora Sánchez
- Aguilera y Asociados. Le recomendamos que dedique un breve momento de su
tiempo para leer y comprender, los términos y condiciones, que se describen a
continuación (en adelante los "Términos y Condiciones") antes de usar el Sitio. El
acceso y uso del Sitio indica que usted acepta y se obliga libremente al
cumplimiento de estos Términos y Condiciones, que tienen por objeto, regular el
uso que del mismo realicen las personas que ingresen al Sitio. Por "Usuario" del
Sitio se entiende tanto a quienes se registren en el mismo como a los que tan sólo
visiten el mismo, (en adelante el "Usuario"). Al registrarse y/o navegar en el Sitio,
el Usuario presta su consentimiento al presente.
Los Términos y Condiciones así como la Política de Privacidad y Confidencialidad
y/o informaciones sobre el Sitio se considerarán de aplicación para todo Usuario
del Sitio desde el primer momento en que acceda al mismo. Los presentes
términos y condiciones tienen carácter obligatorio y vinculante. Se aplican a la
utilización de todos los servicios de consultas online, compras, suscripciones a
contenido “on demand” y actividades realizadas en el Sitio. Si el Usuario no acepta
en forma total los presentes Términos y Condiciones y la Política de Privacidad y
Confidencialidad, le rogamos que no avance en el acceso y visita de nuestro Sitio.
En caso de avanzar en la visita a nuestro Sitio, se entenderá que el Usuario
aceptó sin reservas los presentes Términos y Condiciones y la Política de
Privacidad y Confidencialidad, aceptando recibir mails periódicos con la
información que el Sitio determine. Los Términos y Condiciones y la Política de
Privacidad y Confidencialidad podrán ser modificados en todo o en parte en
cualquier momento y a exclusivo criterio de la Consultora; dichos cambios e
implementaciones tendrán vigencia a partir del momento mismo en que sean
publicados o insertados en el Sitio o desde que sean notificados al Usuario por
cualquier medio, lo que ocurra primero. Por lo expuesto, le sugerimos que los
visite periódicamente. Las violaciones a los Términos y Condiciones generarán el
derecho en favor del titular del Sitio a suspender o terminar la prestación del
servicio al Usuario que las haya realizado, por acción u omisión. El Sitio se
preocupa por la protección de datos de carácter personal y confidencial de los
Usuarios, de acuerdo a los lineamientos expuestos en nuestra Política de
Privacidad y Confidencialidad.
Los productos y servicios que se ofrecen por medio del Sitio (en adelante los
"Servicios"), sólo están disponibles para personas que tengan capacidad legal
para contratar. Por lo tanto, aquellos que no cumplan con esta condición deberán
abstenerse de suministrar información personal para ser incluida en nuestras
bases de datos. Sin embargo pueden hacerlo a través de sus padres o tutores,
gerentes y/o apoderados. Los padres, tutores o responsables de los menores de
edad o incapaces que utilicen el Sitio, como los Gerentes y/o Apoderados que lo
realices por sus representados, serán responsables por dicho uso, incluyendo
cualquier cargo, facturación o daño que se derive de él. Si el Usuario está
registrado como Empresa, deberá tener capacidad para comprar a nombre de tal
entidad y de obligar a la misma en los términos del presente.
Responsabilidad por uso
El Usuario es responsable de toda afirmación y/o expresión y/o acto celebrado con
su nombre de usuario, email y contraseña, y está obligado a proveer información
verdadera, correcta, actual y completa acerca de su persona al ser requerido en
los formularios. El Sitio se reserva el derecho, en cualquier momento y sin
necesidad de darle notificación previa, de retirar cualquier envío o dar por
concluido su calidad de miembro del Sitio por violación de los Términos y
Condiciones aquí descriptos. En caso de que la información o los datos
suministrados por el usuario no sean verdaderos, este será responsable por los
daños que este hecho pudiera ocasionar.
El Usuario acepta y reconoce que el sistema puede no siempre estar disponible
debido a dificultades técnicas o fallas de Internet, o por cualquier otro motivo ajeno
al Sitio, motivo por el cual no podrá imputársele responsabilidad alguna. El
contenido del Sitio, sus logotipos, programas y desarrollos, bases de datos,
imágenes, textos, información y archivos son de propiedad de la Consultora. Su
uso indebido así como su reproducción sin el expreso y escrito consentimiento
previo de Consultora Sánchez - Aguilera y Asociados, se encuentran
expresamente prohibidos y serán objeto de las acciones judiciales que
correspondan. El vínculo que se entable entre el Usuario y el Sitio no podrá ser
interpretado como un contrato de sociedad, mandato o que genere ningún tipo de
relación entre el Sitio y el Usuario distinta a la que se establezca en el Sitio, con
los alcances allí referidos. Por el sólo hecho de acceder al Sitio, el Usuario
reconoce y acepta que el uso de Internet implica la asunción de riesgos de daños
de diversa índole, que eventualmente podrían afectar al software y al hardware del
Usuario, pudiendo la computadora del Usuario ser atacada por hackers que
podrían tener acceso a la información contenida en la computadora del Usuario,
extraerla o dañarla. El envío de información a través de la red o a través de emails
tiene el riesgo de que tal información pueda ser captada por un tercero. El
Sitio no se hace responsable de las consecuencias que pudiera acarrear al
Usuario la asunción por parte del Usuario de ese riesgo.
Excepto en aquellos casos en que la responsabilidad de la Consultora pueda estar
limitada por medio de una política específica, en todo otro caso de existir
responsabilidad de Consultora Sánchez - Aguilera y Asociados., por cualquier tipo
de daños vinculados al Servicio, el monto de la misma no excederá en su totalidad
el precio pagado por el Usuario por el producto en cuestión.
Para adquirir los Servicios ofrecidos por el Sitio, los Usuarios deberán facilitar
determinados datos de carácter personal. Su información personal será procesada
y almacenada en servidores contratados por la Consultora. Para mayor
información sobre la privacidad de los Datos Personales y casos en los que será
revelada la información personal, se puede consultar nuestra Política de
Privacidad y Confidencialidad en nuestro Sitio.
No se permitirá ninguna acción o uso de dispositivo, software, u otro medio
tendiente a interferir tanto en las actividades y operatoria de la Consultora, como
en los servicio, descripciones, valores o bases de datos de la misma. Cualquier
intromisión, tentativa o actividad violatoria o contraria a las leyes sobre derecho de
propiedad intelectual y/o a las prohibiciones estipuladas en el presente harán
pasible a su responsable de las acciones legales pertinentes, y a las sanciones
previstas en estos Términos y Condiciones, así como también lo hará responsable
de indemnizar los daños y perjuicios ocasionados.
Responsabilidad por contenido
El contenido del material de asesoramiento legal online, se encuentra amparado
por las leyes en cada materia y conforma junto a ésta, un criterio de interpretación
que no asegura el resultado del un proceso o litigio. La responsabilidad de los
profesionales de la Consultora, está limitada al servicio prestado de asesoramiento
legal, sin que ello implique responsabilidad directa sobre el resultado a obtener. El
acceso y uso del sitio, así como la información contenida en éste y la utilización de
ésta, no significa ni crea una relación profesional-cliente o cualquier otro tipo de
relación. Usted no puede ni debe utilizar el Sitio, los contenidos del Sitio y la
información contenida en el Sitio como base o fundamento para elaborar
estrategias jurídicas, legales, estructuración de negocios o para decidir sobre
acciones legales o comerciales. Los temas de consultas on line, son elaborados
por profesionales matriculados, pero en ningún caso usted deberá entender que
este Sitio reemplaza la decisión de un juez o magistrado, quienes tienen la última
palabra sobre el tema en particular consultado. Si usted requiere asesoría legal o
jurídica o de cualquier tipo ante un juicio o proceso judicial, le recomendamos
consultar inmediatamente a un profesional quien podrá atender su caso y realizar
un diagnostico de su problema, en tiempo y forma de ley.
El contenido de los servicios legales contratados on line, solo deberá ser utilizado
como información de carácter personal, no constituye bajo ningún concepto una
declaración de resultado. Recuerde que un profesional matriculado debe asistirlo
ante un ligio o proceso judicial, por lo que el contenido de las consultas legales on
line, no puede ser incorporado como prueba documental o informativa a un
proceso judicial.
La Consultora y el Sitio, quedan totalmente eximida de responsabilidad legal por
las publicaciones realizadas por otros sitios o usuarios que accedan al Sitio. Son
de responsabilidad exclusiva del contenido, material informativo y opiniones
particulares, los autores que publiquen o utilicen el Sitio para divulgar información.
Precio de Servicios
La información sobre Servicios y precios está sujeta a cambios sin previo
aviso. Todos los precios expresados en el Sitio incluyen IVA, salvo que se indique
lo contrario. Todos los precios en el Sitio están expresados en pesos argentinos,
moneda de curso legal de la República Argentina. Al realizar una compra online de
los servicios a través del Sitio, se tienen por aceptados por el Usuario los
presentes Términos y Condiciones, la Política de Privacidad y Confidencialidad.
Del mismo modo, acepta que las condiciones correspondientes al uso de la
plataforma de pago que se ofrecen en el Sitio, resultan ajenos a la Consultora,
razón por la cual ninguna responsabilidad podrá ser imputada a la Consultora
derivada del uso y/o de la correcta prestación de dichos servicios.
Vigencia y validez de las promociones
En el caso de que se realicen ofertas y promociones de Servicios, éstas tendrán
validez para las compras efectuadas desde la fecha de comienzo de las mismas,
hasta la de finalización de la oferta según lo expuesto en el Sitio. Los términos y
condiciones particulares de las mismas serán comunicados en el Sitio, y estarán
siempre sujetas a la existencia de los servicios ofrecidos. A tal fin se considerará
como fecha de concreción de la operación de compra la fecha de pago con los
distintos medios aceptados.
Modificaciones a los Términos y Condiciones
El Sitio se reserva expresamente los siguientes derechos: (1) A modificar o
eliminar, en forma unilateral, parcial o totalmente, tanto la disposiciones de los
elementos que componen el Sitio, como su configuración general o particular, los
servicios o contenidos o sus condiciones de acceso y utilización; (2) A denegar o
retirar el acceso a este Sitio, sus servicios y contenidos, en cualquier momento y
sin aviso previo, a aquellos Usuarios que incumplan los Términos y Condiciones,
(3) A poner término, suspender o interrumpir unilateralmente, en cualquier
momento y sin aviso previo, la prestación del servicio del Sitio, (4) A modificar los
alcances de los Términos y Condiciones en cualquier momento.
Los Términos y Condiciones serán interpretados y ejecutados de acuerdo a las
leyes de la República de Argentina. El Usuario se somete incondicionalmente a la
jurisdicción de los Tribunales Ordinarios de la Provincia de San Juan para la
resolución de cualquier duda, dificultad o controversia relacionada con todo o parte
de los Términos y Condiciones.
Se fija como domicilio de SERVIJUS - Consultora Sánchez - Aguilera y Asociados
en Av. Libertador Gral. San Martin 575 Este, Ciudad, San Juan.

	</p>
  
</div>
</div><!-- fin container-->
<br>
<br>
<br>

<?
include ("pie.php");
?>  
