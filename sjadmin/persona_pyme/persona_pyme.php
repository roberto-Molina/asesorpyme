<?
include_once("../bd/conexion.php");
class Persona
{
  
  public function lista()
  {
    $consulta="SELECT per.*,prov.nombre as provincia FROM persona per,provincia prov where per.provincia_id=prov.id order by per.nombre";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }
  
  public function listaprovincia()
  {
    
	$consulta="SELECT * FROM persona order by nombre";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }  


   public function nuevo($nombre,$email,$provincia)
   	{
  	  $sql="INSERT INTO `persona`
            (`nombre`,
             `email`,
             `provincia_id`)
              VALUES (
                      '$nombre',
                      '$email',
                      '$provincia');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
    	}


	public function habilitar($id)
	{
   	  $sql="UPDATE `persona`
              SET 
                `estado` = 'Habilitado',
				 `fecha_baja` = ''
              WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }
	
	public function deshabilitar($id)
	{
   	  $fechabaja = date("Y-m-d");
	  $sql="UPDATE `persona`
              SET 
                `estado` = 'Deshabilitado',
				`fecha_baja` = '$fechabaja'
              WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }

		public function activar($id)
	{
   	  $sql="UPDATE `persona`
              SET 
                `estado` = 'Activo' 
              WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }
	

    public function obtenerId($id)
  	{
  	 $sql="SELECT * FROM persona where id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }

    public function eliminar($id)
	  {
     $sql="DELETE FROM persona WHERE id ='$id'";
     $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
     return $rs;
    }

    public function obtener_persona($email)
    {
     $consulta="SELECT * FROM persona where email='$email' and tipo='Pyme' ";
     $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
     if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
        return $data;
      }else return $rs;
    }

   // para las estadisticas de los ingresos de personas
  public function agregar_e_login_pyme($persona_id,$fechahora)
  {
    $sql="INSERT INTO `e_login_pyme`
            (`persona_id`,
             `fechahora`)
            VALUES ('$persona_id',
                    '$fechahora');";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }
    
}
?>