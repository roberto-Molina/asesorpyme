<?
include_once("../bd/conexion.php");
class persona_plan
{
  
  public function lista()
  {
    $consulta="SELECT
persona_pyme.`nombre` AS persona_nombre,
persona_pyme.`email` AS persona_email,
persona_pyme.`id` AS persona_id,
plan.`nombre` AS plan_nombre,
plan.`id` AS tipo,
persona_plan.`estado` AS plan_estado,
persona_plan.`id` AS plan_id,
persona_plan.`fecha_solicitud` AS fecha_solicitud,
persona_plan.`estado` AS estado
FROM
    `persona_plan`
    INNER JOIN `persona_pyme` 
        ON (`persona_plan`.`persona_id` = `persona_pyme`.`id`)
    INNER JOIN `plan` 
        ON (`persona_plan`.`plan_id` = `plan`.`id`)
        ORDER BY persona_plan.`fecha_solicitud`ASC";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }
  
  public function listaprovincia()
  {
    
	$consulta="SELECT * FROM provincia order by nombre";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }  


 	public function habilitar($id,$fecha_vencimiento,$persona_id)
	{
    $sql="UPDATE `persona_plan`
          SET 
            `estado` = 'Habilitado',
            `fecha_fin` = '$fecha_vencimiento'
             WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    
    $sql="UPDATE `persona_pyme`
    SET 
      `suscripcion` = 'SI'
    WHERE `id` = '$persona_id';";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

	
	public function deshabilitar($id)
	{
   	  $fechabaja = date("Y-m-d");
	  $sql="UPDATE `persona_plan`
              SET 
                `estado` = 'Deshabilitado',
				`fecha_baja` = '$fechabaja'
              WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }

	public function activar($id)
	{
   	  $sql="UPDATE `persona_plan`
              SET 
                `estado` = 'Activo' 
              WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }
	

    public function obtenerId($id)
  	{
  	 $sql="SELECT * FROM persona_plan where id='$id'";
     $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
     if(mysqli_num_rows($rs) >0)
     {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
     }

    public function eliminar($id)
	  {
     $sql="DELETE FROM persona_plan WHERE id ='$id'";
     $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
     return $rs;
    }

    public function obtener_persona($email)
    {
     $consulta="SELECT * FROM persona_pyme where email='$email'";
     $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
     if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
        return $data;
      }else return $rs;
    }

   // para las estadisticas de los ingresos de personas
  public function agregar_e_login($persona_id,$fechahora)
  {
    $sql="INSERT INTO `e_login_pyme`
            (`persona_id`,
             `fechahora`)
            VALUES ('$persona_id',
                    '$fechahora');";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }
    
}
?>