<?php
require '../cabecera.php';
require '../menu.php';
include("persona_plan.php");
?>
<div class="container-fluid">
    <h2>Listado de Suscripciones de Pymes - Solicitadas y Pendientes
     -</h2>

    <!--p> <a class="btn btn-primary" href="nuevo.php">Adicionar Nuevo</a> </p-->

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>Persona E-mail</th>
             <th>Persona Nombre</th>
             <th>Plan</th>
             <th>Fecha Solicitud</th>
      			 <th>Estado</th>
      			 </tr>
           <thead>
           <tbody>
          <?php
          $listados = persona_plan::lista();
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['plan_id']; ?></td>
              <td><?php echo $item ['persona_email']; ?></td>
              <td><?php echo $item ['persona_nombre']; ?></td>
              <td><?php echo utf8_decode($item['plan_nombre']); ?></td> 
              <td><?php echo $item ['fecha_solicitud']; ?></td> 
              <td>
              <?
                if ($item ['estado']=='Solicitada') 
                {
                ?>
						<a class="btn btn-info" id="habilitar<?php echo $item['plan_id'];?>"  > Pendiente - Habilitar - Mensual</a>
                     <?
                    }
                    else echo $item ['estado'];
                  ?> 

                
              </td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

 <script src="../vendor/jquery/jquery.js"></script>
  <script type="text/javascript">


 $(document).ready(function()
  {
     //habilitar persona
     $("a[id^='habilitar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(9,6);
        var opcion = confirm("Confirmar Habilitar : Aceptar o Cancelar");
        if (opcion == true)
        {
          $.ajax({
                  type: "POST",
                  cache: false,
                  async: false,
                  url: 'cambiar_estado_habilitar.php',
                  data: { id: vid},
                  success: function(data){
                    if (data)
                    {
                      alert(data);
                      location.reload(true);
                    }
                }
                })//fin ajax
          //alert("Has clickado OK");
        }
        else {
          alert("Se Cancelo la Operación");
        }  
        });//fin
		
		
     //Deshabilitar persona
     $("a[id^='deshabilitar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(12,6);
        var opcion = confirm("Confirmar Deshabilitar : Aceptar o Cancelar");
        if (opcion == true)
        {
          $.ajax({
                  type: "POST",
                  cache: false,
                  async: false,
                  url: 'cambiar_estado_deshabilitar.php',
                  data: { id: vid},
                  success: function(data){
                    if (data)
                    {
                      alert(data);
                       location.reload(true);
                    }
                }
                })//fin ajax
          //alert("Has clickado OK");
        }
        else {
          alert("Se Cancelo la Operación");
        }  
        });//fin	


     //Activar persona
     $("a[id^='activar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(7,6);
        var opcion = confirm("Confirmar Activar : Aceptar o Cancelar");
        if (opcion == true)
        {
          $.ajax({
                  type: "POST",
                  cache: false,
                  async: false,
                  url: 'cambiar_estado_activar.php',
                  data: { id: vid},
                  success: function(data){
                    if (data)
                    {
                      alert(data);
                       location.reload(true);
                    }
                }
                })//fin ajax
          //alert("Has clickado OK");
        }
        else {
          alert("Se Cancelo la Operación");
        }  
        });//fin		

     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'cambiarestado.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
      });

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        var opcion = confirm("Confirmar Borrar Registro : Aceptar o Cancelar");
        if (opcion == true)
        {
          $.ajax({
                  type: "POST",
                  cache: false,
                  async: false,
                  url: 'eliminar.php',
                  data: { id: vid},
                  success: function(data){
                    if (data)
                    {
                      alert(data);
                       location.reload(true);
                    }
                }
                })//fin ajax
          //alert("Has clickado OK");
        }
        else {
          alert("Se Cancelo la Operación");
        }  
        });//fin
 });


 function prueba(parametro){
    alert(parametro);
    return 1;
  }

</script>
</body>
</html>