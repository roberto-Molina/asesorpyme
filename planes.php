<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>
<div class="espacio gris">
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">MEMBRESIAS</h3>
				<h2 class="mb-4">
					Elíge una de nuestras membresías <br>y disfruta de todo los servicios.
				</h2>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan planmensual">
					<h4>
						Membresía Mensual
					</h4>
					<h5>
						<!-- $250 -->
					<?  
                  $sql="SELECT valor FROM plan where nombre='Mensual Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    <h4>
                        <ul>
                            <li><span>Valor Mensual Bonificado</span></li>
                            <li><span>VALOR A PAGAR x 1/Mes $ 2850</span></li>
                            <li><span>VALOR REAL $ 3000</span></li>
                        </ul>
                    </h4>
					</h5>
					<ul>
						<li><span>Acceso ilimitado todas<br>las Consultas y Servicios</span></li>
						<li><span>Bonificación del 5% en<br>el valor de la cuota</span></li>
						<li><span>Descarga ilimitadas de:</span><br>Contratos Modelos<br>Cartas Documentos y<br>Contesaciones de TL<br>Notas y Formularios</li>
					</ul>
					
					 <script
                    src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                    data-preference-id="96772022-9decb9a4-8f4b-443b-bae5-ea073bb7a762">
                    </script>
					<br>
					<br>
					<a href="https://www.servijus.com.ar/consultas" target="_blank" >Membresía para Cámaras Empresarias</a><br>
					
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plansemestral">
					<h4>
						Membresía Semestral
					</h4>
					<h5>
					
					<?  
                  $sql="SELECT valor FROM plan where nombre='Semestral Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    <h4>
                        <ul>
                            <li><span>Valor Mensual Bonificado</span></li>
                            <li><span>VALOR A PAGAR X 6/meses $ 15300</span></li>
                            <li><span>VALOR REAL $ 18000</span></li>
                        </ul>
                    </h4>
					</h5>
					<ul>
						<li><span>Acceso ilimitado todas<br>las Consultas y Servicios</span></li>
						<li><span>Bonificación del 15% en<br>el valor de la cuota</span></li>
						<li><span>Descarga ilimitadas de:</span><br>Contratos Modelos<br>Cartas Documentos y<br>Contesaciones de TL<br>Notas y Formularios</li>
						<li><span>MEMBRESIA MÁS REQUERIDA<br>POR NUESTROS CLIENTES</span></li>
					</ul>
					  
					<script
                    src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                    data-preference-id="96772022-97276d24-0839-4277-ba11-f09fd193cf7d">
                    </script>
					<br>
					<br>
					<a href="https://www.servijus.com.ar/consultas" target="_blank" >Membresía para Cámaras Empresarias</a><br>
					
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plananual">
					<h4>
						Membresía Anual
					</h4>
					<h5>
						<?  
                  $sql="SELECT valor FROM plan where nombre='Anual Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    <h4>
                        <ul>
                            <li><span>Valor Mensual Bonificado</span></li>
                            <li><span>VALOR A PAGAR X 12/meses $ 27000</span></li>
                            <li><span>VALOR REAL $ 36000</span></li>
                        </ul>
                    </h4>
					</h5>
					<ul>
						<li><span>Acceso ilimitado todas<br>las Consultas y Servicios</span></li>
						<li><span>Bonificación del 25% en<br>el valor de la cuota</span></li>
						<li><span>Descarga ilimitadas de:</span><br>Contratos Modelos<br>Cartas Documentos y<br>Contesaciones de TL<br>Notas y Formularios</li>
					</ul>
				    <script
                    src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                    data-preference-id="96772022-2dce7bbd-c29e-4a4d-b24c-5bb2a8dce5f0">
                    </script>
					<br>
					<br>
					<a href="https://www.servijus.com.ar/consultas" target="_blank" >Membresía para Cámaras Empresarias</a><br>
				
				</div>
			</div>
		</div>
	</div>
</div>
<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function()
  {
    
    $("a[id^='mensual']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(7,5);
        vplan_id=4;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='semestral']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(9,5);
        vplan_id=6;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='anual']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(5,5);
        vplan_id=5;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

 });	
</script>
<?php
include("contadortemas.php");
include("pie.html");
//include("pie.html");
?>