<div id="nav">
	<nav class="container navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="index.php">
			<img src="_img/LOGO-PARA-PYMES.jpg" alt="Servijus" width="200"/>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">

				<li class="nav-item">
					<a class="nav-link" href="index.php">Inicio</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="planes.php">Membresias</a>
				</li>

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Temas de Consultas
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=12">Gestion Empresaria</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=13">Gestion Laboral</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=14">Gestion de RRHH</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=19">Covid-19 en la Empresa</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=6">Derechos Personales</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=7">Derechos Familiares</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=8">Derechos Inmobiliarios</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=10">Derechos del Automotor</a>

					</div>
				</li>

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Servicios
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="https://asesorpyme.servijus.com.ar/agenda.php">Agenda Empresaria</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=11">Asociaciones Civiles</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.servijus.com.ar/registro-marca/">Inscripcion de Marcas y Patentes</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="https://www.servijus.com.ar/disenos-industriales/">Incripcion de Modelos Industriales</a>
					</div>
				</li>


<?php
			 //include("sesion.php");
			session_name("sesion_sj2018");
			 session_start();
			 if (isset($_SESSION['sesion_usuario']))
			 {
			   $ID= $_SESSION['sesion_id'];
			   $USUARIO=$_SESSION['sesion_usuario'];
			   $NOMBRE=$_SESSION['sesion_nombre'];
			   $PROVINCIA=$_SESSION['sesion_provincia'];
			   $EMAIL=$_SESSION['sesion_email'];
			   $SUSCRI=$_SESSION['sesion_suscri'];
			   ?>
			 

			<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Mi Cuenta 
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="perfil.php">Mi Perfil</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="salir.php">Cerrar Sesión</a>
						
					</div>
				</li>

			 <?
			 } else
			    {
			     ?>
				<br>

			</li>
			 <li class="nav-item">   
				<a href="iniciar-sesion.php" class="boton botonazul">Inicia Sesión</a>
			    </li>
			    <?}?>



			</ul>
			<!--form class="form-inline my-2 my-lg-0">
				<input class="input-busqueda mr-sm-2" type="search" placeholder="Buscar consultas" aria-label="Search">
			</form-->

			

			
		</div>
	</nav>
</div>