<?
include("sjadmin/bd/conexion.php");
include("cabecera.php");
?>
<div class="container">
<nav class="navbar navbar-light bg-light justify-content-between">
   <a href="index.php"><img src="img/Logo_Servijus_vector.png" height="88" width="300" class="img-fluid"></a> 
  <form class="form-inline">
     <a href="login.php" class="myButton">INICIAR SESION</a>
  </form>
</nav>


<div class="container">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Suscribirse</li>
  </ol>
</nav>

<br>
<br>
  <div class="row">
  <div class="col-md-12">
  <hr>
    <h3>1 - Completar los datos de Registro</h3>
    <form id="form" method="post" action="nuevo_suscriptor.php">
    
      <div class="col-md-8">
        <label>Email</label>
        <input name="email" id="email" class="form-control" type="email" tabindex="1" maxlength="100" required autofocus/>
      </div>

       <div class="col-md-8">
        <label>Provincia</label>
          <select class="form-control" name="provincia" id="provincia" tabindex="1" required >
            <option>Seleccionar ...</option>
            <?php
            $sql="select id, nombre from provincia";
            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
              foreach($provincias as $item)
              {
                echo "<option value='".$item[id]."'> ". utf8_encode($item[nombre])."</option>";
              }
        ?>
        </select>
      </div>
   <div class="col-md-8">
   <hr>
        <!--button  id="registrar" class="myButton"> Registrarse </button-->
         <button  type="submit" class="myButton"> Registrarse </button>
    </div>
</form>  
    
</div>
</div>

<br>
<br>
<br>
 <hr>
 <h3>2 - Elegir un Plan de Suscripción</h3>

<!-- TABLA PRECIOS-->
<div class="container mb-5 mt-5">

    <div class="pricing card-deck flex-column flex-md-row mb-3">
        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Plan Mensual</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span class="price"> <?  
                  $sql="SELECT valor FROM plan where nombre='Mensual'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }

                ?></span><span class="h6 text-muted ml-2">/ por mes.</span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>Suscripcion Mensual</li>
                    <!--li>Basic support on Github</li>
                    <li>Monthly updates</li>
                    <li>Free cancelation</li-->
                </ul>
              <a mp-mode="dftl" href="https://mpago.la/2XBpMe" name="MP-payButton" class='green-ar-m-rn-undefined'>Suscribirme Mensualmente</a>


<script type="text/javascript">
    (function() {
        function $MPC_load() {
              window.$MPC_loaded !== true && (function() {
                var s = document.createElement("script");
                s.type = "text/javascript";
                s.async = true;
                s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
                window.$MPC_loaded = true;
            })();
        }
        window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
    })();
</script>
            </div>
        </div>

        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Plan Anual</span>
            <div class="bg-transparent card-header pt-4 border-0">
            <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span class="price"> <?  
                  $sql="SELECT valor FROM plan where nombre='Anual'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }

                ?></span><span class="h6 text-muted ml-2">/ por el año.</span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>Suscripcion Anual</li>
                    <!--li>Basic support on Github</li>
                    <li>Monthly updates</li>
                    <li>Free cancelation</li-->
                </ul>
                <a mp-mode="dftl" href="https://mpago.la/1A8qzt" name="MP-payButton" class='green-ar-m-rn-undefined'>Suscribirme Anualmente</a>
<script type="text/javascript">
    (function() {
        function $MPC_load() {
            window.$MPC_loaded !== true && (function() {
                var s = document.createElement("script");
                s.type = "text/javascript";
                s.async = true;
                s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
                window.$MPC_loaded = true;
            })();
        }
        window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
    })();
    </script>

            </div>
        </div>
        
    </div>
</div>
<!-- FIN-->

 <script type="text/javascript">
 $(document).ready(function()
  {
    v_boton ="SUSCRIBITE";
    $.post("registrar_boton.php", {boton: v_boton}, function(mensaje) {
        });//fin post
  });
</script>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</div>
</div>
<?
include ("pie.php");
?>  
