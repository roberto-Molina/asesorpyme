<?php
include("sjadmin/bd/conexion.php");

if( isset($_POST['email']) && !empty($_POST['email']) )
{ 
   $email=$_POST['email'];

   // validar email duplicado
   $sql="SELECT `email` FROM `persona_pyme` WHERE `email`='$email'";
   $consulta=mysqli_query(conexion::obtenerInstancia(), $sql);
   if(mysqli_num_rows($consulta)>0) { 

        //echo "El Email no se encuentra disponible. Por favor intente con otro.";
        //exit();

     echo "<script language=Javascript> alert('El Email no se encuentra disponible. Por favor intente con otro.'); 
       window.history.back();
     </script>"; 
     exit();

      
      //echo "<a href=\"javascript:history.back()\">Regresar</a>"; 
       //echo "<script language=Javascript> location.href=\"registrate.php\"; </script>";
    }
    else
     { // fin de mi consulta 
       // fin del validar email duplicado
       $provincia_id=$_POST['provincia'];
       
       $plan=$_POST['plan'];

       
       
       $fecha_ingreso=date("Y-m-d");
       /*generar una clave  */
       $longitud = 8; // longitud del password  
       $password = substr(rand(),0,$longitud); 
       /*enviar email*/
        ini_set('display_errors', 1 );
        error_reporting( E_ALL );
        $from = "correo@servijus.com.ar";
        $to = $email;
        $subject = "Suscripción Servijus.com.ar";
        $message ='
            <!DOCTYPE html>
            <html lang="es">
            <head>
                <title>Asesor Online</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link type="text/css" rel="stylesheet" href="css/bootstrap_mat.css">
            </head>
            <body>
            <div class="container">
             <table class="table" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
             <tr>
              <td aling="center" style="color:blue;">
              <h1>SERVIJUS Consultoría Online</h1>
              </td>
             </tr>
             <tr>
             <td bgcolor="#ffffff">
             <p style="font-size: 16px">
            Tu registro ha sido exitoso!, para ingresar a Tu Asesor Pyme <br>tu CLAVE DE ACCESO es:
            </p>
            <p style="font-size: 18px; color: blue ; ">
            '.$password.'
            </p>
             <p style="font-size: 16px">
            Para iniciar sesión, utiliza el siguiente link e ingresa TU MAIL y la CLAVE DE ACCESO:<br>
            "https://www.asesorpyme.servijus.com.ar/iniciar-sesion.php"
            </p>            
            <br>
            <p style="font-size: 16px">
            Para mayor seguridad, desde el perfil de Tu Cuenta, realiza el cambio de la CLAVE DE ACCESO. Gracias!
            <br>
            <br>
            Saludos cordiales!
            <br> 
            El equipo de Servijus.
            </p>
            <br>
             </td>
             </tr>
             <tr>
             <td >
            <p style="text-align: justify;">
            SERVIJUS, te recuerda que con el fin de resguardar tu seguridad, nunca te solicitaremos datos personales ni datos
            de tu tarjeta de crédito a través de los e-mails que te enviemos. No respondas e-mail que soliciten información
            personal o incluyan links que te deriven a páginas en internet que te lo soliciten. Si deseas no recibir más
            comunicación de nuestros servicios o dar de baja a tu registro, accede desde el link de desuscripción siguiendo los
            pasos que allí te indicamos. La información de este mail es de uso personal y no puede ser compartida ni transmitida
            a terceros, quedando la misma protegida por las normas legales y los Términos y Condiciones que regulan el uso de
            nuestros servicios jurídicos online.
            </p>
            </td>
            </tr>
            </table>
            </div>  
            </body>
            </html>';
    $headers = "From:" . $from;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    mail($to,$subject,$message,$headers);
       // echo "The email message was sent.";
     /* $largo=8;
      $cadena_base =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      $cadena_base .= '0123456789' ;
      $cadena_base .= '!@#%^&*()_,./<>?;:[]{}\|=+';
     
      $password = '';
      $limite = strlen($cadena_base) - 1;
     
      for ($i=0; $i < $largo; $i++)
      {
        $password .= $cadena_base[rand(0, $limite)];
      }
       */ 

      $password_md=md5($password);
      //  antes $sql="INSERT INTO `persona_pyme` ( `email`,`password`,`provincia_id`,`suscripcion`,`fecha_ingreso`,`estado`,`alta`) VALUES ('$email','$password_md','$provincia_id','','$fecha_ingreso','Activo','Registro') ;";
      
      $sql="INSERT INTO `persona` (`email`,`password`,`provincia_id`,`suscripcion`,`fecha_ingreso`,`estado`,`fecha_habilitacion`,`alta`,
             `tipo`) VALUES ('$email','$password_md','$provincia_id','SI','$fecha_ingreso','Habilitado','$fecha_ingreso','Membresia','Pyme');";
      
      
      $ins_persona=mysqli_query(conexion::obtenerInstancia(), $sql);

      // 05012021 sacar el ultimo id

      $persona_id = mysqli_insert_id(conexion::obtenerInstancia());


     // calcular los dias de la suscripcion
     
      $fecha_ingreso=date("Y-m-d");

    switch ($plan)
    {
    case '4':
        // mensual 30 dias
        { 
                $dias='+30 day';
                break;
              }
    case '5':
        // anual 360 dias
        { 
                $dias='+360 day';
                break;
              }
    case '6':
        // semenstral 180 dias
       { 
                $dias='+180 day';
                break;
              }
    case '9':
        // OBSEQUIO 5 dias
       { 
                $dias='+7 day';
                break;
              }              
    }
    
    
    $fecha = date_create($fecha_ingreso);
 
    date_add($fecha, date_interval_create_from_date_string($dias));

    $fecha_fin= date_format($fecha, 'Y-m-d');
    
    $fecha_solicitud=date("Y-m-d");

   

      // 05012021 insertar el tipo de plan
    $ins_plan_sql="INSERT INTO `persona_plan`
            (`persona_id`,
             `plan_id`,
             `fecha_solicitud`,
             `estado`,
             `fecha_ingreso`,
             `fecha_fin`,
             `tipo`)
              VALUES ('$persona_id',
                      '$plan',
                      '$fecha_solicitud',
                      'Habilitado',
                      '$fecha_ingreso',
                      '$fecha_fin',
                      'Pyme');";
    $ins_plan=mysqli_query(conexion::obtenerInstancia(), $ins_plan_sql);                  

    if($ins_persona)
      {
        // echo "Se envio un email a su cuenta con la clave para ingresar a el contenido de Servijus.";
         //exit();

        echo "<script language=Javascript> alert('Se envio un email a su correo electrónico con la CLAVE DE ACCESO para ingresar a todo el contenido de ASESOR PYME.');
     </script>"; 
     
     echo "<script language=Javascript> location.href=\"iniciar-sesion.php\"; </script>"; 
          //header('Location: listado.php');
      } 
      else {
            //echo "No se pudo Guardar la información.";
            //exit();
            echo "<script language=Javascript> alert('No se pudo Guardar la información.');window.history.back();
             </script>"; 
             exit();
            //echo "<script language=Javascript> location.href=\"index.php\"; </script>";
            }     
    }// fin validar
  }// fin del if principal  
  //exit();
?>