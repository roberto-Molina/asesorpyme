<?php
include("../sjadmin/bd/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Contratos</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="../css/bootstrap.css">
     <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
</head>
<body>
<div class="container">
 <img src="../img/ENCABEZADO_BCV.jpg" class="img-responsive center-block">
<br>
<br>
<div class="row">
<div class="col-md-6">
 <div class="row">
  <div class="col-md-12">
 
    <h4>Registrate Gratis para usar este Servicio.</h4>
    <div id="div_registro">
      <hr>
    <form id="form" >
    
      <div class="col-md-8">
        <label>Email</label>
        <input name="email" id="email"  class="form-control" type="email" tabindex="1" maxlength="100" required />
      </div>

       <div class="col-md-8">
        <label>Provincia</label>
          <select class="form-control" name="provincia" id="prov" tabindex="2" required >
            <option>Seleccionar ...</option>
            <?php
            $sql="select id, nombre from provincia";
            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
              foreach($provincias as $item)
              {
                echo "<option value='".$item[id]."'> ". utf8_encode($item[nombre])."</option>";
              }
        ?>
        </select>
      </div>
   <div class="col-md-8">
   <hr>
        <button id="registro" class="myButton"> Registrarse para habilitar el Formulario de Carga</button>
    </div>
</form>    
    </div>
    
    
</div>

<div class="col-md-12">
  <br>
  <br>

<p class="text-center lead">BOLETO DE COMPRA VENTA DE VEHICULO</p>
<p class="text-justify">
En la ciudad de  <code>[CIUDAD]</code>, el <code>[FECHA]</code> , entre el/a Sr/a. <code>[NN VENDEDOR]</code>, D.N.I. N° <code>[DNI VENDEDOR]</code>, con domicilio real en <code>[DOMIC VENDEDOR]</code>, <code>[PROVINCIA V]</code>, en adelante el “VENDEDOR”, y el/a Sr/a. <code>[NN COMPRADOR]</code>, D.N.I. N° <code>[DNI COMPRADOR]</code>, con domicilio real en <code>[DOMIC COMPRADOR]</code>, <code>[PROVINCIA C]</code>, en adelante el “COMPRADOR”, convienen en celebrar el presente Contrato de Compra Venta de Vehículo, con compromiso de transferencia de dominio, el que se ajustará a las siguientes cláusulas:
</p>
<br>
<p class="text-justify">
Primera: Objeto: El VENDEDOR, vende al COMPRADOR y este adquiere un <code>[VEHICULO]</code>, usado/0 KM, Marca <code>[MARCA]</code>, Tipo <code>[TIPO]</code>, Modelo <code>[MODELO]</code>, Dominio <code>[DOMINIO]</code>, Motor N° <code>[MOTOR NRO]</code>, Chasis N° <code>[CHASIS NRO]</code>, en el estado en que se encuentra, y que el COMPRADOR declara conocer y aceptar. 
</p>
<br>
<p class="text-justify">
El VENDEDOR, exhibe en este acto al COMPRADOR, Certificado de Dominio del Automotor, expedido por el DNRPA N° <code>[REGISTRO NRO]</code>, donde se encuentra el legajo del vehículo objeto del presente contrato y del cual resulta la titularidad dominial del VENDEDOR, declarando en este acto, que el vehículo no se encuentra ni gravado ni prendado y apto para ser transferido.
</p>
<br>
<p class="text-justify">
Segunda: Precio: El precio convenido asciende a la suma de Pesos <code>[PRECIO TOTAL LETRAS]</code>, ($ <code>[PRECIO TOTAL NRO]</code>), el que se abona de la siguiente forma: de contado la suma de $ <code>[PRECIO CONTADO NRO]</code>, y el saldo de $ <code>[PRECIO SALDO NRO]</code>, en <code>[NRO DE CUOTAS]</code> cuotas mensuales y consecutivas de $ <code>[VALOR CUOTA]</code> cada una con vencimientos los días <code>[VTO CUOTA]</code> de cada mes. El dinero en efectivo, es entregado en este acto, sirviendo el presente como suficiente recibo de pago y conformidad.
</p>
<br>
<p class="text-justify">
Tercera: Transferencia: El COMPRADOR, se obliga a efectuar la transferencia del automotor a su nombre dentro del plazo de 24 horas, a contar desde la fecha de la firma del presente, caso contrario se realizará la Denuncia de Venta por ante el DNRPA y por ante la Dirección de Rentas del Automotor, una vez vencido el plazo otorgado.
</p>
<br>
<p class="text-justify">
Cuarta: Incumplimiento: En el caso de que cualquiera de las partes no concurrieran por ante el DNRPA que corresponda, a formalizar la Transferencia de Dominio objeto del presente contrato en el plazo previsto en la clausula anterior, se establece como multa por cada día de retraso el 1% del valor del contrato, el que se exigirá judicialmente, sin previa interpelación.
</p>
<br>
<p class="text-justify">
Quinta: Documentación: En este acto, el VENDEDOR, entrega el Título de Dominio, la Cédula Verde, los comprobantes de pago de las Patentes, haciéndose las siguientes observaciones: <code>[OBSERVACIONES]</code>.
</p>
<br>
<p class="text-justify">
Sexta: Responsabilidad: A partir de la firma del presente día, el COMPRADOR acepta en forma exclusiva la responsabilidad civil, penal y administrativa, por todas las consecuencias derivadas del uso o posesión del vehículo, eximiendo de todo tipo de responsabilidad al VENDEDOR por el uso indebido del mismo.
</p>
<br>
<p class="text-justify">
Séptima: Consentimiento: El/a Sr/a. <code>[CONYUGE]</code>, D.N.I. N° <code>[DNI CONYUGE]</code>, esposa/o del VENDEDOR y presente en este acto, presta su conformidad a la venta pactada y compromete su presencia ante la firma por ante el DNRPA que corresponda, a los fines de dar su asentimiento conyugal para el acto de transferencia de dominio.
</p>
<br>
<p class="text-justify">
Octava: Jurisdicción: Las partes constituyen domicilio legal para los efectos derivados del presente contrato, en los domicilios denunciados up supra, donde se tendrán por validas todas y cada una de las notificaciones extrajudiciales y/o judiciales con relación al contrato. Asimismo ambas partes, manifiestan que se someterán a la competencia ordinaria de los Tribunales de <code>[PROVINCIA]</code>, para todos los efectos derivados del presente boleto de compra venta.
</p>
<br>
<p class="text-justify">
En prueba de conformidad se firman dos ejemplares de un mismo tenor y a un solo efecto.
</p>
<br>

  <div class="row">
    <div class="col-md-6">
        <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>VENDEDOR</td>
          </tr>
        </table>
                                                                            
    </div>
    <div class="col-md-6">
   <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>COMPRADOR</td>
          </tr>
        </table>
    </div>
 </div>
  <?
  //}
?>
</div>
</div>
</div>

<div class="col-md-6"> 
  <h4>Formulario de Carga.</h4>
  <hr>
     
 <form method="post" id="form_carga" action="boleto_pdf.php" style="display: none">
  
  <div class="col-md-12">
    <label>Ciudad</label>
    <input name="ciudad"  class="form-control" type="text" tabindex="1" maxlength="60" required autofocus/>
  </div>

    <div class="col-md-12">
    <label>Fecha</label>
    <input name="fecha"  class="form-control" type="text" tabindex="2" required />
  </div>
 
  
  <div class="col-md-12">
    <label>Nombre del Vendedor</label>
    <input name="nombre_vendedor"  class="form-control" type="text" tabindex="3" maxlength="100" required />
  </div>  

   <div class="col-md-12">
    <label>D.N.I. del Vendedor</label>
    <input name="dni_vendedor"  class="form-control" type="text" tabindex="4" maxlength="18" required />
  </div>

  <div class="col-md-12">
    <label>Domicilio del Vendedor</label>
    <input name="domicilio_vendedor"  class="form-control" type="text" tabindex="5" maxlength="100" required />
  </div>

   <div class="col-md-12">
      <label>Provincia del Vendedor</label>
      <input name="provincia_vendedor"  class="form-control" type="text" tabindex="5" maxlength="50" required />
    </div>

  
  <!--div class="col-md-12">
      <label>Provincia del Vendedor</label>
      <select class="form-control" name="provincia_vendedor" tabindex="6" required >
        <option>Seleccionar ...</option>
        <?
           $provincias="select nombre from provincia";
           $listado=mysqli_query(conexion::obtenerInstancia(), $provincias);
           while( $item = mysqli_fetch_assoc($listado))
           {
            echo "<option value='".$item[id]."'> ".utf8_encode($item[nombre])."</option>";
           }
        ?>
      </select>
    </div-->

    <!--div class="col-md-12">
    <label>Email Vendedor</label>
    <input name="email_vendedor"  class="form-control" type="text" tabindex="7" maxlength="100" required />
  </div-->

  <div class="col-md-12">
    <label>Nombre del Comprador</label>
    <input name="nombre_comprador"  class="form-control" type="text" tabindex="8" maxlength="100" required />
  </div>  

   <div class="col-md-12">
    <label>D.N.I del Comprador</label>
    <input name="dni_comprador"  class="form-control" type="text" tabindex="9" maxlength="18" required />
  </div>

  <div class="col-md-12">
    <label>Domicilio del Comprador</label>
    <input name="domicilio_comprador"  class="form-control" type="text" tabindex="10" maxlength="100" required />
  </div>


   <div class="col-md-12">
      <label>Provincia del Comprador</label>
      <input name="provincia_comprador" class="form-control" type="text" tabindex="10" maxlength="50" required />
      <!--select class="form-control" name="provincia_comprador" tabindex="11" required >
        <option>Seleccionar ...</option>
        <?
           $listado2=mysqli_query(conexion::obtenerInstancia(), $provincias);
           while( $item = mysqli_fetch_assoc($listado2))
           {
            echo "<option value='".$item[id]."'> ".utf8_encode($item[nombre])."</option>";
           }
        ?>
      </select-->
    </div>

    <!--div class="col-md-12">
    <label>Email Comprador</label>
    <input name="email_comprador"  class="form-control" type="text" tabindex="12" maxlength="100" required />
  </div-->

<div class="col-md-12">
    <label>Vehiculo</label>
    <select class="form-control" name="vehiculo"  >
        <option>Seleccionar ...</option>
        <option value="AUTOMOTOR">AUTOMOTOR</option>
        <option value="CAMION">CAMION</option>
        <option value="ACOPLADO">ACOPLADO</option>
        <option value="MOTOVEHICULO">MOTOVEHICULO</option>
        <option value="MAQUINA AGRICOLA">MAQUINA AGRICOLA</option>
        <option value="MAQUINA VIAL">MAQUINA VIAL</option>
        <option value="MAQUINA INDUSTRIAL">MAQUINA INDUSTRIAL,</option>
        <option value="LANCHA">LANCHA</option> 
        <option value="AERONAVE">AERONAVE</option> 
        <option value="OTRO">OTRO.</option> 
      </select>
  </div>  

  <div class="col-md-12">
    <label>Marca</label>
    <input name="marca"  class="form-control" type="text" tabindex="13" maxlength="30" required />
  </div>

  <div class="col-md-12">
    <label>Tipo</label>
    <input name="tipo"  class="form-control" type="text" tabindex="14" maxlength="30" required />
  </div>

  <div class="col-md-12">
    <label>Modelo</label>
    <input name="modelo"  class="form-control" type="text" tabindex="15" maxlength="30" required />
  </div>

    <div class="col-md-12">
    <label>Dominio</label>
    <input name="dominio"  class="form-control" type="text" tabindex="16" maxlength="10" required />
  </div>

  <div class="col-md-12">
    <label>Nº Motor</label>
    <input name="n_motor"  class="form-control" type="text" tabindex="17" maxlength="30" required />
  </div>

  <div class="col-md-12">
    <label>Nº Chasis</label>
    <input name="n_chasis"  class="form-control" type="text" tabindex="18" maxlength="30" required />
  </div>

  <div class="col-md-12">
    <label>Nº Registro</label>
    <input name="n_registro"  class="form-control" type="text" tabindex="19" maxlength="2" required />
  </div>

  <div class="col-md-12">
    <label>Precio Total en Letras</label>
    <input name="precio_letras"  class="form-control" type="text" tabindex="20" maxlength="60" required />
  </div>

  <div class="col-md-12">
    <label>Precio Total en Números</label>
    <input name="precio_numero"  class="form-control" type="text" tabindex="21" maxlength="8" required />
  </div>

  <div class="col-md-12">
    <label>Precio Contado en Números </label>
    <input name="precio_contado"  class="form-control" type="text" tabindex="22" maxlength="8" required />
  </div>

  <div class="col-md-12">
    <label>Precio Saldo en Números </label>
    <input name="precio_saldo"  class="form-control" type="text" tabindex="23" maxlength="8" required />
  </div>

    <div class="col-md-12">
    <label>Número de Cuotas </label>
    <input name="n_cuotas"  class="form-control" type="text" tabindex="24" maxlength="10" required />
  </div>

  <div class="col-md-12">
    <label>Valor de la Cuota </label>
    <input name="valor_cuota"  class="form-control" type="text" tabindex="25" maxlength="8" required />
  </div>

   <div class="col-md-12">
    <label>Vencimiento de la Cuota los dias </label>
    <input name="dia_venc_cuota"  class="form-control" type="text" tabindex="26" maxlength="2" required />
  </div>

    <div class="col-md-12">
    <label>Observaciones </label>
    <textarea rows="3" cols="150" name="observaciones"  class="form-control" type="text" tabindex="27" maxlength="300" required /> </textarea>
  </div>
  
   <div class="col-md-12">
    <label>Nombre Conyuge Vendedor </label>
    <input name="conyuge"  class="form-control" type="text" tabindex="28" maxlength="60" required />
  </div>

   <div class="col-md-12">
    <label>DNI Conyuge </label>
    <input name="dni_conguye"  class="form-control" type="text" tabindex="29" maxlength="80" required />
  </div>

   <div class="col-md-12">
      <label>Tribunales de la Provincia</label>
       <input name="provincia_tribunales"  class="form-control" type="text" tabindex="29" maxlength="50" required />
      <!--select class="form-control" name="provincia_tribunales" tabindex="30" required >
        <option>Seleccionar ...</option>
        <?
           $listado3=mysqli_query(conexion::obtenerInstancia(), $provincias);
           while( $item = mysqli_fetch_assoc($listado3))
           {
            echo "<option value='".$item[id]."'> ".utf8_encode($item[nombre])."</option>";
           }
        ?>
      </select-->
    </div>

    <div class="col-md-12">
    <br>
    <button type="submit" class="myButton" tabindex="31">Imprimir Boleto Compra Venta</button>
  </div>
</form>
</div>  
</div>
</div>   

<script type="text/javascript">
 $(document).ready(function()
  {

     $("#registro").click(function(){
       event.preventDefault();
       var x = document.getElementById("form_carga");
       x.style.display = "block";

       var email = $("#email").val();
       var prov = $("#prov").val();

       if ($("#email").val().lenght =! '') {

         $.post("registrar_persona.php", {email: email, provincia: prov}, function(mensaje) {
          alert(mensaje);
          //$("#div_registro").html(mensaje);
        });//fin post
       } else alert('Error Email no tiene datos.');



       /*$.post("../nuevo.php", {email: email, provincia: prov}, function(mensaje) {
         // alert(mensaje);
          //$("#div_registro").html(mensaje);
              });//fin post*/

      



        });// fin funcion registro
    });
</script>

<br>
 <br>   
<footer style="
    background-color: #124489;
    color: white;
    text-align: center;
    padding: 10px;
">       
      <p>© 2019 <a href="https://www.servijus.com.ar/">SERVIJUS - Servicios Jurídicos Online.</a> Todos los Derechos Reservados.</p>
      <div class="container">
      <div class="row">

        <div class="col-md-4" style="text-align: left;">
        
        <p class="float-left">
          <a href="https://www.servijus.com.ar/nosotros/" target="_blank">Nosotros</a>
        </p>
        <br>
       
        <p class="float-left">
          <a href="https://www.servijus.com.ar/servijus-responsable/" target="_blank">Servijus Responsable</a>
        </p> 

        <br>
       
        <p class="float-left">
          <a href="https://www.fundacion.servijus.com.ar/" target="_blank">Fundación FormaRSE</a>
        </p>  
        <br>
       

        <p class="float-left">
         <a href="terminos.php" target="_blank">Terminos y Condiciones</a>
        </p>
        <br>
       
      
        </div>


        <div class="col-md-4" style="text-align: left;">

        <p class="float-left">
          <a href="https://www.servijus.com.ar/centro-de-ayuda/" target="_blank">Ayuda - Te ayudamos!</a>
        </p>
        <br>
        
        <p class="float-left">
          <a href="https://www.servijus.com.ar/contacto/" target="_blank">Contacto - Te escuchamos!</a>
        </p>
        <br>
       
         <p class="float-left">
          <a href="https://www.servijus.com.ar/urgencias-legales/" target="_blank">Tienes una Urgencia Legal!</a>
        </p>
        <br>
        
       
        <p class="float-left">
          <a href="politicas.php" target="_blank">Politica de Privacidad y Datos Personales</a>
        </p>  
       
        </div>
        
    <div class="col-md-4" style="text-align: center;" >
       
        <p >
          
           <a href="http://qr.afip.gob.ar/?qr=FLxmFfNtNc2nSebSYiq1kA,," target="_F960AFIPInfo"><img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" height="100" width="80"></a>
        </p>
       
         
    </div>
    </div>   
    </div>     
</footer>




