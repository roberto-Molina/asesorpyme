<?php
if( isset($_POST['ciudad']) && !empty($_POST['ciudad']) )
{
  
  $ciudad= utf8_encode($_POST['ciudad']);
  $fecha= utf8_encode($_POST['fecha']);
  $nombre_vendedor= utf8_encode($_POST['nombre_vendedor']);
  $dni_vendedor= utf8_encode($_POST['dni_vendedor']);
  $domicilio_vendedor= utf8_encode($_POST['domicilio_vendedor']);
  $provincia_vendedor= utf8_encode($_POST['provincia_vendedor']);
  $email_vendedor= utf8_encode($_POST['email_vendedor']);
  $nombre_comprador= utf8_encode($_POST['nombre_comprador']);
  $dni_comprador= utf8_encode($_POST['dni_comprador']);
  $domicilio_comprador= utf8_encode($_POST['domicilio_comprador']);
  $provincia_comprador= utf8_encode($_POST['provincia_comprador']);
  $email_comprador= utf8_encode($_POST['email_comprador']);
  $vehiculo= utf8_encode($_POST['vehiculo']);
  $marca= utf8_encode($_POST['marca']);
  $tipo= utf8_encode($_POST['tipo']);
  $modelo= utf8_encode($_POST['modelo']);
  $dominio= utf8_encode($_POST['dominio']);
  $n_motor= utf8_encode($_POST['n_motor']);
  $n_chasis= utf8_encode($_POST['n_chasis']);
  $n_registro= utf8_encode($_POST['n_registro']);
  $precio_letras= utf8_encode($_POST['precio_letras']);
  $precio_numero= utf8_encode($_POST['precio_numero']);
  $precio_contado= utf8_encode($_POST['precio_contado']);
  $precio_saldo= utf8_encode($_POST['precio_saldo']);
  $n_cuotas= utf8_encode($_POST['n_cuotas']);
  $valor_cuota= utf8_encode($_POST['valor_cuota']);
   $dia_venc_cuota= utf8_encode($_POST['dia_venc_cuota']);
  $observaciones= utf8_encode($_POST['observaciones']);
  $conyuge= utf8_encode($_POST['conyuge']);
  $dni_conguye= utf8_encode($_POST['dni_conguye']);
  $provincia_tribunales= utf8_encode($_POST['provincia_tribunales']);

/*require('../libreria/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
*/

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>DEMO SJ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link type="text/css" rel="stylesheet" href="../css/bootstrap-theme.css">
    <link type="text/css" rel="stylesheet" href="../css/bootstrap-theme.css.map">

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.css">

    <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>


<div class="container" style="font-size: 14px">
<p class="text-center lead">BOLETO DE COMPRA VENTA DE VEHICULO</p>
<p class="text-justify">
En la ciudad de  <? echo $ciudad; ?>, el <? echo $fecha; ?>, entre el/a Sr/a. <? echo $nombre_vendedor; ?>, D.N.I. N° <? echo $dni_vendedor; ?>, con domicilio real en <? echo $domicilio_vendedor; ?>, <? echo $provincia_vendedor; ?>,<? echo $email_vendedor; ?> , en adelante el “VENDEDOR”, y el/a Sr/a. <? echo $nombre_comprador; ?> , D.N.I. N° <? echo $dni_comprador; ?>, con domicilio real en <? echo $domicilio_comprador; ?>, <? echo $provincia_comprador; ?>, <? echo $email_comprador; ?> ,  en adelante el “COMPRADOR”, convienen en celebrar el presente Contrato de Compra Venta de Vehículo, con compromiso de transferencia de dominio, el que se ajustará a las siguientes cláusulas:
</p>
<br>
<p class="text-justify">
Primera: Objeto: El VENDEDOR, vende al COMPRADOR y este adquiere un  <? echo $vehiculo; ?>, usado/0 KM, Marca  <? echo $marca; ?>, Tipo <? echo $tipo; ?>, Modelo <? echo $modelo; ?> , Dominio <? echo $dominio; ?>, Motor N° <? echo $n_motor; ?> , Chasis N° <? echo $n_chasis; ?> , en el estado en que se encuentra, y que el COMPRADOR declara conocer y aceptar. 
</p>
<br>
<p class="text-justify">
El VENDEDOR, exhibe en este acto al COMPRADOR, Certificado de Dominio del Automotor, expedido por el DNRPA N° <? echo $n_registro; ?> , donde se encuentra el legajo del vehículo objeto del presente contrato y del cual resulta la titularidad dominial del VENDEDOR, declarando en este acto, que el vehículo no se encuentra ni gravado ni prendado y apto para ser transferido.
</p>
<br>
<p class="text-justify">
Segunda: Precio: El precio convenido asciende a la suma de Pesos <? echo $precio_letras; ?>, ($ <? echo $precio_numero; ?>), el que se abona de la siguiente forma: de contado la suma de $ <? echo $precio_contado; ?>, y el saldo de $ <? echo $precio_saldo; ?>, en <? echo $n_cuotas; ?> cuotas mensuales y consecutivas de $ <? echo $valor_cuota; ?> cada una con vencimientos los días <? echo $dia_venc_cuota; ?> de cada mes. El dinero en efectivo, es entregado en este acto, sirviendo el presente como suficiente recibo de pago y conformidad.
</p>
<br>
<p class="text-justify">
Tercera: Transferencia: El COMPRADOR, se obliga a efectuar la transferencia del automotor a su nombre dentro del plazo de 24 horas, a contar desde la fecha de la firma del presente, caso contrario se realizará la Denuncia de Venta por ante el DNRPA y por ante la Dirección de Rentas del Automotor, una vez vencido el plazo otorgado.
</p>
<br>
<p class="text-justify">
Cuarta: Incumplimiento: En el caso de que cualquiera de las partes no concurrieran por ante el DNRPA que corresponda, a formalizar la Transferencia de Dominio objeto del presente contrato en el plazo previsto en la clausula anterior, se establece como multa por cada día de retraso el 1% del valor del contrato, el que se exigirá judicialmente, sin previa interpelación.
</p>
<br>
<p class="text-justify">
Quinta: Documentación: En este acto, el VENDEDOR, entrega el Título de Dominio, la Cédula Verde, los comprobantes de pago de las Patentes, haciéndose las siguientes observaciones: <? echo $observaciones; ?>.
</p>
<br>
<p class="text-justify">
Sexta: Responsabilidad: A partir de la firma del presente día, el COMPRADOR acepta en forma exclusiva la responsabilidad civil, penal y administrativa, por todas las consecuencias derivadas del uso o posesión del vehículo, eximiendo de todo tipo de responsabilidad al VENDEDOR por el uso indebido del mismo.
</p>
<br>
<p class="text-justify">
Séptima: Consentimiento: El/a Sr/a. <? echo $conyuge; ?>, D.N.I. N° <? echo $dni_conguye; ?>, esposa/o del VENDEDOR y presente en este acto, presta su conformidad a la venta pactada y compromete su presencia ante la firma por ante el DNRPA que corresponda, a los fines de dar su asentimiento conyugal para el acto de transferencia de dominio.
</p>
<br>
<p class="text-justify">
Octava: Jurisdicción: Las partes constituyen domicilio legal para los efectos derivados del presente contrato, en los domicilios denunciados up supra, donde se tendrán por validas todas y cada una de las notificaciones extrajudiciales y/o judiciales con relación al contrato. Asimismo ambas partes, manifiestan que se someterán a la competencia ordinaria de los Tribunales de <? echo $provincia_tribunales; ?>, para todos los efectos derivados del presente boleto de compra venta.
</p>
<br>
<p class="text-justify">
En prueba de conformidad se firman dos ejemplares de un mismo tenor y a un solo efecto.
</p>
<br>

  <div class="row">
    <div class="col-md-6">
        <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>VENDEDOR</td>
          </tr>
        </table>
                                                                            
    </div>
    <div class="col-md-6">
   <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>COMPRADOR</td>
          </tr>
        </table>
    </div>
 </div>
</div>
</div>
<?}?>