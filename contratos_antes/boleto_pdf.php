<?php
if( isset($_POST['ciudad']) && !empty($_POST['ciudad']) )
{
  
  $ciudad= utf8_encode($_POST['ciudad']);  
  $fecha= utf8_encode($_POST['fecha']);  
  $nombre_vendedor= utf8_encode($_POST['nombre_vendedor']);  
  $dni_vendedor= utf8_encode($_POST['dni_vendedor']);  
  $domicilio_vendedor= utf8_encode($_POST['domicilio_vendedor']);  
  $provincia_vendedor= utf8_encode($_POST['provincia_vendedor']) ; 
 // $email_vendedor= utf8_encode($_POST['email_vendedor'])  ;
  $nombre_comprador= utf8_encode($_POST['nombre_comprador']);  
  $dni_comprador= utf8_encode($_POST['dni_comprador'])  ;
  $domicilio_comprador= utf8_encode($_POST['domicilio_comprador'])  ;
  $provincia_comprador= utf8_encode($_POST['provincia_comprador'])  ;
  //$email_comprador= utf8_encode($_POST['email_comprador'])  ;
  $vehiculo= utf8_encode($_POST['vehiculo'])  ;
  $marca= utf8_encode($_POST['marca'])  ;
  $tipo= utf8_encode($_POST['tipo'])  ;
  $modelo= utf8_encode($_POST['modelo']);  
  $dominio= utf8_encode($_POST['dominio']);  
  $n_motor= utf8_encode($_POST['n_motor']) ; 
  $n_chasis= utf8_encode($_POST['n_chasis']);  
  $n_registro= utf8_encode($_POST['n_registro']);  
  $precio_letras= utf8_encode($_POST['precio_letras']);  
  $precio_numero= utf8_encode($_POST['precio_numero']) ; 
  $precio_contado= utf8_encode($_POST['precio_contado']);  
  $precio_saldo= utf8_encode($_POST['precio_saldo'])  ;
  $n_cuotas= utf8_encode($_POST['n_cuotas'])  ;
  $valor_cuota= utf8_encode($_POST['valor_cuota']);  
   $dia_venc_cuota= utf8_encode($_POST['dia_venc_cuota']);  
  $observaciones= utf8_encode($_POST['observaciones'])  ;
  $conyuge= utf8_encode($_POST['conyuge']);  
  $dni_conguye= utf8_encode($_POST['dni_conguye'])  ;
  $provincia_tribunales= utf8_encode($_POST['provincia_tribunales'])  ;

require_once __DIR__ . '../../vendor/autoload.php';  
// Create an instance of the class:
$mpdf = new \Mpdf\Mpdf();  

$html='<div style="font-size: 13px"><h3>BOLETO DE COMPRA VENTA DE VEHICULO</h3>
<p>En la ciudad de '. $ciudad.', el '.$fecha.', entre el/a Sr/a. '. $nombre_vendedor.', D.N.I. N° '. $dni_vendedor.', con domicilio real en '. $domicilio_vendedor.', '. $provincia_vendedor.', en adelante el “VENDEDOR”, y el/a Sr/a. '.  $nombre_comprador .' , D.N.I. N°  '.  $dni_comprador  .', con domicilio real en '.  $domicilio_comprador.','.  $provincia_comprador.', en adelante el “COMPRADOR”, convienen en celebrar el presente Contrato de Compra Venta de Vehículo, con compromiso de transferencia de dominio, el que se ajustará a las siguientes cláusulas:
</p>

<p >
Primera: Objeto: El VENDEDOR, vende al COMPRADOR y este adquiere un '.  $vehiculo    .', usado/0 KM, Marca '.  $marca    .', Tipo  '.  $tipo    .', Modelo  '.  $modelo    .' , Dominio  '.  $dominio    .', Motor N°  '.  $n_motor    .' , Chasis N°  '.  $n_chasis    .' , en el estado en que se encuentra, y que el COMPRADOR declara conocer y aceptar. 
</p>

<p >
El VENDEDOR, exhibe en este acto al COMPRADOR, Certificado de Dominio del Automotor, expedido por el DNRPA N°  '.  $n_registro    .' , donde se encuentra el legajo del vehículo objeto del presente contrato y del cual resulta la titularidad dominial del VENDEDOR, declarando en este acto, que el vehículo no se encuentra ni gravado ni prendado y apto para ser transferido.
</p>

<p >
Segunda: Precio: El precio convenido asciende a la suma de Pesos  '.  $precio_letras    .', ($  '.  $precio_numero    .'), el que se abona de la siguiente forma: de contado la suma de $  '.  $precio_contado    .', y el saldo de $  '.  $precio_saldo    .', en  '.  $n_cuotas    .' cuotas mensuales y consecutivas de $  '.  $valor_cuota    .' cada una con vencimientos los días  '.  $dia_venc_cuota    .' de cada mes. El dinero en efectivo, es entregado en este acto, sirviendo el presente como suficiente recibo de pago y conformidad.
</p>

<p>
Tercera: Transferencia: El COMPRADOR, se obliga a efectuar la transferencia del automotor a su nombre dentro del plazo de 24 horas, a contar desde la fecha de la firma del presente, caso contrario se realizará la Denuncia de Venta por ante el DNRPA y por ante la Dirección de Rentas del Automotor, una vez vencido el plazo otorgado.
</p>

<p>
Cuarta: Incumplimiento: En el caso de que cualquiera de las partes no concurrieran por ante el DNRPA que corresponda, a formalizar la Transferencia de Dominio objeto del presente contrato en el plazo previsto en la clausula anterior, se establece como multa por cada día de retraso el 1% del valor del contrato, el que se exigirá judicialmente, sin previa interpelación.
</p>

<p>
Quinta: Documentación: En este acto, el VENDEDOR, entrega el Título de Dominio, la Cédula Verde, los comprobantes de pago de las Patentes, haciéndose las siguientes observaciones:  '.  $observaciones    .'.
</p>

<p >
Sexta: Responsabilidad: A partir de la firma del presente día, el COMPRADOR acepta en forma exclusiva la responsabilidad civil, penal y administrativa, por todas las consecuencias derivadas del uso o posesión del vehículo, eximiendo de todo tipo de responsabilidad al VENDEDOR por el uso indebido del mismo.
</p>

<p >
Séptima: Consentimiento: El/a Sr/a.  '.  $conyuge    .', D.N.I. N° '.  $dni_conguye    .', esposa/o del VENDEDOR y presente en este acto, presta su conformidad a la venta pactada y compromete su presencia ante la firma por ante el DNRPA que corresponda, a los fines de dar su asentimiento conyugal para el acto de transferencia de dominio.
</p>

<p>
Octava: Jurisdicción: Las partes constituyen domicilio legal para los efectos derivados del presente contrato, en los domicilios denunciados up supra, donde se tendrán por validas todas y cada una de las notificaciones extrajudiciales y/o judiciales con relación al contrato. Asimismo ambas partes, manifiestan que se someterán a la competencia ordinaria de los Tribunales de  '.  $provincia_tribunales    .', para todos los efectos derivados del presente boleto de compra venta.
</p>

<p>
En prueba de conformidad se firman dos ejemplares de un mismo tenor y a un solo efecto.
</p>


 <div >
       <table>
          <tr> 
            <td>........................................................................         
            </td>
            <td>........................................................................</td>
          </tr>
          <tr> 
          <td style="text-align: center  ">VENDEDOR </td>
          <td style="text-align: center  ">COMPRADOR</td>
          </tr>
        </table>
    </div>
 </div>' ;

$css='<style>'.file_get_contents('estilo.css').'</style>' ; 

$mpdf->writeHTML($css);  
$mpdf->writeHTML($html);  

$mpdf->Output();  
exit();  
}
?>