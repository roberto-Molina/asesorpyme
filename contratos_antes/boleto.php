<?
include("../sjadmin/bd/conexion.php");
$provincias="select nombre from provincia";
$listado=mysqli_query(conexion::obtenerInstancia(), $provincias);
/*

if( isset($_GET['contrato_id']) && !empty($_GET['contrato_id']) )
{
 //datos pretamo 
  $contrato_id=$_GET['contrato_id'];
  $datosContrato = contrato::datoscontrato($contrato_id);
  foreach($datosContrato as $item)
  {
    $contrato_id= $item ['contrato_id'];
    $dni=$item ['dni'];
    $domicilio=$item['domicilio'];
    $departamento=$item['departamento'];
    $nombre=$item ['nombre'];
    $telefono=$item ['telefono'];
    $email=$item ['email'];
    $fecha=$item ['contrato_fecha'];  
    $total=$item ['total'];   
    }
   // pasar la fecha
  //echo date('d-m-Y',strtotime($fecha));
  
  $dia=date('d',strtotime($fecha));

  $mes = date('m',strtotime($fecha)); 

  $a=date('Y',strtotime($fecha));

  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); 

  $mes=$meses[$mes-1];  */

  if( isset($_POST['nombre']) && !empty($_POST['nombre']) )
 {

  
  $nombre= utf8_encode($_POST['nombre']);
  $foto =$nuevo_name;
  $domicilio= utf8_encode($_POST['domicilio']);  
  $provincia= $_POST['provincia'];  
  $telefono = $_POST['telefono'];
  $email = $_POST['email'];
  $idioma1= utf8_encode($_POST['idioma1']);
  $idioma2= utf8_encode($_POST['idioma2']);
  $idioma3= utf8_encode($_POST['idioma3']);
  $idioma4= utf8_encode($_POST['idioma4']);
  $habi= utf8_encode($_POST['habi']);
  $perfilpersonal= utf8_encode($_POST['perfilpersonal']);
  $experiencia= utf8_encode($_POST['experiencia']);
  $educacion= utf8_encode($_POST['educacion']);
  
     
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>DEMO SJ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link type="text/css" rel="stylesheet" href="../css/bootstrap-theme.css">
    <link type="text/css" rel="stylesheet" href="../css/bootstrap-theme.css.map">

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.css">

    <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>

<style type="text/css" media="print">
 @page {
         size: A4 landscape; 
         size: auto;/* es el valor por defecto */
         margin: 0.5cm 0.5cm 1.5cm 1.8cm;
        }

@media print {
  body { 
        font-size:22px;
        }
  table {
         font-size: 14px;
        }
#noimprimir {display:none;}
#parte2 {display:none;}
}
</style>

  <div class="container">
 <img src="../img/ENCABEZADO_BCV.jpg" class="img-fluid">

<div id="seleccion">

<div class="container">
<p class="text-center lead">BOLETO DE COMPRA VENTA DE VEHICULO</p>
<p class="text-justify">
En la ciudad de  <input name="ciudad"  type="text" maxlength="60" required placeholder=" NOMBRE DE LA CIUDAD"/>, el <input name="fecha" type="text" required placeholder="FECHA" />, entre el/a Sr/a.<input name="nombre_vendedor"   type="text" tabindex="3" maxlength="100" required placeholder="NOMBRE VENDEDOR" /> , D.N.I. N°  <input name="dni_vendedor"   type="text" tabindex="3" maxlength="100" required placeholder="D.N.I." /> , con domicilio real en  <input name="domicilio_vendedor"   type="text" tabindex="3" maxlength="100" required placeholder="DOMICILIO VENDEDOR" />,<select   name="provincia_vendedor" tabindex="5" required >
        <option> Provincia Vendedor</option>
        <?
           while( $item = mysqli_fetch_assoc($listado))
           {
            echo "<option value='".$item[id]."'> ".utf8_encode($item[nombre])."</option>";
           }
        ?>
      </select>, <input name="email_vendedor"   type="text" tabindex="3" maxlength="100" required placeholder="MAIL VENDEDOR" />, en adelante el “VENDEDOR”, y el/a Sr/a.<input name="nombre_comprador"   type="text" tabindex="3" maxlength="100" required placeholder="NOMBRE COMPRADOR" /> , D.N.I. N° <input name="dni_comprador"   type="text" tabindex="3" maxlength="100" required placeholder="DNI COMPRADOR" />, con domicilio real en  <input name="domicilio_comprador"   type="text" tabindex="3" maxlength="100" required placeholder="DOMICILIO COMPRADOR" />, <select   name="provincia_comprador" tabindex="5" required >
        <option> Provincia Comprador</option>
        <?
           $listado1=mysqli_query(conexion::obtenerInstancia(), $provincias);
           while( $item1 = mysqli_fetch_assoc($listado1))
           {
            echo "<option value='".$item1[id]."'> ".utf8_encode($item1[nombre])."</option>";
           }
        ?>
      </select>,<input name="email_comprador"  type="text" tabindex="3" maxlength="100" required placeholder="MAIL COMPRADOR" /> ,  en adelante el “COMPRADOR”, convienen en celebrar el presente Contrato de Compra Venta de Vehículo, con compromiso de transferencia de dominio, el que se ajustará a las siguientes cláusulas:
</p>
<br>
<p class="text-justify">
Primera: Objeto: El VENDEDOR, vende al COMPRADOR y este adquiere un  <select  name="vehiculo"  >
        <option>Vehiculo</option>
        <option value="AUTOMOTOR">AUTOMOTOR</option>
        <option value="CAMION">CAMION</option>
        <option value="ACOPLADO">ACOPLADO</option>
        <option value="MOTOVEHICULO">MOTOVEHICULO</option>
        <option value="MAQUINA AGRICOLA">MAQUINA AGRICOLA</option>
        <option value="MAQUINA VIAL">MAQUINA VIAL</option>
        <option value="MAQUINA INDUSTRIAL">MAQUINA INDUSTRIAL,</option>
        <option value="LANCHA">LANCHA</option> 
        <option value="AERONAVE">AERONAVE</option> 
        <option value="OTRO">OTRO.</option> 
      </select>, usado/0 KM, Marca <input name="marca"    type="text" tabindex="4" maxlength="20" required placeholder="MARCA" />, Tipo <input name="tipo" type="text" tabindex="4" maxlength="20" required placeholder="TIPO" />, Modelo <input name="modelo"    type="text" tabindex="4" maxlength="20" required placeholder="MODELO" />, Dominio <input name="dominio"    type="text" tabindex="4" maxlength="20" required placeholder="DOMINIO" />, Motor N°  <input name="n_motor"    type="text" tabindex="4" maxlength="20" required placeholder="Nº MOTOR" />, Chasis N° <input name="n_chasis"    type="text" tabindex="4" maxlength="20" required placeholder="Nº CHASIS" />, en el estado en que se encuentra, y que el COMPRADOR declara conocer y aceptar. 
</p>
<br>
<p class="text-justify">
El VENDEDOR, exhibe en este acto al COMPRADOR, Certificado de Dominio del Automotor, expedido por el DNRPA N°  <input name="n_chasis"    type="text" tabindex="4" maxlength="20" required placeholder="Nº REGISTRO" />, donde se encuentra el legajo del vehículo objeto del presente contrato y del cual resulta la titularidad dominial del VENDEDOR, declarando en este acto, que el vehículo no se encuentra ni gravado ni prendado y apto para ser transferido.
</p>
<br>
<p class="text-justify">
Segunda: Precio: El precio convenido asciende a la suma de Pesos  <input name="precio_letras" type="text" tabindex="4" maxlength="20" required placeholder="PRECIO TOTAL EN LETRAS" />, ($ <input name="precio_numero" type="text" tabindex="4" maxlength="20" required placeholder="PRECIO TOTAL NUMEROS" />), el que se abona de la siguiente forma: de contado la suma de $ <input name="precio_contado" type="text" tabindex="4" maxlength="20" required placeholder="PRECIO CONTADO NRO" />, y el saldo de $ <input name="precio_saldo" type="text" tabindex="4" maxlength="20" required placeholder="PRECIO SALDO NRO" />, en <input name="n_cuotas"    type="text" tabindex="4" maxlength="20" required placeholder="NRO DE CUOTAS" /> cuotas mensuales y consecutivas de $ <input name="valor_cuotas" type="text" tabindex="4" maxlength="20" required placeholder="VALOR DE LA CUOTA" /> cada una con vencimientos los días <input name="dia_vto" type="text" tabindex="4" maxlength="20" required placeholder="DIA VENCIMIENTO" /> de cada mes. El dinero en efectivo, es entregado en este acto, sirviendo el presente como suficiente recibo de pago y conformidad.
</p>
<br>
<p class="text-justify">
Tercera: Transferencia: El COMPRADOR, se obliga a efectuar la transferencia del automotor a su nombre dentro del plazo de 24 horas, a contar desde la fecha de la firma del presente, caso contrario se realizará la Denuncia de Venta por ante el DNRPA y por ante la Dirección de Rentas del Automotor, una vez vencido el plazo otorgado.
</p>
<br>
<p class="text-justify">
Cuarta: Incumplimiento: En el caso de que cualquiera de las partes no concurrieran por ante el DNRPA que corresponda, a formalizar la Transferencia de Dominio objeto del presente contrato en el plazo previsto en la clausula anterior, se establece como multa por cada día de retraso el 1% del valor del contrato, el que se exigirá judicialmente, sin previa interpelación.
</p>
<br>
<p class="text-justify">
Quinta: Documentación: En este acto, el VENDEDOR, entrega el Título de Dominio, la Cédula Verde, los comprobantes de pago de las Patentes, haciéndose las siguientes observaciones: <input name="observaciones"    type="text" tabindex="4" maxlength="20" required placeholder="OBSERVACIONES" />.
</p>
<br>
<p class="text-justify">
Sexta: Responsabilidad: A partir de la firma del presente día, el COMPRADOR acepta en forma exclusiva la responsabilidad civil, penal y administrativa, por todas las consecuencias derivadas del uso o posesión del vehículo, eximiendo de todo tipo de responsabilidad al VENDEDOR por el uso indebido del mismo.
</p>
<br>
<p class="text-justify">
Séptima: Consentimiento: El/a Sr/a. <input name="conyuge" type="text" tabindex="4" maxlength="20" required placeholder="NOMBRE CONYUGE" />, D.N.I. N° <input name="dni_conguye" type="text" tabindex="4" maxlength="20" required placeholder="DNI CONYUGE" />, esposa/o del VENDEDOR y presente en este acto, presta su conformidad a la venta pactada y compromete su presencia ante la firma por ante el DNRPA que corresponda, a los fines de dar su asentimiento conyugal para el acto de transferencia de dominio.
</p>
<br>
<p class="text-justify">
Octava: Jurisdicción: Las partes constituyen domicilio legal para los efectos derivados del presente contrato, en los domicilios denunciados up supra, donde se tendrán por validas todas y cada una de las notificaciones extrajudiciales y/o judiciales con relación al contrato. Asimismo ambas partes, manifiestan que se someterán a la competencia ordinaria de los Tribunales de <select   name="provincia_tribunales" tabindex="5" required >
        <option>PROVINCIA DE JURISDICCION</option>
        <?
           $listado2=mysqli_query(conexion::obtenerInstancia(), $provincias);
           while( $item2 = mysqli_fetch_assoc($listado2))
           {
            echo "<option value='".$item2[id]."'> ".utf8_encode($item1[nombre])."</option>";
           }
        ?>
      </select>[PROVINCIA], para todos los efectos derivados del presente boleto de compra venta.
</p>
<br>
<p class="text-justify">
En prueba de conformidad se firman dos ejemplares de un mismo tenor y a un solo efecto.
</p>
<br>

  <div class="row">
    <div class="col-md-6">
        <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>VENDEDOR</td>
          </tr>
        </table>
                                                                            
    </div>
    <div class="col-md-6">
   <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>COMPRADOR</td>
          </tr>
        </table>
    </div>

<a class="btn btn-primary" href="index.php">Cerrar</a>
       <a href="javascript:window.print()" class="btn btn-primary"> Imprimir</a>

 </div>
  
  <div >
   <br>
    <p>
       
    </p>
  </div>