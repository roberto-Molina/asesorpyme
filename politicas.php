<?
include("cabecera.php");
?>
<div class="container">
 <img src="img/ENCABEZADO_PPyDP.jpg" class="img-responsive ">
<br>
<br>
<p class="text-justify">
		POLITICA DE PRIVACIDAD DE SUSCRIPTORES Y USUARIOS.<br>
En SERVIJUS Servicios Jurídicos Online, utilizamos la información personal
brindada por los clientes para procesar, validar y verificar sus suscripciones y
acceso a los temas de consultas legales.
La utilización de la información recibida por SERVIJUS, tiene los siguientes fines:
a) analizar la información en forma general y disociada (nunca individual) sobre los
temas legales consultados por los clientes, sus intereses, y la trazabilidad de la
problemática legal consultada a través de estadísticas; b) el desarrollo de nuevos
servicios jurídicos que satisfagan las necesidades de los clientes; c) brindar
información a la red externa de estudios jurídicos y escribanías asociadas en la
prestación de los servicios legales de SERVIJUS, y d) para mantener la
comunicación con el cliente, vía redes sociales, vía mail, whatsapp o
telefónicamente, a fines de relevar inquietudes, seguimientos de los servicios
prestados y/o informar a los mismos sobre los nuevos servicios jurídicos de
SERVIJUS.
El cliente tiene derecho a peticionar a SERVIJUS, la baja del servicio de asesoría
personal y de no recibir más información sobre los servicios jurídicos online
prestados, según lo dispuesto por el Artículo 16 de la Ley 25.326. Los datos
personales recolectados no serán difundidos ni empleados para un fin distinto a
los enunciados up supra en el presente pliego.
En relación a lo dispuesto por el Artículo 5 de la Ley 25.326, el tratamiento de
datos y el envío de información por correo electrónico que SERVIJUS realiza a
sus clientes, cuenta con su expreso y libre consentimiento, el cual es emitido e
informado al momento de la suscripción a nuestros servicios jurídicos online.
Los clientes cuentan con la posibilidad de editar en cualquier momento la
información que han suministrado en la sección de su perfil personal, al que solo
tiene acceso a través de su usuario y contraseña ingresada al momento de la
registración y posterior suscripción. No obstante ello, el titular (cliente) de los datos
personales tiene la facultad de ejercer el derecho de acceso a los mismos en
forma gratuita a intervalos no inferiores a 6 meses, salvo que se acredite un
interés legítimo al efecto, conforme lo establecido en el art. 14, inc. 3 de la Ley
25.326. Conforme Art 1 de la Disposición 10/2008.
En cumplimiento del Artículo 10 de la Ley 25.326, SERVIJUS mantendrá absoluta
confidencialidad de los datos personales brindados por los clientes, guardando la
información suministrada bajo extremas medidas de seguridad en los servidores
contratados y/o propios, en cumplimiento del Artículo 9 de la Ley 25.326.
No obstante, el cumplimiento efectivo de la normativa en materia de protección y
seguridad de datos por parte de SERVIJUS, ésta no se hace responsable por la
divulgación de información personal online, que los clientes realicen en internet y
que pueda ser utilizada por otros en la red, ni por los daños y perjuicios que
genere la divulgación.
Las bases de datos de SERVIJUS Consultora se encuentran registradas en la
Dirección Nacional de Protección de Datos Personales del Ministerio de Justicia y
Derechos Humanos de la Presidencia de la Nación (República Argentina), en
cumplimiento de lo dispuesto en el Artículo 3 de la Ley 25.326.
Cualquier reclamo y/o consulta puede ser dirigido a nuestra sección de Consultas
y Sugerencia y/o ante La DIRECCIÓN NACIONAL DE PROTECCIÓN DE DATOS
PERSONALES, quien es el Órgano de Control establecido por la Ley 25.326, y
quien tiene la atribución de atender las denuncias y reclamos que se interpongan
con relación al incumplimiento de las normas sobre protección de datos
personales. Conforme Art 2 de la Disposición 10/2008.
ART. 6°, Ley 25.326 (Información). Cuando se recaben datos personales se
deberá informar previamente a sus titulares en forma expresa y clara:
a) La finalidad para la que serán tratados y quiénes pueden ser sus destinatarios o
clase de destinatarios;
b) La existencia del archivo, registro, banco de datos, electrónico o de cualquier
otro tipo, de que se trate y la identidad y domicilio de su responsable;
c) El carácter obligatorio o facultativo de las respuestas al cuestionario que se le
proponga, en especial en cuanto a los datos referidos en el artículo siguiente;
d) Las consecuencias de proporcionar los datos, de la negativa a hacerlo o de la
inexactitud de los mismos;
e) La posibilidad del interesado de ejercer los derechos de acceso, rectificación y
supresión de los datos.
	</p>

	<a href="CODIGO_QR-CERTIFICADO-20206552477--RL-2018-68254687-APN-DNPDP.pdf" target="-blank">Certificado APN DNPDP</a>
  
</div>
</div><!-- fin container-->
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<?
include ("pie.php");
?>  
