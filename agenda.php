<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");
$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';

?>



<div class="container">
    <div class="row">
        	<div class="row mt-5 breadcrumbs">
		    <div cass="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<a href="index.php" class="pill pillblanco mr-1">Volver al Directorio</a>

		    </div>
		    </div>
	</div>    
    <br>   
    <div class="row">
   		<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h1 class="mb-4">AGENDA EMPRESARIA</h1>
			</div>
    </div>

    <div class="row mt-5">
     <?php
      $eventos="SELECT
      evento.`titulo` AS titulo,
                evento.`detalle` AS detalle,
                evento.`lugar` AS lugar,
                evento.`domicilio` AS domicilio,
                evento.`fecha` AS fecha,
                evento.`hora` AS hora,
                evento.`foto` AS foto,
                evento.`costo` AS costo,
                evento.`precio` AS precio,               
                evento.`modalidad` AS modalidad,
                evento.`inscripcion` AS inscripcion,
                
                organizador.`nombre` AS organizador,
                coordinador.`nombre` AS coordinador,
                provincia.`nombre` AS provincia
                FROM
                    `evento`
                    INNER JOIN `organizador` 
                        ON (`evento`.`organizador_id` = `organizador`.`id`)
                    INNER JOIN `coordinador` 
                        ON (`evento`.`coordinador_id` = `coordinador`.`id`)
                    INNER JOIN `provincia` 
        ON (`provincia`.`id` = `evento`.`provincia_id`)
            ORDER BY fecha;";
            
            
      $i=1;
      $listado=mysqli_query(conexion::obtenerInstancia(), $eventos);
      while( $item = mysqli_fetch_assoc($listado))
      {
          $i++;
      ?>
    <div class="text-center col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">   
      <div class="card text-center">
        <div class="card-header">
          <img class="card-img-top" src="https://www.asesoronline.servijus.com.ar/sjadmin/evento_imagen/<?php echo $item['foto'];?>" >
    
        </div>
        <div class="card-body">
          <h5 class="card-title"><?php echo utf8_decode($item['titulo']);?></h5>
           <?php echo date("d/m/Y", strtotime($item['fecha']));?>
           <?php echo date("H:i", strtotime($item['hora']));?>
         
          <p class="card-text"><?php echo utf8_encode($item['costo']);?></p>
           <p class="card-text"><?php echo utf8_encode($item['provincia']);?></p>
          
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#evento<?echo $i;?>">
          Ver Evento
        </button>
        </div>
      </div>
      <br>
     </div> 
     
     <!-- Modal -->
<div class="modal fade" id="evento<?echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Evento:  <?php echo utf8_decode($item['costo']);?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
          <h5 class="card-title"><?php echo utf8_decode($item['titulo']);?></h5>
          <p class="card-text"><?php echo utf8_decode($item['detalle']);?></p>
          <h5 class="card-text">Fecha: <?php echo date("d/m/Y", strtotime($item['fecha']));?> - <?php echo date("H:i", strtotime($item['hora']));?> hs.</h5>
          <p class="card-text">Acceso: <?php echo utf8_decode($item['costo']);?> - Arancel: <?php echo utf8_decode($item['precio']);?></p>
         
          <p class="card-text">Modalidad: <?php echo utf8_decode($item['modalidad']);?></p>
          <!--p class="card-text">Hora: <?php echo date("H:i", strtotime($item['hora']));?></p-->
          <p class="card-text">Lugar/Plataforma: <?php echo utf8_encode($item['lugar']);?></p>
          <p class="card-text">Domicilio/Link: <?php echo utf8_encode($item['domicilio']);?></p>
          <p class="card-text">Provincia: <?php echo utf8_encode($item['provincia']);?></p>
          <p class="card-text">Organizador: <?php echo utf8_encode($item['organizador']);?></p>
          <p class="card-text">Coordinador: <?php echo utf8_encode($item['coordinador']);?></p>
   
      </div>
      <div class="modal-footer">
        <?
          $pos = strpos($item['inscripcion'], 'http');

            // Nótese el uso de ===. Puesto que == simple no funcionará como se espera
            // porque la posición de 'a' está en el 1° (primer) caracter.
            if ($pos === false)
            {
                //echo "La cadena '$findme' no fue encontrada en la cadena '$mystring'";
                ?>
                <p class="card-text">Inscripción: <?php echo utf8_encode($item['inscripcion']);?></p>
                <?
            } else {
               // echo "La cadena '$findme' fue encontrada en la cadena '$mystring'";
                //echo " y existe en la posición $pos";
                ?>
                <p class="centre"> <a href="<?php echo utf8_encode($item['inscripcion']);?>"target="_blank" class="btn btn-primary">Inscripción</a></p>
                <?
            }
            
          ?>
     
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>
<!-- fin Modal -->
      
      
      
      
      
      
      
      <?
      }//fin del while
      ?>
  
</div><!--row-->
<br>
</div>
</div>



<!--script> 
function vermas(id){
if(id=="mas"){
document.getElementById("desplegar").style.display="block";   
document.getElementById("mas").style.display="none"; 
}
else{
document.getElementById("desplegar").style.display="none";
document.getElementById("mas").style.display="inline";
}
}
</script>

</head>

<body>
  <div>
    <div class="parrafo">
 <h2>El águila y la zorra </h2>
 <p>
 Un águila y una zorra que eran muy amigas decidieron vivir juntas 
 con la idea de que eso reforzaría su amistad. Entonces el águila escogió 
 un árbol muy elevado para poner allí sus huevos, mientras que la zorra soltó 
 a sus hijos bajo unas zarzas sobre la tierra al pie del mismo árbol. 
 Un día que la zorra salió a buscar su comida, el águila, que estaba 
 hambrienta cayó sobre las zarzas, se llevó a los zorruelos, y entonces ella 
 y sus crías se regozijaron con un banquete. 
 <a href="#" onclick="vermas('mas');" id="mas">... [leer más]</a>
 </p>
 <p id="desplegar" style="display:none;">
  Regresó la zorra y más le dolió el no poder vengarse, que saber de la 
  muerte de sus pequeños; ¿Cómo podría ella, siendo un animal terrestre,
  sin poder volar, perseguir a uno que vuela? Tuvo que conformarse con
 el usual consuelo de los débiles e impotentes: maldecir desde lejos
  a su enemigo.
  <br />
  Mas no pasó mucho tiempo para que el águila recibiera el pago de su 
traición contra la amistad. Se encontraban en el campo unos pastores
  sacrificando una cabra; cayó el águila sobre ella y se llevó 
  una víscera que aún conservaba fuego, colocándola en su nido. 
  Vino un fuerte viento y transmitió el fuego a las pajas, ardiendo 
 también sus pequeños aguiluchos, que por pequeños aún no sabían volar, 
  los cuales se vinieron al suelo. Corrió entonces la zorra, y tranquilamente
 devoró a todos los aguiluchos ante los ojos de su enemiga. <br />
  <b>“Nunca traiciones la amistad sincera, pues si lo hicieras, tarde 
  o temprano del cielo llegará el castigo”.</b>
  <a href="#" onclick="vermas('menos');" id="menos">... [leer menos]</a>
  </p>
</div>
</div>
</body>
</html-->


<br>
<br>
<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function()
  {
  // 02/05/2019 llenar combo temas
  //buscar por tema
  $('#categoria_id').on('change',function()
  {
        vid = $(this).val()
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'lista_subcategoria.php',
          data: { cat: vid},
          success: function(data){
            if (data)
            {
                $('#subcategoria_id').html(data);
               //$('#div_dinamico').html(data);
            }
           }
       });//fin ajax
     });//fin  
     
      $('#valor_buscado').on('keyup',function()
     {
    
      var MIN_LENGTH = 3;
      var texto = $("#valor_buscado").val();
      //alert(vkeyword);
      if (texto.length >= MIN_LENGTH)
      {
       //$('#botonMensaje').attr("disabled", false);
       $.get("buscarportema.php", { textotema: texto } )
        .done(function( data ) {
           //$("#capa").show();
           $('#listado_temas').html(data);
        });
      }

    });



 });
</script>
</div><!--content-->
<?php
include ("contadortemas.php");
include ("pie.html");
?>