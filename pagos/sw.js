    const urlsToCache = [
        '/pagos/index.html',
    ];

    self.addEventListener('install', (event) => {
        // Perform install steps
        // Perform install steps
        console.log("Arrancamos el cache");
        event.waitUntil(
        caches.open('servijus-cache-v2')
          .then((cache) => {
            return cache.addAll(urlsToCache);
          })
        )
    });

    self.addEventListener('fetch', (event) => {
        // Handle requests
    });

    self.addEventListener('activate', (event) => {
        // Clean up old cache versions
    });