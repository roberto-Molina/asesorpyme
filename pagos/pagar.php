<?php
    require_once ("sdkphp/lib/mercadopago.php");
    $mp = new MP("2272408881629340", "0DXXqatHI5LKLCpFsaWHuKxLtcKx9oL3");

    /*
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";
    exit();
    */

    /*
	if($_SERVER["HTTPS"] != "on")
	{
	    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
	    exit();
	}
    */

    if (isset($_POST["monto"]) && !empty($_POST["monto"]))
    {
        /* ################################################ MAIL ######################## */
        /* ################################################ MAIL ######################## */

        date_default_timezone_set('America/Argentina/San_Juan');
        require 'phpmailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        //Enable SMTP debugging. 
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();            
        //Set SMTP host name                          
        $mail->Host = "servijus.com.ar";
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;                          
        //Provide username and password     
        $mail->Username = "correo@servijus.com.ar";
        $mail->Password = "Ru7u1ZbEL3";
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";
        //$mail->SMTPSecure = true;
        //$mail->SMTPAutoTLS = true;
        //Set TCP port to connect to 
        $mail->Port = 587;
        //$mail->SMTPDebug = 2;
        $mail->From = "correo@servijus.com.ar";
        $mail->FromName = "Correo - Servijus";

        $cnombre = $_POST["nombre"];
        $cdni = $_POST["dni"];
        $cmotivo = $_POST["motivo"];
        $cmonto = $_POST["monto"];

        $mail->addAddress("joseluis.sanchez.trabajo@gmail.com", "Jose Luis Sanchez");
        $mail->isHTML(true);
        $mail->Subject = "Notificacion de pago iniciado";
        $mail->Body = "<i>Se ha completado el formulario en el sitio de pagos. Los datos ingresados son:</i><br /><br />";
        $mail->Body .= "Nombre y apelido: ".$cnombre."<br />";
        $mail->Body .= "DNI: ".$cdni."<br />";
        $mail->Body .= "Motivo: ".$cmotivo."<br />";
        $mail->Body .= "Monto: ".$cmonto."<br />";
        $mail->Body .= "<br />PD: Esto NO significa que el pago haya sido realizado y/o acreditado";
        $mail->AltBody = "Se ha completado el formulario en el sitio de pagos. Los datos ingresados son\r\n";
        $mail->AltBody .= "\r\nNombre y apelido: ".$cnombre."\r\n";
        $mail->AltBody .= "DNI: ".$cdni."\r\n";
        $mail->AltBody .= "Motivo: ".$cmotivo."\r\n";
        $mail->AltBody .= "Monto: ".$cmonto."\r\n";
        $mail->AltBody .= "\r\nPD: Esto NO significa que el pago haya sido realizado y/o acreditado";
        if(!$mail->send()) 
        {
            //echo "Mailer Error: " . $mail->ErrorInfo;
        } 
        /* ################################################ MAIL ######################## */
        /* ################################################ MAIL ######################## */

        $preference_data = array(
            "items" => array(
                array(
                    "title" => "Pago de Servicio",
                    "description" => $_POST["motivo"],
                    "quantity" => 1,
                    "currency_id" => "ARS",
                    "unit_price" => (float)$_POST["monto"]
                )
            ),
            "back_urls" => array(
                "success" => "https://www.servijus.com.ar/pagos/pagar.php?mensaje=exito",
                "failure" => "https://www.servijus.com.ar/pagos/pagar.php?mensaje=fallo",
                "pending" => "https://www.servijus.com.ar/pagos/pagar.php?mensaje=pendiente"
            ),
            "auto_return" => "approved",
            "notification_url" => "https://www.servijus.com.ar/pagos/ipn.php",
            "expires" => false,
            "expiration_date_from" => null,
            "expiration_date_to" => null                
        );
        $preferenceResult = $mp->create_preference($preference_data);
        //echo "<pre>";
        //echo json_encode($preferenceResult, JSON_PRETTY_PRINT);
        //echo "</pre><br />";
        //echo "<pre>";
        //print_r($preferenceResult["response"]["sandbox_init_point"]);
        //echo "</pre><br />";
    }   
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- Head -->
<head>
    <title>Servicio de Cobros - Servijus</title>

    <!-- Meta -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../consultas/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../consultas/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../consultas/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="manifest" href="manifest.json">

    <!-- Web Fonts -->
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700%7COpen+Sans:300,400,600,700" rel="stylesheet">

    <!-- Bootstrap Styles -->
    <link rel="stylesheet" type="text/css" href="../consultas/assets/vendors/bootstrap/css/bootstrap.min.css">

    <!-- Theme Styles -->
    <link rel="stylesheet" type="text/css" href="../consultas/assets/css/styles.css">

    <style type="text/css" media="screen">
        .help-block {
            margin-top:5px;
        }
        .with-errors {
            color:red;
            font-weight: bold;
        }
    </style>
</head>
<!-- End Head -->

<body>
    <header role="header">
        <!-- Navbar Light -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand mr-4" href="index.php">
                    <img src="../consultas/logoservijus.jpg" alt="Servijus">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" 
                        data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" 
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item active mr-3">
                            <a class="nav-link" href="/pagos">Boton de Pago <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item mr-3">
                            <a class="nav-link" href="/consultas">Pago de Membresia</a>
                        </li>
                        <li class="nav-item mr-3">
                            <a class="nav-link" href="https://www.servijus.com.ar">Servijus Consultora</a>
                        </li>
                        <li class="nav-item mr-3">
                            <a class="nav-link" href="https://www.asesorpyme.servijus.com.ar">Asesor Pymes</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar Light -->
    </header>


    <main role="main">

        <section>
            <div class="container">
                <hr class="my-2">
                <div>
                    <h1 class="h2 mb-3 text-center">Pagos por Mercado Pago</h1>
                </div>
                <hr class="my-2">
            </div>
        </section>

        <section class="mt-4">
            <div class="container">
                
                <?php 
                    if (isset($_GET["mensaje"]) && !empty($_GET["mensaje"]))
                    {
                ?>

                        <?php 
                            if($_GET["mensaje"] == "exito") {
                        ?>
                                <div class="alert bg-success text-white fade show rounded-0" role="alert">
                                    <div class="container d-flex">
                                        <div class="alert__icon mr-3">
                                            <i class="fas fa-check-circle"></i>
                                        </div>
                                        <div class="align-self-center mr-3">El pago ha sido generado y acreditado exitosamente</div>
                                        <div class="ml-auto">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        ?>

                        <?php 
                            if($_GET["mensaje"] == "fallo") {
                        ?>
                                <div class="alert bg-danger text-white fade show rounded-0" role="alert">
                                    <div class="container d-flex">
                                        <div class="alert__icon mr-3">
                                            <i class="fas fa-minus-circle"></i>
                                        </div>
                                        <div class="align-self-center mr-3">Hubo algun error procesando el pago. Intente nuevamente</div>
                                        <div class="ml-auto">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        ?>

                        <?php 
                            if($_GET["mensaje"] == "pendiente") {
                        ?>
                                <div class="alert bg-info text-white fade show rounded-0" role="alert">
                                    <div class="container d-flex">
                                        <div class="alert__icon mr-3">
                                            <i class="fas fa-info-circle"></i>
                                        </div>
                                        <div class="align-self-center mr-3">El pago y/o acreditación se encuentran pendientes. Pronto recibirá mas información</div>
                                        <div class="ml-auto">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        ?>

                <?php
                    }
                ?>
                
            </div>
            
        </section>

        <section class="mt-7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="index.html" rel="button" class="btn btn-primary btn-lg btn-block">Realizar un nuevo pago / Volver a Intentar</a>
                    </div>
                </div>
            </div>        
        </section>

    </main>

    <footer class="bg-light u-content-space-top pb-4" role="footer">
        <div class="container-fluid">
            <div class="row px-md-3">
                <hr class="mb-4" style="width:75%;">
            </div>
            <div class="row px-md-3">
                <div class="col-md-12 text-center text-md-center mb-3 mb-md-0">
                    <small>© <a href="https://www.servijus.com.ar">SERVIJUS Consultora</a>. Todos los derechos reservados.</small>
                </div>                
            </div>
        </div>
    </footer>

    <script src="sw.js"></script>

    <!-- Global Vendor -->
    <script src="../consultas/assets/vendors/jquery.min.js"></script>
    <script src="../consultas/assets/vendors/jquery.migrate.min.js"></script>
    <script src="../consultas/assets/vendors/popper.min.js"></script>
    <script src="../consultas/assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="../consultas/assets/js/validator.min.js"></script>
    
    <!-- Theme Settings and Calls -->
    <script src="../consultas/assets/js/global.js"></script>
    <!-- END JAVASCRIPTS -->

    <script src="//secure.mlstatic.com/mptools/render.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        $MPC.openCheckout ({
            url: "<?php echo $preferenceResult["response"]["init_point"]; ?>",
            mode: "modal"
        });        
    </script>

    <script type="text/javascript"> 
        // jQuery    
        $(document).ready(function() {
        });        
    </script>

</body>
<!-- End Body -->
</html>