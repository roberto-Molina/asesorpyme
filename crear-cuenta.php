<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>
<style type="text/css">
.ocultar {
    display: none;
}
 
.mostrar {
    display: block;
}
</style>
<div class="acceso espacio gris">
	<div class="container">

		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<div id="div-registro"></div>
			</div>
		</div>		

		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<h2 class="mb-4">
					Crear Cuenta en Servijus
				</h2>
				<h3 class="volanta">Para darte de alta completar el formulario</h3>
			</div>

			
			<div class="text-right col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">




				<span class="text-dark mr-3"><strong>¿Ya tienes cuenta?</strong></span>
				<a href="iniciar-sesion.php" class="boton botonazul">Inicia Sesión</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<hr/>
			</div>
		</div>

		<div class="row mt-4">
			<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">

				<form class="d-block mt-5" id="form">

				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Ingresar Correo Electrónico:</strong></label>
				    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" autofocus>
				  </div>
				  
				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Crear una Clave de 8 digitos:</strong></label>
				    <input type="password" class="form-control" name="password" id="password" >
				    <p id="passstrength"></p>
				  </div>

				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Repitir la Clave ingresada:</strong></label>
				    <input type="password" class="form-control" name="password2" id="password2" >
				  </div>


				  <div id="msg"></div>
 
<!-- Mensajes de Verificación -->
<div id="error" class="alert alert-danger ocultar" role="alert">
    Las Contraseñas no coinciden, vuelve a intentar !
</div>
<div id="ok" class="alert alert-success ocultar" role="alert">
    Las Contraseñas coinciden ! (Procesando formulario ... )
</div>

				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Seleccionar la Plataforma de Servicios: (Persona, Empresa, Profesional)</strong></label>
					   <select class="form-control" name="tipo" id="tipo" tabindex="1" required >
			            <option>Seleccionar ...</option>
			            <option value='Persona'>Persona</option>
			            <option value='Pyme'>Empresa</option>
			            <option value='Profesional'>Profesional</option>
		        </select>
				  </div>

				  <div class="form-group">
					<label for="exampleFormControlSelect1"><strong>Seleccionar Provincia de Residencia:</strong></label>
					     <select class="form-control" name="provincia" id="provincia" tabindex="1" required >
			            <option>Seleccionar ...</option>
			            <?php
			            $sql="select id, nombre from provincia";
			            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
			              foreach($provincias as $item)
			              {
			                echo "<option value='".$item['id']."'> ". utf8_encode($item['nombre'])."</option>";
			              }
			        ?>
			        </select>
				  </div>
				  
				  <button id="registrar" class="boton mt-3">Crear Cuenta Servijus</button>
				</form>
				
			</div>
			<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
			    <br>
			    <br>
			    
				<img src="_img/registrate-gratis.jpg" alt="" class="d-block w-100"/>
			</div>
		</div>
	</div>
</div>

<?php
include("contadortemas.php");
include("pie.html");
?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="_js/jquery-3.4.1.slim.min.js"></script>
<script src="_js/popper.min.js"></script>
<script src="_bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<script src="js/jquery-1.10.2.js"></script>
<script type="text/javascript">
 $(document).ready(function()
   {
    
    v_boton ="REGISTRATE";
    $.post("registrar_boton_pyme.php", {boton: v_boton}, function(mensaje) {
        });//fin post


     $("#registrar").click(function(){
       event.preventDefault();
       var email = $("#email").val();
       var prov = $("#provincia").val();
       var password = $("#password").val();
       var tipo = $("#tipo").val();

       if (verificarPasswords())
       {

				$.post("nuevo.php", {email: email, provincia: prov, password: password, tipo: tipo}, function(respuesta)
				        {
				          //alert(mensaje);
				          //$("#div_registro").html(mensaje);
				          if (respuesta.estado === "ok")
				          {
									console.log(JSON.stringify(respuesta));
									var tipo = respuesta.tipo,
									mensaje = respuesta.mensaje;
									$("#div-registro").html("<div class='alert alert-success' role='alert'>  <h4 class='alert-heading'>La cuenta fue creada!!!</h4>  <p>Ya puede ingresar a su cuenta.</p>  <hr>  <a href='"+tipo+"' class='boton botonazul'>Inicia Sesión</a></div>");
								}
				        });//fin post
       }
        });// fin funcion registro

     $('#password').keyup(function(e) {
     var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
     var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
     var enoughRegex = new RegExp("(?=.{6,}).*", "g");
     if (false == enoughRegex.test($(this).val()))
     {
     	 			$('#passstrength').empty();
     	 			$("#passstrength").removeClass();
     	 			$('#passstrength').addClass('alert alert-primary');
            $('#passstrength').html('8 caracteres como minimo.');

     } else if (strongRegex.test($(this).val())) {
     				$('#passstrength').empty();
     				$("#passstrength").removeClass();
             $('#passstrength').addClass('alert alert-success');;
             $('#passstrength').html('Seguridad Fuerte!');
     } else if (mediumRegex.test($(this).val())) {
     				$('#passstrength').empty();
     				$("#passstrength").removeClass();
             $('#passstrength').addClass('alert alert-warning');;
             $('#passstrength').html('Seguridad Media!');
     } else {
     				$('#passstrength').empty();
     				$("#passstrength").removeClass();
             $('#passstrength').addClass('alert alert-danger');;
             $('#passstrength').html('Seguridad Débil!');
     }
     return true;
});



    function verificarPasswords()
    {
 
    // Ontenemos los valores de los campos de contraseñas 
    password = document.getElementById('password');
    password2 = document.getElementById('password2');

   
 
    // Verificamos si las constraseñas no coinciden 
    if (password.value != password2.value) {
 
        // Si las constraseñas no coinciden mostramos un mensaje 
        document.getElementById("error").classList.add("mostrar");
 
        return false;
    } else {
 
         return true;
    }
 
}
     });
</script>

</body>
</html>