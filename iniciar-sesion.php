<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>

<div class="acceso espacio gris">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<h2 class="mb-4">
					Ingresar a la Plataforma
				</h2>
			</div>
			<div class="text-right col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<span class="text-dark mr-3"><strong>¿No tienes cuenta en Servijus?</strong></span>
				<a href="crear-cuenta.php" class="boton">Crear Cuenta en Servijus</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<hr/>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
				
				<form class="d-block mt-5" method="post" action="validar_cliente_pyme.php">
				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Correo electrónico:</strong></label>
				    <input type="email" class="form-control" name="email" type="email" autofocus aria-describedby="emailHelp" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1"><strong>Contraseña:</strong></label>
				    <input type="password" class="form-control" name="password" required>
				  </div>
				  <div class="row mt-4">
				  	<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
				  		<button type="submit" class="boton">Ingresar</button>
				  	</div>
					<div class="text-right col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
						<a href="recuperar-contrasena.php" class="d-block mt-2"><small><strong>Olvidé mi contraseña</strong></small></a>
					</div>
				  </div>
				</form>
				
			</div>
			<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
				<img src="_img/inicia-sesion.jpg" alt="" class="d-block w-100"/>
			</div>
		</div>
	</div>
</div>
<?php
include("contadortemas.php");
include("pie.html");
?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="_js/jquery-3.4.1.slim.min.js"></script>
<script src="_js/popper.min.js"></script>
<script src="_bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
 $(document).ready(function()
  {
    v_boton ="INICIAR SESION";
    $.post("registrar_boton_pyme.php", {boton: v_boton}, function(mensaje) {
        });//fin post
  });
</script>
</body>
</html>