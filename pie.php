<footer >       
      <p>© 2019 <a href="https://www.servijus.com.ar/">SERVIJUS - Servicios Jurídicos Online.</a> Todos los Derechos Reservados.</p>
      <div class="container">
      <div class="row">

        <div class="col-md-4" style="text-align: left;">
        
        <p class="float-left">
          <a href="https://www.servijus.com.ar/nosotros/" target="_blank">Nosotros</a>
        </p>
        <br>
        <br>
        <p class="float-left">
          <a href="https://www.servijus.com.ar/servijus-responsable/" target="_blank">Servijus Responsable</a>
        </p> 

        <br>
        <br>
        <p class="float-left">
          <a href="https://www.fundacion.servijus.com.ar/" target="_blank">Fundación FormaRSE</a>
        </p>  
        <br>
        <br>

        <p class="float-left">
         <a href="terminos.php" target="_blank">Terminos y Condiciones</a>
        </p>
        <br>
        <br>
      
        </div>


      	<div class="col-md-4" style="text-align: left;">

        <p class="float-left">
          <a href="https://www.servijus.com.ar/centro-de-ayuda/" target="_blank">Centro de Ayuda</a>
        </p>
        <br>
        <br>

        <p class="float-left">
          <a href="https://www.servijus.com.ar/contacto/" target="_blank">Consultas y Sugerencias</a>
        </p>
        <br>
        <br>

         <p class="float-left">
          <a href="https://www.servijus.com.ar/urgencias-legales/" target="_blank">Tienes una Urgencia Legal!</a>
        </p>
        <br>
        <br>
       
        <p class="float-left">
          <a href="politicas.php" target="_blank">Politica de Privacidad y Datos Personales</a>
        </p>  
       
        </div>
        
    <div class="col-md-4" style="text-align: center;" >
       
        <p >
          
           <a href="https://qr.afip.gob.ar/?qr=FLxmFfNtNc2nSebSYiq1kA,," target="_F960AFIPInfo"><img src="https://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" height="100" width="80"></a>
        </p>
       
         
    </div>
    </div>   
    </div>     
</footer>