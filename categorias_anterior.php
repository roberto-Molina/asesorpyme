<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.php");
include ("menu.php");
?>
  
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Categorias</li>
  </ol>
</nav>

<h3>Categorias de Consultas</h3>
<hr>
  <div class="row">
  <?php
         /* verificar persona habilitada o activa*/
          $categorias="SELECT * FROM categoria where habilitada='SI'";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
           ?>
                <div class="col-md-4">
                  <div class="card mb-4 box-shadow">
                    
                    <a href="subcategoria.php?cat=<?php echo $item['id'];?>" >
                      <img src="sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top"  alt="nombre" >
                    </a>

                    <div class="card-body">
                      <p class="text-justify"><?php echo utf8_decode($item['resumen']);?></p>
                   
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                           <a href="subcategoria.php?cat=<?php echo $item['id'];?>" >Ver más ....</a>
                        </div>
                       </div>
                    </div>
                   </div>
                </div>
          <?
             }//fin del while
          ?>
  
</div><!--row-->

<br>
<br>
<h3>Mis Servicios</h3>
<hr>
<div class="container">
  <div class="row">
    <!--p>servicios gratuitos</p-->
    <div class="col-md-6">
      <div class="card mb-4 box-shadow">
            <a href="curriculum/nuevo.php" target="_blank"> 
                <img class="card-img-top" src="img/GRATIS_CV.jpg" >
            </a>

            <div class="card-body">
              <p class="card-text"></p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                         <a href="curriculum/formulario_curriculum.php" class="myButton" target="_blank">Imprimi tu curriculum </a>
                    </div>
                </div>
            </div>
      </div>
    </div>
 
    <div class="col-md-6">
      <div class="card mb-4 box-shadow">
            <a href="contratos/formulario_contrato.php" target="_blank">
                <img class="card-img-top" src="img/GRATIS_CTTO.jpg" >
            </a>  
            <div class="card-body">
                  <p class="card-text"></p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <!--button type="button"  onclick="location.href='contratos/nuevo.php';" >Imprimi Contratos</button-->
                      <a href="contratos/formulario_contrato.php" class="myButton" target="_blank"> Imprimir Contratos </a>
                    </div>
                  </div>
            </div>
      </div>
    </div>
  </div>  
</div><!-- fin container-->

<!--div class="container">
 
       <a href="curriculum/nuevo.php" class="myButton" target="_blank">Imprimi tu curriculum </a>
                   
    
     
          <a href="contratos/nuevo.php" class="myButton" target="_blank"> Imprimir Contratos </a>
  
</div><!-- fin container-->
<br>
<br>

  
<script type="text/javascript">
 $(document).ready(function()
  {
    //buscar subcategoria 
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'subcategoria.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
     });//fin

 });
</script>
</div><!--content-->
<?php
include ("pie.php");
?>

    