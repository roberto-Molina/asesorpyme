<?php
include("../sjadmin/bd/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Contratos</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="../css/bootstrap_mat.css">
     <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
</head>
<body>
<div class="container">
 <img src="../img/ENCABEZADO_BCV.jpg" class="img-responsive center-block">
<br>
<br>

 <div class="row">
  <div class="col-md-12">
    <a href=""></a>
 
    <h4>Registrate Gratis para usar este Servicio.</h4>
    <hr>
    <div id="div_registro">
    <hr>
    <form id="form" action="registrar_persona.php" method="POST" >
    
      <div class="col-md-8">
        <label>Email</label>
        <input name="email" id="email"  class="form-control" type="email" tabindex="1" maxlength="100" required />
      </div>

       <div class="col-md-8">
        <label>Provincia</label>
          <select class="form-control" name="provincia" id="prov" tabindex="2" required >
            <option>Seleccionar ...</option>
            <?php
            $sql="select id, nombre from provincia";
            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
              foreach($provincias as $item)
              {
                echo "<option value='".$item[id]."'> ". utf8_encode($item[nombre])."</option>";
              }
        ?>
        </select>
      </div>
      <div class="col-md-8">
       <hr>
        <button id="registro" class="myButton" type="submit"> Registrate y accede al Formulario de Carga</button>
       </div>
    </form>    
    </div>
    <hr>
</div>
<br>
<hr>
<br>
<br>
<br>

<div class="col-md-12">
<br>
<br>
<h3>Modelo del Contrato</h3>
<hr>
<br>

<p class="text-center lead">BOLETO DE COMPRA VENTA DE VEHICULO</p>
<p class="text-justify">
En la ciudad de  <code>[CIUDAD]</code>, el <code>[FECHA]</code> , entre el/a Sr/a. <code>[NN VENDEDOR]</code>, D.N.I. N° <code>[DNI VENDEDOR]</code>, con domicilio real en <code>[DOMIC VENDEDOR]</code>, <code>[PROVINCIA V]</code>, en adelante el “VENDEDOR”, y el/a Sr/a. <code>[NN COMPRADOR]</code>, D.N.I. N° <code>[DNI COMPRADOR]</code>, con domicilio real en <code>[DOMIC COMPRADOR]</code>, <code>[PROVINCIA C]</code>, en adelante el “COMPRADOR”, convienen en celebrar el presente Contrato de Compra Venta de Vehículo, con compromiso de transferencia de dominio, el que se ajustará a las siguientes cláusulas:
</p>
<br>
<p class="text-justify">
Primera: Objeto: El VENDEDOR, vende al COMPRADOR y este adquiere un <code>[VEHICULO]</code>, usado/0 KM, Marca <code>[MARCA]</code>, Tipo <code>[TIPO]</code>, Modelo <code>[MODELO]</code>, Dominio <code>[DOMINIO]</code>, Motor N° <code>[MOTOR NRO]</code>, Chasis N° <code>[CHASIS NRO]</code>, en el estado en que se encuentra, y que el COMPRADOR declara conocer y aceptar. 
</p>
<br>
<p class="text-justify">
El VENDEDOR, exhibe en este acto al COMPRADOR, Certificado de Dominio del Automotor, expedido por el DNRPA N° <code>[REGISTRO NRO]</code>, donde se encuentra el legajo del vehículo objeto del presente contrato y del cual resulta la titularidad dominial del VENDEDOR, declarando en este acto, que el vehículo no se encuentra ni gravado ni prendado y apto para ser transferido.
</p>
<br>
<p class="text-justify">
Segunda: Precio: El precio convenido asciende a la suma de Pesos <code>[PRECIO TOTAL LETRAS]</code>, ($ <code>[PRECIO TOTAL NRO]</code>), el que se abona de la siguiente forma: de contado la suma de $ <code>[PRECIO CONTADO NRO]</code>, y el saldo de $ <code>[PRECIO SALDO NRO]</code>, en <code>[NRO DE CUOTAS]</code> cuotas mensuales y consecutivas de $ <code>[VALOR CUOTA]</code> cada una con vencimientos los días <code>[VTO CUOTA]</code> de cada mes. El dinero en efectivo, es entregado en este acto, sirviendo el presente como suficiente recibo de pago y conformidad.
</p>
<br>
<p class="text-justify">
Tercera: Transferencia: El COMPRADOR, se obliga a efectuar la transferencia del automotor a su nombre dentro del plazo de 24 horas, a contar desde la fecha de la firma del presente, caso contrario se realizará la Denuncia de Venta por ante el DNRPA y por ante la Dirección de Rentas del Automotor, una vez vencido el plazo otorgado.
</p>
<br>
<p class="text-justify">
Cuarta: Incumplimiento: En el caso de que cualquiera de las partes no concurrieran por ante el DNRPA que corresponda, a formalizar la Transferencia de Dominio objeto del presente contrato en el plazo previsto en la clausula anterior, se establece como multa por cada día de retraso el 1% del valor del contrato, el que se exigirá judicialmente, sin previa interpelación.
</p>
<br>
<p class="text-justify">
Quinta: Documentación: En este acto, el VENDEDOR, entrega el Título de Dominio, la Cédula Verde, los comprobantes de pago de las Patentes, haciéndose las siguientes observaciones: <code>[OBSERVACIONES]</code>.
</p>
<br>
<p class="text-justify">
Sexta: Responsabilidad: A partir de la firma del presente día, el COMPRADOR acepta en forma exclusiva la responsabilidad civil, penal y administrativa, por todas las consecuencias derivadas del uso o posesión del vehículo, eximiendo de todo tipo de responsabilidad al VENDEDOR por el uso indebido del mismo.
</p>
<br>
<p class="text-justify">
Séptima: Consentimiento: El/a Sr/a. <code>[CONYUGE]</code>, D.N.I. N° <code>[DNI CONYUGE]</code>, esposa/o del VENDEDOR y presente en este acto, presta su conformidad a la venta pactada y compromete su presencia ante la firma por ante el DNRPA que corresponda, a los fines de dar su asentimiento conyugal para el acto de transferencia de dominio.
</p>
<br>
<p class="text-justify">
Octava: Jurisdicción: Las partes constituyen domicilio legal para los efectos derivados del presente contrato, en los domicilios denunciados up supra, donde se tendrán por validas todas y cada una de las notificaciones extrajudiciales y/o judiciales con relación al contrato. Asimismo ambas partes, manifiestan que se someterán a la competencia ordinaria de los Tribunales de <code>[PROVINCIA]</code>, para todos los efectos derivados del presente boleto de compra venta.
</p>
<br>
<p class="text-justify">
En prueba de conformidad se firman dos ejemplares de un mismo tenor y a un solo efecto.
</p>
<br>

  <div class="row">
    <div class="col-md-6">
        <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>VENDEDOR</td>
          </tr>
        </table>
                                                                            
    </div>
    <div class="col-md-6">
   <table>
          <tr> 
            <td>…………………………………………………</td>
          </tr>
          <tr>
            <td>COMPRADOR</td>
          </tr>
        </table>
    </div>
 </div>
  <?
  //}
?>
</div>
</div>
</div>


<br>
 <br>   
<footer style="
    background-color: #124489;
    color: white;
    text-align: center;
    padding: 10px;
">       
      <p>© 2019 <a href="https://www.servijus.com.ar/">SERVIJUS - Servicios Jurídicos Online.</a> Todos los Derechos Reservados.</p>
      <div class="container">
      <div class="row">

        <div class="col-md-4" style="text-align: left;">
        
        <p class="float-left">
          <a href="https://www.servijus.com.ar/nosotros/" target="_blank">Nosotros</a>
        </p>
        <br>
       
        <p class="float-left">
          <a href="https://www.servijus.com.ar/servijus-responsable/" target="_blank">Servijus Responsable</a>
        </p> 

        <br>
       
        <p class="float-left">
          <a href="https://www.fundacion.servijus.com.ar/" target="_blank">Fundación FormaRSE</a>
        </p>  
        <br>
       

        <p class="float-left">
         <a href="terminos.php" target="_blank">Terminos y Condiciones</a>
        </p>
        <br>
       
      
        </div>


        <div class="col-md-4" style="text-align: left;">

        <p class="float-left">
          <a href="https://www.servijus.com.ar/centro-de-ayuda/" target="_blank">Ayuda - Te ayudamos!</a>
        </p>
        <br>
        
        <p class="float-left">
          <a href="https://www.servijus.com.ar/contacto/" target="_blank">Contacto - Te escuchamos!</a>
        </p>
        <br>
       
         <p class="float-left">
          <a href="https://www.servijus.com.ar/urgencias-legales/" target="_blank">Tienes una Urgencia Legal!</a>
        </p>
        <br>
        
       
        <p class="float-left">
          <a href="politicas.php" target="_blank">Politica de Privacidad y Datos Personales</a>
        </p>  
       
        </div>
        
    <div class="col-md-4" style="text-align: center;" >
       
        <p >
          
           <a href="http://qr.afip.gob.ar/?qr=FLxmFfNtNc2nSebSYiq1kA,," target="_F960AFIPInfo"><img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" height="100" width="80"></a>
        </p>
       
         
    </div>
    </div>   
    </div>     
</footer>




