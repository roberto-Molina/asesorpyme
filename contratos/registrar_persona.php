<?php
include("../sjadmin/bd/conexion.php");
if( isset($_POST['email']) && !empty($_POST['email']) )
{ 
   $email=$_POST['email'];
   // validar email duplicado
   $sql="SELECT `email` FROM `persona` WHERE `email`='$email'";
   $consulta=mysqli_query(conexion::obtenerInstancia(), $sql);
   if(mysqli_num_rows($consulta)>0) { 
      echo "<script language=Javascript> alert('El Email ya se encuentra registrado. Inicie Sesion para acceder al Formulario de Carga.'); </script>"; 
      //echo "<a href=\"javascript:history.back()\">Regresar</a>"; 
       echo "<script language=Javascript> location.href=\"nuevo.php\"; </script>";
    }
    else
     { // fin de mi consulta 
       // fin del validar email duplicado
       $provincia_id=$_POST['provincia'];
       //$plan=$_POST['plan'];
       $fecha_ingreso=date("Y-m-d");
       /*generar una clave  */
       $longitud = 8; // longitud del password  
       $password = substr(rand(),0,$longitud); 
       /*enviar email*/
        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        $from = "correo@servijus.com.ar";
        $to = $email;
        $subject = "Registración - Servijus.com.ar";
        $message ='
            <div class="container">
             <table class="table" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
             <tr>
              <td aling="center" style="color:blue;">
              <h3>SERVIJUS Servicios Jurídicos Online</h3>
              </td>
             </tr>
             <tr>
             <td bgcolor="#ffffff">
             <p style="font-size: 16px">
            Tu registro ha sido exitoso!, para ingresar a Tu Asesor Personal tu clave de acceso es:
            </p>
            <br>
            <p style="font-size: 18px; color: blue ; ">
            '.$password.'
            </p>
            <br>
            <p style="font-size: 16px">
            Usa este enlace al Formulario de Carga del Boleto de Compra Venta :
             <a href="https://asesoronline.servijus.com.ar/contratos/formulario_contrato.php">Formulario de Carga  - Contratos </a>
            </p>
            
            <p style="font-size: 16px">
            Para mayor seguridad, ingresa en tu perfil y hace el cambio de la misma. Gracias!
            <br>
            <br>
            Saludos cordiales!
            <br> 
            El equipo de Servijus.
            </p>
            <br>
             </td>
             </tr>
             <tr>
             <td >
            <p style="text-align: justify;">
            SERVIJUS, te recuerda que con el fin de resguardar tu seguridad, nunca te solicitaremos datos personales ni datos
            de tu tarjeta de crédito a través de los e-mails que te enviemos. No respondas e-mail que solicite información
            personal o incluya links que te deriven a páginas en internet que te lo soliciten. Si deseas no recibir más
            comunicación de nuestros servicios o dar de baja a tu registro, accede desde el link de desuscripción siguiendo los
            pasos que allí te indicamos. La información de este mail es de uso personal y no puede ser compartida ni transmitida
            a terceros, quedando la misma protegida por las normas legales y los Términos y Condiciones que regulan el uso de
            nuestros servicios jurídicos online.
            </p>
            </td>
            </tr>
            </table>
            </div>
            ';
    $headers = "From:" . $from;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    mail($to,$subject,$message, $headers);
       // echo "The email message was sent.";
     /* $largo=8;
      $cadena_base =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      $cadena_base .= '0123456789' ;
      $cadena_base .= '!@#%^&*()_,./<>?;:[]{}\|=+';
     
      $password = '';
      $limite = strlen($cadena_base) - 1;
     
      for ($i=0; $i < $largo; $i++)
      {
        $password .= $cadena_base[rand(0, $limite)];
      }
       */ 

      $password_md=md5($password);
      $sql="INSERT INTO `persona` (
      `email`,`password`,`provincia_id`,`fecha_ingreso`,`estado`,`alta`) VALUES ('$email','$password_md','$provincia_id','$fecha_ingreso','Activo','Contratos') ;";
      $consulta=mysqli_query(conexion::obtenerInstancia(), $sql);
      if($consulta)
      {
        echo "<script language=Javascript> alert('Se envio un Email a su cuenta de correo con el acceso al Formulario de Carga.'); </script>";
         
      } 
      else {
            echo "<script language=Javascript> alert('No se guardaron los Datos'); </script>";
            echo "<script language=Javascript> location.href=\"nuevo.php\"; </script>";
            }     
    }// fin validar

  }// fin del if principal  
  echo "<script language=Javascript> location.href=\"nuevo.php\"; </script>";
?>