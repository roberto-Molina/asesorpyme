<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>
<div class="espacio gris">
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">SUSCRIPCION DE PLAN</h3>
				<h2 class="mb-4">
					Elegí uno de nuestros planes <br>y disfrutá de todos los servicios.
				</h2>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
			    
				<div class="plan planmensual">
					<h4>
						Plan Mensual
					</h4>
					<h5>
						<!-- $250 -->
					<?  
                  $sql="SELECT valor FROM plan where nombre='Mensual Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
					</h5>
					<ul>
						<li><span>Acceso ilimitado todas<br>las Consultas Jurídicas</span></li>
						<li><span>Bonificación 0% en<br>el valor de la cuota</span></li>
						<li>Descarga ilimitadas de<br>Contratos Modelos</li>
						<li>Descarga ilimitadas de<br>Telegramas Laborales</li>
						<li>Descarga ilimitadas de<br>Cartas Documentos</li>
						<li>Descarga ilimitadas de<br>Notas y Formularios</li>
					</ul>
					<?php 
                    ?>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">

<meta charset="utf-8">
<link href="https://portal.todopago.com.ar:443/app/css/boton.css" rel="stylesheet">
<div class="boton-todopago-css">
  <a href='https://forms.todopago.com.ar:443/formulario/commands?command=formulario&fr=1&m=56613a0ab285e4af55d05c3a6e30b705#utm_source=3788957&utm_medium=boton_de_pago&utm_campaign=web'>
    <div class="col-md-4 col-sm-4 col-xs-12 tipo-boton-class boton_solo" id="htmlBoton" style="display: block;"> <input type="button" id="vistaPreviaBoton" class="btn aviso-boton-texto disabled" value="PAGAR" style="font-family: Arial, Helvetica, sans-serif; border: 1px solid; width: 80px; background-color: rgb(0, 198, 255); color: rgb(0, 0, 0);"> </div>
  </a>
</div>
<br>

                    </div>
				</div>
			</div>
			
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plansemestral">
					<h4>
						Plan Semestral
					</h4>
					<h5>
							<!-- $250 -->
					<?  
                  $sql="SELECT valor FROM plan where nombre='Semestral Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
					</h5>
					<ul>
						<li><span>Acceso ilimitado todas<br>las Consultas Jurídicas</span></li>
						<li><span>Bonificación 15% en<br>el valor de las cuotas</span></li>
						<li>Descarga ilimitadas de<br>Contratos Modelos</li>
						<li>Descarga ilimitadas de<br>Telegramas Laborales</li>
						<li>Descarga ilimitadas de<br>Cartas Documentos</li>
						<li>Descarga ilimitadas de<br>Notas y Formularios</li>
						<li><span>PLAN MÁS SOLICITADO<br>POR NUESTROS CLIENTES</span></li>
					</ul>
					<?php 
				
					?>
			
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  
<meta charset="utf-8">
<link href="https://portal.todopago.com.ar:443/app/css/boton.css" rel="stylesheet">
<div class="boton-todopago-css">
  <a href='https://forms.todopago.com.ar:443/formulario/commands?command=formulario&fr=1&m=b818482ff6ebc7d2846840970acc3afc#utm_source=3788957&utm_medium=boton_de_pago&utm_campaign=web'>
    <div class="col-md-4 col-sm-4 col-xs-12 tipo-boton-class boton_solo" id="htmlBoton" style="display: block;"> <input type="button" id="vistaPreviaBoton" class="btn aviso-boton-texto disabled" value="PAGAR" style="font-family: Arial, Helvetica, sans-serif; border: 1px solid; width: 80px; background-color: rgb(255, 144, 0); color: rgb(0, 0, 0);"> </div>
  </a>
</div>
<br>

         	        </div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plananual">
					<h4>
						Plan Anual
					</h4>
					<h5>
						<?  
                  $sql="SELECT valor FROM plan where nombre='Anual Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
					</h5>
					<ul>
						<li><span>Acceso ilimitado todas<br>las Consultas Jurídicas</span></li>
						<li><span>Bonificación 30% en<br>el valor de las cuotas</span></li>
						<li>Descarga ilimitadas de<br>Contratos Modelos</li>
						<li>Descarga ilimitadas de<br>Telegramas Laborales</li>
						<li>Descarga ilimitadas de<br>Cartas Documentos</li>
						<li>Descarga ilimitadas de<br>Notas y Formularios</li>
					</ul>
					<?php 
					?>

                   <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                          
<meta charset="utf-8">
<link href="https://portal.todopago.com.ar:443/app/css/boton.css" rel="stylesheet">
<div class="boton-todopago-css">
  <a href='https://forms.todopago.com.ar:443/formulario/commands?command=formulario&fr=1&m=50ef48b4a3b88712dbd972306cc1f1fd#utm_source=3788957&utm_medium=boton_de_pago&utm_campaign=web'>
    <div class="col-md-4 col-sm-4 col-xs-12 tipo-boton-class boton_solo" id="htmlBoton" style="display: block;"> <input type="button" id="vistaPreviaBoton" class="btn aviso-boton-texto disabled" value="PAGAR" style="font-family: Arial, Helvetica, sans-serif; border: 1px solid; width: 80px; background-color: rgb(255, 240, 0); color: rgb(0, 0, 0);"> </div>
  </a>
</div>
<br>
                    </div>
            	</div>
			</div>
		</div>
	</div>
</div>
<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function()
  {
    
    $("a[id^='mensual']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(7,5);
        vplan_id=1;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='semestral']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(9,5);
        vplan_id=3;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='anual']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(5,5);
        vplan_id=2;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

 });	
</script>
<?php
//include("contadortemas.php");
include("pie.html");
//include("pie.html");
?>