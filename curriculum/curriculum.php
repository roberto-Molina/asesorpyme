<?php
include_once("../sjadmin/bd/conexion.php");
class Curriculum
{
  
  public function lista()
  {
      $consulta="SELECT
      categoria.`titulo` AS categoria,
      subcategoria.`categoria_id` AS categorira_id,
      subcategoria.`foto` AS foto,
      subcategoria.`habilitada` AS habilitada,
      subcategoria.`id` AS id,
      subcategoria.`resumen` AS resumen,
      subcategoria.`titulo` as titulo,
	    subcategoria.`orden` as orden
      FROM
          `subcategoria`
          INNER JOIN `categoria` 
              ON (`subcategoria`.`categoria_id` = `categoria`.`id`)";
                  
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
       $data[] = $fila;
       
      }
    }
    return $data;
  }

  public function nuevo($nombre, $foto,$domicilio,$provincia, $telefono,$email, $idioma1, $idioma2, $idioma3,
			$idioma4, $habi, $perfilpersonal, $experiencia, $educacion)
   	{
  	    
		$sql="INSERT INTO `curriculum`
            (`nombre`, `foto`, `domicilio`,`provincia_id`, 
			`telefono`, `email`, `idioma1`, `idioma2`, `idioma3`, 
			`idioma4`, `habi`, `perfilpersonal`, `experiencia`, `educacion`)
            VALUES ('$nombre', '$foto', '$domicilio','$provincia', '$telefono', '$email', '$idioma1', '$idioma2', '$idioma3',
			'$idioma4', '$habi', '$perfilpersonal', '$experiencia', '$educacion')";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      
      $rs=mysqli_insert_id(conexion::obtenerInstancia());
      return $rs;
    	}


	public function editar($id,$categoria_id,$titulo,$resumen,$foto,$habilitada,$orden)
	{
      $sql="UPDATE `subcategoria`
            SET 
              `categoria_id` = '$categoria_id',
              `titulo` = '$titulo',
              `resumen` = '$resumen',
              `foto` = '$foto',
              `habilitada` = '$habilitada',
			  `orden` = '$orden'
            WHERE `id` = '$id';";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }

    public function ultimoid()
  	{
  	  $sql = "SELECT MAX(id) AS id FROM curriculum";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }	
	
	

    public function obtenerId($id)
  	{
  	 $sql="SELECT * FROM curriculum where id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }

   
  public function listarprovincia()
  {
    $consulta="SELECT * FROM provincia order by nombre";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }  

  public function agregar_persona($email,$provincia_id,$fecha_ingreso,$estado,$alta)
  {
      // enviar email

       /*generar una clave  */
       $longitud = 8; // longitud del password  
       $password = substr(rand(),0,$longitud); 
       /*enviar email*/
        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        $from = "correo@servijus.com.ar";
        $to = $email;
        $subject = "Suscripción Servijus.com.ar";
        $message ='
            <div class="container">
             <table class="table" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
             <tr>
              <td aling="center" style="color:blue;">
              <h3>SERVIJUS Servicios Jurídicos Online</h3>
              </td>
             </tr>
             <tr>
             <td bgcolor="#ffffff">
             <p style="font-size: 16px">
            Tu registro ha sido exitoso!, para ingresar a Tu Asesor Personal tu clave de acceso es:
            </p>
            <br>
            <p style="font-size: 18px; color: blue ; ">
            '.$password.'
            </p>
            <br>
            <p style="font-size: 16px">
            Para mayor seguridad, ingresa en tu perfil y hace el cambio de la misma. Gracias!
            <br>
            <br>
            Saludos cordiales!
            <br> 
            El equipo de Servijus.
            </p>
            <br>
             </td>
             </tr>
             <tr>
             <td >
            <p style="text-align: justify;">
            SERVIJUS, te recuerda que con el fin de resguardar tu seguridad, nunca te solicitaremos datos personales ni datos
            de tu tarjeta de crédito a través de los e-mails que te enviemos. No respondas e-mail que solicite información
            personal o incluya links que te deriven a páginas en internet que te lo soliciten. Si deseas no recibir más
            comunicación de nuestros servicios o dar de baja a tu registro, accede desde el link de desuscripción siguiendo los
            pasos que allí te indicamos. La información de este mail es de uso personal y no puede ser compartida ni transmitida
            a terceros, quedando la misma protegida por las normas legales y los Términos y Condiciones que regulan el uso de
            nuestros servicios jurídicos online.
            </p>
            </td>
            </tr>
            </table>
            </div>
            ';
            $headers = "From:" . $from;
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            mail($to,$subject,$message, $headers);
            
      $password_md=md5($password);
     

    $sql="INSERT INTO `persona`
            (`email`,
               `password`,
             `provincia_id`,
             `fecha_ingreso`,
             `estado`,
             `alta`)
          VALUES ('$email',
                   '$password_md',
                  '$provincia_id',
                  '$fecha_ingreso',
                  '$estado',
                  '$alta');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      
      $rs=mysqli_insert_id(conexion::obtenerInstancia());
      return $rs;
      }
  
   
}
?>