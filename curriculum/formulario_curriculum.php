<!DOCTYPE html>
<html lang="es">
<head>
    <title>DEMO SJ</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
     <script type="text/javascript" src="../js/nicEdit.js"></script>
 <script type="text/javascript">
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
   
 </script>
 <style type="text/css">
   /*boton personal*/
.myButton {
  background-color:#79bbff;
  -moz-border-radius:28px;
  -webkit-border-radius:28px;
  border-radius:28px;
  border:1px solid #337bc4;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:15px;
  padding:8px 40px;
  text-decoration:none;
  text-shadow:0px 1px 0px #528ecc;
}
.myButton:hover {
  background-color:#378de5;
  color:#ffffff;
}
.myButton:active {
  position:relative;
  top:1px;
}
 </style>
</head>
<body>
<?
include("../sjadmin/bd/conexion.php");
include("curriculum.php");
if( isset($_POST['nombre']) && !empty($_POST['nombre']) )
 {

  //subir archivo
  if ($_POST["action"] == "upload")
  {
    // obtenemos los datos del archivo
    $tamano = $_FILES["foto"]['size'];
    $tipo = $_FILES["foto"]['type'];
    $archivo = $_FILES["foto"]['name'];
    if ($archivo != "")
      {
        if (($tipo == "image/gif") or
            ($tipo == "image/jpeg")or
            ($tipo == "image/jpg") or
            ($tipo == "image/png"))
        {
         // guardamos el archivo a la carpeta imagenes
          $destino =  "../curriculum/fotos/".$archivo;
          if (copy($_FILES['foto']['tmp_name'],$destino))
              {
                // renombrar archivo subido
                $long = 6; // la longitud del rand 
                $nuevo_rand = ''; 
                for($i=0;$i<=$long;$i++){ 
                $nuevo_rand .= rand(0,9); 
                }
                //subcadena
                $subcadena = substr ($archivo,-3);

               // $info = new SplFileInfo($archivo);
               // $subcadena=($info->getExtension());  

                // nombre modificado del archivo        
                 $nuevo_name ='curri_'.$nuevo_rand.'.'.$subcadena; 
                rename ("../curriculum/fotos/".$archivo,"../curriculum/fotos/".$nuevo_name);
                //fin del renombrar
                $status = "Archivo subido como : <b>".$nuevo_name."</b>";
              }
              else { 
                   echo $status = "Error al Copiar el Archivo";
                   exit();
                   }
            }else{ echo $status = "El Archivo no es una imagen o foto";
                   exit();}
          }
          else { echo $status = "Achivo esta vacio";
                 exit();}
   }
//insertar bd
  $nombre= utf8_encode($_POST['nombre']);
  $foto =$nuevo_name;
  $domicilio= utf8_encode($_POST['domicilio']);  
  $provincia= $_POST['provincia'];  
  $telefono = $_POST['telefono'];
  $email = $_POST['email'];
  $idioma1= utf8_encode($_POST['idioma1']);
  $idioma2= utf8_encode($_POST['idioma2']);
  $idioma3= utf8_encode($_POST['idioma3']);
  $idioma4= utf8_encode($_POST['idioma4']);
  $habi= utf8_encode($_POST['habi']);
  $perfilpersonal= utf8_encode($_POST['perfilpersonal']);
  $experiencia= utf8_encode($_POST['experiencia']);
  $educacion= utf8_encode($_POST['educacion']);
  
  // para personas
  $fecha_ingreso=date("Y-m-d");
  
  //$agregar=curriculum::agregar_persona($email,$provincia,$fecha_ingreso,'Activo','Curriculum');

  // fin enviar email
  $todobien = curriculum::nuevo($nombre, $foto,$domicilio,$provincia,$telefono,$email,$idioma1,$idioma2,$idioma3,$idioma4, $habi, $perfilpersonal,$experiencia,$educacion);
 
  if($todobien > 0){
    $url="plantilla.php?id=".$todobien;
      //echo "<script language=Javascript> location.href=\"plantilla.php\"; </script>"; 
	  echo "<script language=Javascript> location.href='$url'; </script>"; 
    //echo "<script language=Javascript> imprimir(".$todobien.") </script>"; 

   // exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    exit();
    }     
}
else
{
?>

<div id="principal">
<div class="container">
 <img src="../img/ENCABEZADO_CV.jpg" class="img-fluid" >
 
 <div class="row">
   <div class="col-md-12">
    <br>
     <h4>Mi Curriculum</h4>
     <hr>

 <form id="form" enctype="multipart/form-data" method="post" action="nuevo.php">
    <input name="MAX_FILE_SIZE" value="20200000" type="hidden">
    <input name="action" value="upload" type="hidden" >
  
  <div class="col-md-12">
    <label>Nombre Y Apellido</label>
    <input name="nombre" class="form-control" type="text" tabindex="1" maxlength="60" required autofocus/>
  </div>
 
   <div class="col-md-12">
    <label>Foto</label>
    <input name="foto" class="form-control" type="file" tabindex="2" required />
  </div>
  
  <div class="col-md-12">
    <label>Domicilio</label>
    <input name="domicilio"  class="form-control" type="text" tabindex="3" maxlength="100" required />
  </div>  

  <div class="col-md-12">
      <label>Provincia</label>
      <select class="form-control" name="provincia" tabindex="4" required >
        <option>Seleccionar ...</option>
        <?
            $listado = curriculum::listarprovincia();
            foreach($listado as $item)
            {
            echo "<option value='".$item[id]."'> ". utf8_encode($item[nombre])."</option>";
            }
        ?>
      </select>
    </div>
	
  <div class="col-md-12">
    <label>Telefono</label>
    <input name="telefono"  class="form-control" type="text" tabindex="6" maxlength="20" required />
  </div>    

  <div class="col-md-12">
    <label>E-mail</label>
    <input name="email"  class="form-control" type="email" tabindex="7" maxlength="30" required />
  </div>  

  <div class="col-md-12">
    <label>Idioma</label>
    <select class="form-control" name="idioma1" tabindex="8" >
        <option value=" " >Seleccionar ...</option>
        <option value="Ingles">INGLES</option>
        <option value="Frances">FRANCES</option>
		<option value="Aleman">ALEMAN</option>
		<option value="Otro">OTRO</option>
      </select>
  </div>  
  
  <div class="col-md-12">
    <label>Idioma</label>
    <select class="form-control" name="idioma2" tabindex="9" >
         <option value=" " >Seleccionar ...</option>
        <option value="Ingles">INGLES</option>
        <option value="Frances">FRANCES</option>
		<option value="Aleman">ALEMAN</option>
		<option value="Otro">OTRO</option>
      </select>
  </div>    
  
  <div class="col-md-12">
    <label>Idioma</label>
    <select class="form-control" name="idioma3" tabindex="10" >
         <option value=" " >Seleccionar ...</option>
        <option value="Ingles">INGLES</option>
        <option value="Frances">FRANCES</option>
		<option value="Aleman">ALEMAN</option>
		<option value="Otro">OTRO</option>
      </select>
  </div>      
  
  <div class="col-md-12">
    <label>Idioma</label>
    <select class="form-control" name="idioma4" tabindex="11"  >
         <option value=" " >Seleccionar ...</option>
        <option value="Ingles">INGLES</option>
        <option value="Frances">FRANCES</option>
		<option value="Aleman">ALEMAN</option>
		<option value="Otro">OTRO</option>
      </select>
  </div>      

   <div class="col-md-12">
    <label>Habilidades</label>
    <textarea rows="12" cols="150" name="habi" id="habi" tabindex="12" style="width: 100%;" required > </textarea>
  </div>
   
  <div class="col-md-12">
    <label>Perfil Personal</label>
    <textarea rows="12" cols="150" name="perfilpersonal" id="perfilpersonal" tabindex="13" style="width: 100%;"  > </textarea>
  </div>   
  
    <div class="col-md-12">
    <label>Experiencia laboral</label>
    <textarea rows="8" cols="150" name="experiencia" id="experiencia" tabindex="14" style="width: 100%;"  > </textarea>
   </div>   
  
    <div class="col-md-12">
    <label>Estudios</label>
    <textarea rows="8" cols="150" name="educacion" id="educacion" tabindex="15" style="width: 100%;"  > </textarea>
   </div>   
 
   <div class="col-md-12">
  <hr>
      <!--button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Volver </button-->
      <button type="submit" class="myButton"><i class="fa fa-floppy-o"></i> Imprimir Mi Curriculum</button>
  </div>
</form>
</div>  
</div>
</div>  
</div>
 <?
 }
 ?>  

 <script type="text/javascript">
 $(document).ready(function()
  {
    v_boton ="CURRICULUM";
    $.post("registrar_boton.php", {boton: v_boton}, function(mensaje) {
        });//fin post
  });
</script>
 <br>
 <br>   
<footer style="
    background-color: #124489;
    color: white;
    text-align: center;
    padding: 10px;
">       
      <p>© 2019 <a href="https://www.servijus.com.ar/">SERVIJUS - Servicios Jurídicos Online.</a> Todos los Derechos Reservados.</p>
      <div class="container">
      <div class="row">

        <div class="col-md-4" style="text-align: left;">
        
        <p class="float-left">
          <a href="https://www.servijus.com.ar/nosotros/" target="_blank">Nosotros</a>
        </p>
        <br>
        <br>
        <p class="float-left">
          <a href="https://www.servijus.com.ar/servijus-responsable/" target="_blank">Servijus Responsable</a>
        </p> 

        <br>
        <br>
        <p class="float-left">
          <a href="https://www.fundacion.servijus.com.ar/" target="_blank">Fundación FormaRSE</a>
        </p>  
        <br>
        <br>

        <p class="float-left">
         <a href="terminos.php" target="_blank">Terminos y Condiciones</a>
        </p>
        <br>
        <br>
      
        </div>


        <div class="col-md-4" style="text-align: left;">

        <p class="float-left">
          <a href="https://www.servijus.com.ar/centro-de-ayuda/" target="_blank">Ayuda - Te ayudamos!</a>
        </p>
        <br>
        <br>

        <p class="float-left">
          <a href="https://www.servijus.com.ar/contacto/" target="_blank">Contacto - Te escuchamos!</a>
        </p>
        <br>
        <br>

         <p class="float-left">
          <a href="https://www.servijus.com.ar/urgencias-legales/" target="_blank">Tienes una Urgencia Legal!</a>
        </p>
        <br>
        <br>
       
        <p class="float-left">
          <a href="politicas.php" target="_blank">Politica de Privacidad y Datos Personales</a>
        </p>  
       
        </div>
        
    <div class="col-md-4" style="text-align: center;" >
       
        <p >
          
           <a href="http://qr.afip.gob.ar/?qr=FLxmFfNtNc2nSebSYiq1kA,," target="_F960AFIPInfo"><img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" height="100" width="80"></a>
        </p>
       
         
    </div>
    </div>   
    </div>     
</footer>