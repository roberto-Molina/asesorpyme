<?php
 include("curriculum.php");
 if( isset($_GET['id']) && !empty($_GET['id']) )
 {
        $id=(int)$_GET['id'];
        $listado=curriculum::obtenerId($id);
       
        foreach($listado as $veh)
        {
          $id = $veh['id'];
          $nombre= utf8_decode($veh['nombre']);
          $foto=  $veh['foto'];
          $domicilio= utf8_decode($veh['domicilio']);
          $telefono=  $veh['telefono'];
          $email=  $veh['email'];
          $idioma1=  $veh['idioma1'];
          $idioma2=  $veh['idioma2'];
          $idioma3=  $veh['idioma3'];
          $idioma4=  $veh['idioma4'];
          $habi=  utf8_decode($veh['habi']);
          $perfilpersonal=  utf8_decode($veh['perfilpersonal']);
          $experiencia=  utf8_decode($veh['experiencia']);
          $educacion=  utf8_decode($veh['educacion']);
         }
     
// Require composer autoload
require_once __DIR__ . '../../vendor/autoload.php';  
// Create an instance of the class:
$mpdf = new \Mpdf\Mpdf(); 
$html1='
<center>

<table  width="100%" collspacing="0" collpadding="0" border="0">
<tr> 
   <td>
   <img src="../curriculum/fotos/'.$foto.'" width="180px" height="180px" style="padding:20px;margin:20px" class="foto">
   </td>
   <td>
  
    </td>
</tr>
</table>

<table  width="100%" collspacing="0" collpadding="0" border="0">
<tr> 
   <td>
     <h5>CONTACTO</h5>
     <hr>
             
               <p>DOMICILIO : '.$domicilio.'</p> <br>  
               <p>TELÉFONO : '.$telefono.'</p>  <br>
               <p>CORREO ELECTRÓNICO : '.$email.'</p> 
    
   </td>
</tr>
<tr> 
   <td>
     <br>
     <h5>PERFIL PERSONAL</h5>
     <hr>
          <p>'.$perfilpersonal.'</p> <br>  
    
   </td>
</tr>
<tr> 
   <td>
    <br>
     <h5>EXPERIENCIA LABORAL</h5>
     <hr>
         <p>'.$experiencia.'</p>  <br>
   
   </td>
</tr>
<tr> 
   <td>
    <br>
     <h5>HABILIDADES</h5>
     <hr>
         <p>'.$habi.'</p>  <br>
   
   </td>
</tr>
<tr> 
   <td>
    <br>
     <h5>EDUCACIÓN</h5>
     <hr>
         <p>'.$educacion.'</p>  <br>
  
   </td>
</tr>
<tr> 
   <td>
    <br>
     <h5>IDIOMAS</h5>
     <hr>
         <p>'.$idioma1.'</p>  <br>
         <p>'.$idioma2.'</p>  <br>
         <p>'.$idioma3.'</p>  <br>
         <p>'.$idioma4.'</p>  <br>
   </td>
</tr>
</table>
</center>
';


$html='
</head>
<body>
  <center>
      <table  width="100%" collspacing="0" collpadding="0" border="0">
            <tbody>
                  <tr>

                  <td class="filauno" width="30%" align="center">

                    <img src="../curriculum/fotos/'.$foto.'" width="150px" height="150px" >

                    <br>
                    <br>
                    <div align="center">

                    <h2 style="text-align: center;">'.$nombre.'</h2>
                    
                    </div>
                    
                    <br>
                    <br>

                    <h4>CONTACTO</h4>
                    <hr>
               
                     <p> DOMICILIO  :<br> '.$domicilio.'</p> 
                     <br>  
                     <p> TELÉFONO : <br>'.$telefono.'</p>  
                     <br>
                     <p> CORREO ELECTRÓNICO :<br> '.$email.'</p>


                  </td>

                  <td width="70%" align="center">
                   
                   <h4>EDUCACIÓN</h4>
                   <hr>
                   <p>'.$educacion.'</p>
                   <br>

                   <h4>EXPERIENCIA LABORAL</h4>
                   <hr>
                   <p>'.$experiencia.'</p>
                   <br>

                    <h4>HABILIDADES</h4>
                    <hr>
                    <p>'.$habi.'</p>
                    <br>

                    <h4>IDIOMAS</h4>
                     <hr>
                         <p>'.$idioma1.'</p>  <br>
                         <p>'.$idioma2.'</p>  <br>
                         <p>'.$idioma3.'</p>  <br>
                         <p>'.$idioma4.'</p>  <br>
                  </td>
                  </tr>
                  
                </tbody>
              </table>
   </center>';

$css='<style>'.file_get_contents('estilo.css').'</style>'; 
$mpdf->writeHTML($css);  
$mpdf->writeHTML($html);  
$mpdf->Output();  
exit(); 
} 
?>