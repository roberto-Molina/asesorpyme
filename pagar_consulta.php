<?php
    if (isset($_POST["monto"]) && !empty($_POST["monto"]))
    {
        require_once ("pagos/sdkphp/lib/mercadopago.php");
        $mp = new MP("2272408881629340", "0DXXqatHI5LKLCpFsaWHuKxLtcKx9oL3");

         $id_tema=htmlspecialchars($_POST['tema'],ENT_QUOTES,'UTF-8');

         $hora_activo=htmlspecialchars($_POST['hora_activo'],ENT_QUOTES,'UTF-8');


        $preference_data = array(
            "items" => array(
                array(
                    "title" => "Pago de Consulta",
                    "description" => $_POST["motivo"],
                    "quantity" => 1,
                    "currency_id" => "ARS",
                    "unit_price" => (float)$_POST["monto"]
                )
            ),
            "back_urls" => array(
                "success" => "https://www.asesoronline.servijus.com.ar/consulta_comprada.php?tema=$id_tema&hash=$hora_activo",
                "failure" => "https://www.asesoronline.servijus.com.ar/index.php?mensaje=fallo",
                "pending" => "https://www.asesoronline.servijus.com.ar/index.php?mensaje=fallo"
            ),
            "auto_return" => "approved",
            "notification_url" => "https://www.servijus.com.ar/pagos/ipn.php",
            "expires" => false,
            "expiration_date_from" => null,
            "expiration_date_to" => null                
        );
        $preferenceResult = $mp->create_preference($preference_data);
        //echo "<pre>";
        //echo json_encode($preferenceResult, JSON_PRETTY_PRINT);
        //echo "</pre><br />";
        //echo "<pre>";
        //print_r($preferenceResult["response"]["sandbox_init_point"]);
        //echo "</pre><br />";
    }   

include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");

$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';
if( isset($_POST['tema']) && !empty($_POST['tema']) )
{   
  $id_tema=htmlspecialchars($_POST['tema'],ENT_QUOTES,'UTF-8');
  // registrar estadistica
  $fechahora = date("Y-m-d H:i:s");

  if (isset($ID)) {
    $reg="INSERT INTO `e_tema` (`usuario_id`,`fechahora`,`tema_id`,'tipo') VALUES ('$ID','$fechahora','$id_tema','PAGA')";
    $registrado=mysqli_query(conexion::obtenerInstancia(), $reg);
  }
  

  $temas="SELECT
            tema.`contenido_principal` AS contenido_principal,
            tema.`contenido_secundario` AS contenido_secundario,
            tema.`contenido_destacado` AS contenido_destacado,
            tema.`foto` AS foto,
            tema.`recomendacion` AS recomendacion,
            tema.`relacion_temas` AS relacion_temas,
            tema.`requisitos` AS requisitos,
            tema.`resumen` AS resumen,
            tema.`lectura` AS lectura,
            tema.`titulo` AS titulo,
            tema.`tipo` AS tipo,
            tema.`tramite` AS tramite,
            subcategoria.`categoria_id` AS categoria_id,
            subcategoria.`id` AS subcategoria_id,
            subcategoria.`titulo` AS subcategoria,
            categoria.`id` AS categoria_id,
            categoria.`titulo` AS categoria
            FROM
                `tema`
                  INNER JOIN `subcategoria` 
                    ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                    INNER JOIN `categoria` 
            ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
                    WHERE `tema`.`id`=$id_tema";
    $listado=mysqli_query(conexion::obtenerInstancia(), $temas);
    while( $item = mysqli_fetch_assoc($listado))
    {
      $contenido_principal=$item['contenido_principal'];
      $contenido_secundario=$item['contenido_secundario'];
      $contenido_destacado=$item['contenido_destacado'];
      $foto=$item['foto'];
      $recomendacion=$item['recomendacion'];
      $relacion_temas=$item['relacion_temas'];
      $requisitos=$item['requisitos'];
      $resumen=$item['resumen'];
      $lectura=$item['lectura'];
      $subcategoria_id=$item['subcategoria_id'];
      $subcategoria=$item['subcategoria'];
      $titulo=$item['titulo'];
      $tramite=$item['tramite'];
      $categoria_id=$item['categoria_id'];
      $categoria=$item['categoria'];
      
    }
?>

<div class="consulta-header">
    <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $foto;?>" alt="" class="d-block w-100"/>
</div>

<div class="consulta-contenido espaciomin grisoscuro">
    <div class="container">
        
        <div class="row breadcrumbs">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-8">
                <a href="subcategoria.php?cat=<? echo $categoria_id?>" class="pill pillblanco mr-1">
                    <? echo "&larr;&nbsp;&nbsp;".$categoria;?>
                </a>

                <a href="tema.php?subcat=<?php echo $item['id'];?>&cat=<?php echo $id_cat;?>" class="pill pillazul mr-1">
                    <? echo "&larr;&nbsp;&nbsp;".$subcategoria;?>
                </a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                <h1>
                    <?php echo utf8_decode($titulo);?>
                </h1>
                <h2 class="mt-3">
                    <?php echo utf8_decode($resumen);?>
                </h2>
                <br>
                <a class="pill pillblanco mr-1">Tiempo estimado de lectura: </a> <?php echo utf8_decode($lectura);?>

            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="plan plananual comprar-consulta">
                    <h4>
                        Valor de Consulta
                    </h4>

                    <h5>
                        <!-- ingresar el valor individual de la tabla TEMAS-->
                    <?php  
                  // antes 11102021$sql="SELECT valor FROM plan where nombre='Unitario'";

                            $sql="SELECT `costo` FROM `tema` WHERE id=$id_tema"; 
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['costo'];
                    $monto=$item['costo'];
                  }?>
                    </h5>
                    
                    <?php 
                    // si esta registrado 
                    if (!is_null($ID))
                     {
                    ?>
                      <a id="consulta<?php echo $id_tema;?>" mp-mode="dftl" href="https://www.mercadopago.com.ar/checkout/v1/redirect?pref_id=96772022-aae07c10-f74b-4fe7-9637-12a08b7e6919" name="MP-payButton" class='orange-ar-m-ov-aron'>Pagar Consulta</a>
<script type="text/javascript">
(function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x =
document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
</script>   

                    <?php
                    } 
                    else 
                    { // cliente no registrado
                     ?>
                    <!--a href="registrate-gratis.php" class="boton">Abonar Consulta</a-->

                     <form method="post" action="pagar_consulta.php" data-toggle="validator">
                        <input type="hidden" name="monto" value="<?php echo $monto;?>">
                        <input type="hidden" name="motivo" value="<?php echo $titulo;?>">
                        <input type="hidden" name="tema" value="<?php echo $id_tema;?>">
                      <button class="boton" type="submit">Abonar Consulta</button>
                    </form>        

                    <?
                    }
                    ?>
                    
                    <ul>
                        <br>
                        <li><span>Para obtener Acceso a todo el <br>Contenido de la Consulta Jurídica,</span>
                        <br>de la Descarga de material y de sus<br> documentos disponibles</li>
                    </ul>
                    
                    <h4>
                        Para acceder de por vida<br> al contenido de la consulta
                    </h4>
                        <a href="registrate-gratis.php" class="boton">Registra tu Mail</a>

                    <ul>
                        <br>
                        <li><span>Las consultas jurídicas adquiridas<br>estarán disponibles en Mis Compras</span></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="espacio gris">
    <div class="container">
        <div class="row">
            <div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3 class="volanta">MEMBRESIAS</h3>
                <h2 class="mb-4">
                    Suscríbete a una membresías<br>y disfrutá de todo el contenido.
                </h2>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="plan planmensual">
                    <h4>
                        Membresía Mensual
                    </h4>
                    <h5>
                        <!-- $250 -->
                    <?  
                  $sql="SELECT valor FROM plan where nombre='Mensual'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    </h5>
                    <ul>
                        <li><span>Acceso ilimitado a todas<br>las Consultas Legales</span></li>
                        <li><span>Bonificación 10% en<br>el valor de la cuota</span></li>
                        <li><span>Descarga ilimitadas de:</span><br>
                        Contratos Modelos<br>
                        Telegramas Laborales<br>
                        Cartas Documentos<br>
                        Notas y Formularios</li>
                    </ul>
                    <?php 
                    // si esta registrado 
                    if (!is_null($ID))
                     {
                    ?>
                      <a id="mensual<?echo $ID;?>" mp-mode="dftl" href="https://www.mercadopago.com.ar/checkout/v1/redirect?pref_id=96772022-
513a24af-d804-413b-982e-312569d4b466" name="MP-payButton" class='orange-ar-m-ov-aron'>Membresía Mensual</a>
<script type="text/javascript">
(function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
</script>

                    <?
                    } 
                    else 
                    {

                     ?>
                    <a href="registrate-gratis.php" class="boton">Membresía Mensual</a>
                    <?
                    }
                    ?>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="plan plansemestral">
                    <h4>
                        Membresía Semestral
                    </h4>
                    <h5>
                            <!-- $250 -->
                    <?  
                  $sql="SELECT valor FROM plan where nombre='Semestral'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    </h5>
                    <ul>
                        <li><span>Acceso ilimitado a todas<br>las Consultas Legales</span></li>
                        <li><span>Bonificación 20% en<br>el valor de la cuota</span></li>
                        <li><span>Descarga ilimitadas de:</span><br>
                        Contratos Modelos<br>
                        Telegramas Laborales<br>
                        Cartas Documentos<br>
                        Notas y Formularios</li>
                        <li><span>MEMBRESÍA MÁS SOLICITADA<br>POR NUESTROS CLIENTES</span></li>
                    </ul>
                    <?php 
                    // si esta registrado 
                    if (!is_null($ID))
                     {
                    ?>
                      <a id="semestral<?echo $ID;?>" mp-mode="dftl" href="https://www.mercadopago.com.ar/checkout/v1/redirect?pref_id=96772022-c79f0e38-8057-4d18-b896-05d5b71f3bc9" name="MP-payButton" class='orange-ar-m-ov-aron'>Membresía Semestral</a>
<script type="text/javascript">
(function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
</script>

                    <?
                    } 
                    else 
                    {

                     ?>
                    <a href="registrate-gratis.php" class="boton">Membresía Semestral</a>
                    <?
                    }
                    ?>
                </div>
            </div>
            
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="plan plananual">
                    <h4>
                        Membresía Anual
                    </h4>
                    <h5>
                        <?php  
                  $sql="SELECT valor FROM plan where nombre='Anual'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    </h5>
                    <ul>
                        <li><span>Acceso ilimitado a todas<br>las Consultas Legales</span></li>
                        <li><span>Bonificación 30% en<br>el valor de la cuota</span></li>
                        <li><span>Descarga ilimitadas de:</span><br>
                        Contratos Modelos<br>
                        Telegramas Laborales<br>
                        Cartas Documentos<br>
                        Notas y Formularios</li>
                    </ul>
                    <?php 
                    // si esta registrado 
                    if (!is_null($ID))
                     {
                    ?>
                      <a id="anual<?echo $ID;?>" mp-mode="dftl" href="https://www.mercadopago.com.ar/checkout/v1/redirect?pref_id=96772022-dc187814-a819-4e64-8b29-9420578dd114" name="MP-payButton" class='orange-ar-m-ov-aron'>Membresía Anual</a>
<script type="text/javascript">
(function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
</script>

                    <?
                    } 
                    else 
                    {
                     ?>
                    <a href="registrate-gratis.php" class="boton">Membresía Anual</a>
                    <?
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="relativo espacio blanco border-top">
    <img class="redondel" src="_img/icono-relacionadas.png" alt="" width="60"/>
    <div class="container">
        <div class="row">
            <div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3 class="volanta">Consultas Jurídicas On Demand</h3>
                <h2 class="mb-4">Otros temas consultados</h2>
            </div>
        </div>
        
        <div class="row mt-5">
        <?
         $relacion_tema="SELECT `id`,`tema_id`,`tema_rela_id` FROM `tema_relacionado` WHERE tema_id=$id_tema";
         $listado=mysqli_query(conexion::obtenerInstancia(), $relacion_tema);
         while( $item = mysqli_fetch_assoc($listado))
          {
            $rela_tema=$item['tema_rela_id']; 
            $temas="SELECT                        
                        subcategoria.`titulo` AS sc_titulo,
                        subcategoria.`id` AS sc_id,
                        tema.`foto` AS foto,
                        tema.`id` AS tema_id,
                        tema.`titulo` AS titulo,
                         tema.`resumen` AS resumen, 
                         tema.`lectura` AS lectura, 
                         tema.`tipo` AS tipo, 
                        tema.`habilitada` AS habilitada 
                        FROM
                            `subcategoria`
                            INNER JOIN `tema` 
                                ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                                    where  tema.`id`='$rela_tema' and tema.`habilitada`='SI' 
                                      order by tema.`orden` asc";

            $listado1=mysqli_query(conexion::obtenerInstancia(), $temas);
            while( $item1 = mysqli_fetch_assoc($listado1))
            {
           
            if ($suscripcion =='SI')
            {
            ?>
              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                  <div class="caja subcategoria">
                      <figure class="limit">
                        <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                        <img src="sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
                        </a>
                      </figure>
                    
                      <div class="subcategoria-tipo">
                      </div>
                  
                      <div class="subcategoria-texto">
                        <h4><a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                          <?php echo utf8_decode($item1['titulo']);?></a>
                        </h4>
                        <p>
                          <!--?php echo utf8_decode($item1['resumen']);?-->
                        </p>
                      </div>
                </div>
          </div>
           <?
            }

        if ($suscripcion == 'NO' )
        {
          if ($item1['tipo']=='PAGO')
           {
           ?>   
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
            <div class="caja subcategoria">
                <figure class="limit">
                    <a href="consulta-paga.php?tema=<?php echo $item1['tema_id'];?>">
                    <img src="sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
                </a>
                </figure>

                <div class="subcategoria-tipo">
                    <!--div class="pill amarillo">
                      Pago
                   </div-->
                </div>

                <div class="subcategoria-texto">  
                <h4>
                    <a href="consulta-paga.php?tema=<?php echo $item1['tema_id'];?>">
                        <?php echo utf8_decode($item1['titulo']);?></a>
                </h4>
                <p>
                    <!--?php echo utf8_decode($item1['resumen']);?-->
                </p>
                </div>
            </div>
            </div>   
               <?
             }
              else
                {
                ?>  
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                <div class="caja subcategoria">
                    <figure class="limit">
                        <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                        <img src="sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
                    </a>
                    </figure>

                    <div class="subcategoria-tipo">
                        <!--div class="pill verde">
                          Gratis
                       </div-->
                    </div>

                    <div class="subcategoria-texto">  
                    <h4>
                        <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>"><?php echo utf8_decode($item1['titulo']);?></a>
                    </h4>
                    <p>
                        <!--?php echo utf8_decode($item1['resumen']);?-->
                    </p>
                    </div>
                </div>
            </div>   
            <? 
            }// fin else
          }// fin if suscripcion
      }//fin del while
      }//fin del while

         ?>
            
            
        </div>
    </div>
</div>


<?php
}
include("estudios.php");
include("pie.html");
?>


<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function()
  {
    
    $("a[id^='consulta']").click(function(evento)
    {
        evento.preventDefault();
        vid = this.id.substr(8,5);
        

        console.log(vid);
       $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_consulta.php',
                data: { tema_id: vid},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='mensual']").click(function(evento)
    {
        evento.preventDefault();
        vid = this.id.substr(7,5);
        vplan_id=1;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='semestral']").click(function(evento)
    {
        evento.preventDefault();
        vid = this.id.substr(9,5);
        vplan_id=3;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='anual']").click(function(evento)
    {
        evento.preventDefault();
        vid = this.id.substr(5,5);
        vplan_id=2;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

 });    
</script>

    <script src="//secure.mlstatic.com/mptools/render.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        $MPC.openCheckout ({
            url: "<?php echo $preferenceResult["response"]["init_point"]; ?>",
            mode: "modal"
        });        
    </script>

    <script type="text/javascript"> 
        // jQuery    
        $(document).ready(function() {
        });        
    </script>

</body>
<!-- End Body -->
</html>