<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");

$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';

?>
<div class="destacado">
  <div class="row">
    <div class="col-md-12">

	        	
      <img src="img/FOTO-PYME.jpg" alt="" class="d-block w-100"/> 
      </br>
      </br>
      
      		<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">SERVICIOS Y CONSULTORIA ON DEMAND</h3>
				<h1 class="mb-4">Servijus agrega valor a Tu Negocio</h1>
			</div>
    </div>
  </div>  
</div><!-- fin container-->

<br>
<br>

			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			    <h3 class="volanta"><font color="DODGERBLUE">SERVIJUS Potencia los resultados de tu negocio, conoce tus derechos.</font></h3>
			    <h2 class="mb-4"> <font color="DARKBLUE">Servicios y Consultoría para Pymes <font color="DARKBLUE"></h2>
			</div>
       
<div class="espacio gris">
<div class="container">
    <div class="row">
     <?php
          $categorias="SELECT * FROM categoria where habilitada='SI' and pyme='si' order by orden asc";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
    ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				    <div class="caja categoria">
                        <a href="subcategoria.php?cat=<?php echo $item['id'];?>" class="categoria-imagen"> 
                        <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top"  alt="nombre" >
                        </a>
						    <div class="categoria-texto">
						    <h4><a href="subcategoria.php?cat=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a></h4>
                            <p class="text-justify"><?php echo utf8_decode($item['resumen']);?></p>
                             <div class="d-flex justify-content-between align-items-center">
                           <a href="subcategoria.php?cat=<?php echo $item['id'];?>" >Conocer más ....</a>
                       </div>
                    </div>
                   </div>
                </div>

          <?
             }//fin del while
          ?>
  
</div><!--row-->

        <br>
        <br>
        <br>
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta"> <font color="DODGERBLUE"> Que tu vida privada no afecte tu negocio, conoce tus derechos. </font></h3>
				<h2 class="mb-4"> <font color="DARKBLUE">Contenido Jurídico de Derechos Privados</font></h2>
			</div>
        <br>
<hr>
  <div class="row">
  <?php
         /* verificar persona habilitada o activa*/
          $categorias="SELECT * FROM categoria where habilitada='SI' and personal='si' order by orden asc";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
           ?>
          <div class="text-center col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
          <div class="caja subcategoria">
          <figure class="limit">
                    
                    <a href="subcategoria.php?cat=<?php echo $item['id'];?>" >
                      <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top"  alt="nombre" >
                    </a>
                    </figure>
          <div class="subcategoria-tipo">
          </div>
 
          <div class="subcategoria-texto">
                   <h4><a href="subcategoria.php?cat=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a></h4>
                      <p class="text-justify"><?php echo utf8_decode($item['resumen']);?></p>
                   
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                           <a href="subcategoria.php?cat=<?php echo $item['id'];?>" >Conocer más ....</a>
                        </div>
                       </div>
                    </div>
                   </div>
                </div>
          <?
             }//fin del while
          ?>
  </div>
</div><!--row-->

<br>
<br>
        <br>
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h1 class="mb-4"> <font color="DARKBLUE">Servicios Empresarios</font></h2>
			</div>
        <br>
<hr>
<div class="container">
  <div class="row">
 
    <div class="col-md-6">
      <div class="row">

      <div class="col-md-6">
        <div class="card mb-4 box-shadow">
              <a href="agenda.php" target="_blank"> 
                  <img class="card-img-top" src="img/agenda_empresaria.jpg" >
              </a>

              <div class="card-body">
                <p class="card-text"></p>
                  <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <h4><a href="agenda.php" target="_blank">Agenda Empresaria</a></h4>
                      </div>
                  </div>
              </div>
        </div>
      </div>
 
      <div class="col-md-6">
        <div class="card mb-4 box-shadow">
              <a href="https://capacitacion.servijus.com.ar/index.php" target="_blank">
                  <img class="card-img-top" src="img/capacitacion_a_medida.jpg" >
              </a>  
              <div class="card-body">
                    <p class="card-text"></p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <h4><a href="https://capacitacion.servijus.com.ar/index.php" target="_blank">Capacitación a Medida</a></h4>

                      </div>
                    </div>
              </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="card mb-4 box-shadow">
              <a href="ofrece_empleo.php" target="_blank"> 
                  <img class="card-img-top" src="img/ofrezco_trabajo.jpg" >
              </a>

              <div class="card-body">
                <p class="card-text"></p>
                  <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <h4><a href="ofrece_empleo.php" target="_blank">Ofrezco Empleo</a></h4>
                      </div>
                  </div>
              </div>
        </div>
      </div>
 
      <div class="col-md-6">
        <div class="card mb-4 box-shadow">
              <a href="busco_empleado.php" target="_blank">
                  <img class="card-img-top" src="img/busco_empleado.jpg" >
              </a>  
              <div class="card-body">
                    <p class="card-text"></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <h4><a href="busco_empleado.php" target="_blank">Busco Empleado</a></h4>
                      </div>
                    </div>
              </div>
        </div>
      </div>
     </div>
    </div>

   


    <div class="col-md-6">
      <form method="post" action="enviarconsulta.php" >
        <div class="col-md-12">
            
            <br>


            <h4><p>Consultas en Línea</p></h4>
            <br>
          <label>Selecciona el Tema de Consulta</label>

           <select class="form-control" name="categoria_id" id="categoria_id" tabindex="1" required >
             <option>Seleccionar ...</option>
                <?
                $categorias="SELECT * FROM categoria where habilitada='SI'";
                $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
                while( $item = mysqli_fetch_assoc($listado))
                //foreach($categorias as $item)
                {
                echo "<option value='".$item['id']."'> ". utf8_decode($item['titulo'])."</option>";
                }
                ?>
             </select>
           </div>
              
            <div class="col-md-12">
            <br>
                <label>Selecciona el Subtema de Consulta</label>
                <select class="form-control" name="subcategoria_id" id="subcategoria_id" tabindex="2" required >
                <div id="div_dinamico"></div>
                </select>
            </div>

            <div class="col-md-12">
            <br>
              <label>¿Cual es tu pregunta?</label>
              <textarea name="mensaje" rows="3" cols="25"class="form-control" tabindex="3" required>  </textarea>
        </div>
        <br>
        <br>
        <div class="col-md-12 align-items-center">
                <button class="boton" type="submit">Enviar Consulta</button>
                				<a href="enviarconsulta.php" class="boton">Registrate Gratis</a>

        </div>
       </form>

   
    </div>

  </div>  

</div><!-- fin container-->

<br>
<br>

<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function()
  
    {
   
  // 02/05/2019 llenar combo temas
  //buscar por tema
  $('#categoria_id').on('change',function()
  {
        vid = $(this).val()
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'lista_subcategoria.php',
          data: { cat: vid},
          success: function(data){
            if (data)
            {
                $('#subcategoria_id').html(data);
               //$('#div_dinamico').html(data);
            }
           }
       });//fin ajax
     });//fin    



 });
</script>
</div><!--content-->
<?php
include ("pie.html");
?>

    