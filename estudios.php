<?
//include("sjadmin/bd/conexion.php");
//include ("cabecera.html");
//include ("menu.php");
$PROVINCIA=20;
$estudios_profecionales= array();
?>
<div id="estudios" class="azul espacio">
	<img class="redondel" src="_img/ayuda-utiliza.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">
					Estudios profesionales
				</h3>
				<h2 class="text-white">
					¿Quién puede ayudarme?
				</h2>
			</div>
		</div>
		<div class="row mt-5">


<?

$sql="SELECT
  `id`,
  `nombre`,
  `domicilio`,
  `email`,
  `telefono`,
  `abogados`,
  `titular`,
  `telefonotitular`,
  `responsable`,
  `web`,
  `provincia_id`
FROM `estudiojuridico`
where `provincia_id`=$PROVINCIA";
$listado=mysqli_query(conexion::obtenerInstancia(), $sql);
while( $item = mysqli_fetch_assoc($listado))
{
	$tipo='Estudio Juridico';
	$nombre=$item['nombre'];
	$domicilio=$item['domicilio'];
	$telefono=$item['telefono'];
    $email=$item['email'];
    $escribania= array('tipo'=>$tipo,'nombre'=>$nombre,'domicilio'=>$domicilio,'telefono'=>$telefono,'email'=>$email);
    array_push ( $estudios_profecionales , $escribania );
}


$sql="SELECT
  `id`,
  `nombre`,
  `domicilio`,
  `email`,
  `telefono`,
  `abogados`,
  `titular`,
  `telefonotitular`,
  `responsable`,
  `web`,
  `provincia_id`
FROM `escribania`
where `provincia_id`=$PROVINCIA";
$listado=mysqli_query(conexion::obtenerInstancia(), $sql);
while( $item = mysqli_fetch_assoc($listado))
{
	$tipo='Escribania';
	$nombre=$item['nombre'];
	$domicilio=$item['domicilio'];
	$telefono=$item['telefono'];
    $email=$item['email'];
    $escribania= array('tipo'=>$tipo,'nombre'=>$nombre,'domicilio'=>$domicilio,'telefono'=>$telefono,'email'=>$email);
    array_push ( $estudios_profecionales , $escribania );
}


$sql="SELECT
  `id`,
  `nombre`,
  `domicilio`,
  `email`,
  `telefono`,
  `abogados`,
  `titular`,
  `telefonotitular`,
  `responsable`,
  `web`,
  `provincia_id`
FROM `est_contable`
where `provincia_id`=$PROVINCIA";
$listado=mysqli_query(conexion::obtenerInstancia(), $sql);
while( $item = mysqli_fetch_assoc($listado))
{
	$tipo='Estudio Contable';
	$nombre=$item['nombre'];
	$domicilio=$item['domicilio'];
	$telefono=$item['telefono'];
    $email=$item['email'];
    $escribania= array('tipo'=>$tipo,'nombre'=>$nombre,'domicilio'=>$domicilio,'telefono'=>$telefono,'email'=>$email);
    array_push ( $estudios_profecionales , $escribania );
}


$sql="SELECT
  `id`,
  `nombre`,
  `domicilio`,
  `email`,
  `telefono`,
  `abogados`,
  `titular`,
  `telefonotitular`,
  `responsable`,
  `web`,
  `provincia_id`
FROM `inmobiliaria`
where `provincia_id`=$PROVINCIA";
$listado=mysqli_query(conexion::obtenerInstancia(), $sql);
while( $item = mysqli_fetch_assoc($listado))
{
	$tipo='Trámites y Gestoría';
	$nombre=$item['nombre'];
	$domicilio=$item['domicilio'];
	$telefono=$item['telefono'];
    $email=$item['email'];
    $escribania= array('tipo'=>$tipo,'nombre'=>$nombre,'domicilio'=>$domicilio,'telefono'=>$telefono,'email'=>$email);
    array_push ( $estudios_profecionales , $escribania );
}

//saco el numero de elementos
$longitud = count($estudios_profecionales);
 
//Recorro todos los elementos
for($i=0; $i<$longitud; $i++)
{
      //saco el valor de cada elemento
	  // echo $estudios_profecionales[$i]['nombre'];
	  //echo "<br>";
	?>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
				<a class="mt-4 mb-4 pill pillblanco" href="#">
					<? echo $estudios_profecionales[$i]['tipo'];?>
				</a>
				<h6>
					<? echo $estudios_profecionales[$i]['nombre'];?>
				</h6>
				<p>
					<span>Direcci&oacute;n:</span>
					<? echo $estudios_profecionales[$i]['domicilio'];?>
				</p>
				<p>
					<span>Tel&eacute;fono:</span>
					<? echo $estudios_profecionales[$i]['telefono'];?>
				</p>
				<p>
					<span>Correo electr&oacute;nico:</span>
					<a href="mailto:jl_sanchez@servijus.com.ar">

					<? echo $estudios_profecionales[$i]['email'];?></a>
				</p>
	</div>
	<?
}
?>
		</div>
	</div>
</div>
