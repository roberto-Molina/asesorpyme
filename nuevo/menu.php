<?php
 //include("sesion.php");
?>
<div id="nav">
	<nav class="container navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<img src="_img/servijus.png" alt="Servijus" width="240"/>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Acerca</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Categorías
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Mis Derechos</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Mi Familia</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Mi Casa</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Mi Trabajo</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Mi Vehículo</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Mi Negocio</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Precios</a>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<input class="input-busqueda mr-sm-2" type="search" placeholder="Buscar consultas" aria-label="Search">
			</form>
			<a class="boton" href="#">Registrate gratis</a>
		</div>
	</nav>
</div>