<?php
include("../sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>

<div class="destacado">
	<span>
		Tema destacado:
		<a href="#">
			Coronavirus
		</a>
	</span>
	<img src="_img/contenido/destacado-coronavirus.jpg" alt="" class="d-block w-100"/>
</div>

<div class="espacio gris">
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Conoce tus derechos, toma decisiones informadas</h3>
				<h2 class="mb-4">Contenido jurídico on demand</h2>
			</div>
		</div>
		<div class="row mt-5">
		<?php
          $categorias="SELECT * FROM categoria where habilitada='SI' and producto='personal' order by orden asc";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
         ?>


			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
				<div class="caja categoria">
					<a class="categoria-imagen">
						<span>
							<img src="_img/icono-mas.png" alt="" width="75"/>
						</span>
						<img src="sjadmin/imagenes/<?php echo $item['foto'];?>" alt="" class="d-block w-100"/>
					</a>
					<div class="categoria-texto">
						<h4><a href="subcategoria.php?cat=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a></h4>
						<p>
							<?php echo utf8_decode($item['resumen']);?>
						</p>
					</div>
				</div>
			</div>
         <?
             }//fin del while
          ?>
			
		</div>
	</div>
</div>

<div class="relativo espacio blanco border-top">
	<img class="redondel" src="_img/icono-recientes.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Servicios Jurídicos Online</h3>
				<h2 class="mb-4">Tu consultas recientes</h2>
			</div>
		</div>
		<div class="row mt-5">
			
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<img src="_img/contenido/derecho-a-mi-vida.jpg" alt="" class="d-block w-100 grow"/>
					</figure>
					<div class="subcategoria-tipo">
						<div class="pill amarillo">Pago</div>
					</div>
					<div class="subcategoria-texto">
						<h4><a href="#">Derecho a Mi Vida</a></h4>
						<p>
							Conoce todos los derechos que protegen la vida desde la concepción y en la adopción.
						</p>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<img src="_img/contenido/derecho-a-mi-vida.jpg" alt="" class="d-block w-100 grow"/>
					</figure>
					<div class="subcategoria-tipo">
						<div class="pill amarillo">Pago</div>
					</div>
					<div class="subcategoria-texto">
						<h4><a href="#">Derecho a Mi Vida</a></h4>
						<p>
							Conoce todos los derechos que protegen la vida desde la concepción y en la adopción.
						</p>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<img src="_img/contenido/derecho-a-mi-vida.jpg" alt="" class="d-block w-100 grow"/>
					</figure>
					<div class="subcategoria-tipo">
						<div class="pill verde">Gratis</div>
					</div>
					<div class="subcategoria-texto">
						<h4><a href="#">Derecho a Mi Vida</a></h4>
						<p>
							Conoce todos los derechos que protegen la vida desde la concepción y en la adopción.
						</p>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<img src="_img/contenido/derecho-a-mi-vida.jpg" alt="" class="d-block w-100 grow"/>
					</figure>
					<div class="subcategoria-tipo">
						<div class="pill amarillo">Pago</div>
					</div>
					<div class="subcategoria-texto">
						<h4><a href="#">Derecho a Mi Vida</a></h4>
						<p>
							Conoce todos los derechos que protegen la vida desde la concepción y en la adopción.
						</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div id="cifras" class="azul espaciomin">
	<img class="redondel" src="_img/icono-cifras.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
				<h6>5</h6>
				<p>
					TEMAS DE<br>
					DERECHOS<br>
					PERSONALES
				</p>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
				<h6>26</h6>
				<p>
					TEMAS DE<br>
					DERECHOS<br>
					DE FAMILIA
				</p>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
				<h6>40</h6>
				<p>
					TEMAS DE<br>
					DERECHOS<br>
					DE TRABAJO
				</p>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
				<h6>5</h6>
				<p>
					TEMAS DE<br>
					DERECHOS<br>
					DE TU CASA
				</p>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
				<h6>13</h6>
				<p>
					TEMAS DE<br>
					DERECHOS<br>
					DE TU VEHÍCULO
				</p>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
				<h6>0</h6>
				<p>
					TEMAS DE<br>
					DERECHOS<br>
					DE TU NEGOCIO
				</p>
			</div>
		</div>
	</div>
</div>
<footer id="footer" class="azuloscuro espaciomin">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<nav>
					<a href="#">Nosotros</a>
					<a href="#">Servijus Responsable</a>
					<a href="#">Fundación FormaRSE</a>
					<a href="#">Terminos y Condiciones</a>
					<a href="#">Data Fiscal</a>
				</nav>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
				<nav>
					<a href="#">Centro de Ayuda</a>
					<a href="#">Consultas y Sugerencias</a>
					<a href="#">¡Tienes una Urgencia Legal!</a>
					<a href="#">Politica de Privacidad y Datos Personales</a>
				</nav>
			</div>
			<div class="col-xl-3 col-lg-2 col-md-12 col-sm-12 col-12">
				<nav class="rrss">
					<a href="#"><img src="_img/icono-facebook.png" alt="" width="18"/><span>Facebook</span></a>
					<a href="#"><img src="_img/icono-twitter.png" alt="" width="18"/><span>Twitter</span></a>
					<a href="#"><img src="_img/icono-linkedin.png" alt="" width="18"/><span>LinkedIn</span></a>
					<a href="#"><img src="_img/icono-instagram.png" alt="" width="18"/><span>Instagram</span></a>
					<a href="#"><img src="_img/icono-youtube.png" alt="" width="18"/><span>YouTube</span></a>
				</nav>
			</div>
			<div class="text-right col-xl-2 col-lg-3 col-md-12 col-sm-12 col-12">
				<a href="#" class="boton">Registrate Gratis</a>
			</div>
		</div>
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<a href="#" class="d-block mt-5">
					<img src="_img/servijus-blanco.png" alt="Servijus" width="120"/>
				</a>
			</div>
		</div>
	</div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="_js/jquery-3.4.1.slim.min.js"></script>
<script src="_js/popper.min.js"></script>
<script src="_bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>

</body>
</html>