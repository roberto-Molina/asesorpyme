<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.php");
include ("menu.php");

if( isset($_GET['tema']) && !empty($_GET['tema']) )
{   
  $id_tema=htmlspecialchars($_GET['tema'],ENT_QUOTES,'UTF-8');

  ///////////////// registrar estadistica////////////////////////////
  $fechahora = date("Y-m-d H:i:s");
  $reg="INSERT INTO `e_tema_pyme`
            (`usuario_id`,`fechahora`,`tema_id`)
            VALUES ('$ID','$fechahora','$id_tema')";
  $registrado=mysqli_query(conexion::obtenerInstancia(), $reg);

  /////////////////traer datos tema///////////////////////////////
   $navegacion="SELECT
        categoria.`id` AS cat_id,
        categoria.`titulo` AS cat_titulo,
        subcategoria.`id` AS subcat_id,
        subcategoria.`titulo` AS subcat_titulo
        FROM
            `tema`
            INNER JOIN `subcategoria` 
                ON (`tema`.`subcategoria_id` = `subcategoria`.`id`)
            INNER JOIN `categoria` 
                ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
                where `tema`.`id`=$id_tema";
    $listado=mysqli_query(conexion::obtenerInstancia(), $navegacion);
    while( $item = mysqli_fetch_assoc($listado))
     {
      $cat_id=utf8_decode($item['cat_id']);
      $cat_titulo=utf8_decode($item['cat_titulo']);
      $subcat_id=utf8_decode($item['subcat_id']);
      $subcat_titulo=utf8_decode($item['subcat_titulo']);
      }  
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="categorias.php" > Categorias de Consultas : <? echo $cat_titulo; ?></a>
    </li>
    <li class="breadcrumb-item"><a href="subcategoria.php?cat=<? echo $cat_id;?>" >Subcategorias de Consultas <? echo $subcat_titulo; ?></a>
    </li>
    <li class="breadcrumb-item active" aria-current="page"><? echo $subcat_titulo; ?></li>
  </ol>
</nav>
</div>

<?php
  $sql="SELECT estado FROM persona_pyme where id=".$ID;
  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
  while( $item = mysqli_fetch_assoc($listado))
  {
    $estado=$item['estado'];
  }




// validar el estado del suscriptor pyme
if ($estado=='Activo')



{ // boton mercado pago
?>
 <br>
 <h3 align="center"> Para habilitar el contenido Juridico Online. Elija un plan de suscripción. </h3>
 <br>

  <!-- TABLA PRECIOS-->
  <div class="container mb-5 mt-5">
    <div class="pricing card-deck flex-column flex-md-row mb-3">
        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Plan Mensual</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span class="price"> <?  
                  $sql="SELECT valor FROM plan where nombre='Mensual' and producto='Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }

                ?></span><span class="h6 text-muted ml-2">/ por mes.</span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>Suscripcion Mensual</li>
                    
                </ul>
              <a mp-mode="dftl" href="https://mpago.la/2XBpMe" name="MP-payButton" class='green-ar-m-rn-undefined'>Suscribirme Mensualmente</a>

            <script type="text/javascript">
                (function() {
                    function $MPC_load() {
                        window.$MPC_loaded !== true && (function() {
                            var s = document.createElement("script");
                            s.type = "text/javascript";
                            s.async = true;
                            s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                            var x = document.getElementsByTagName('script')[0];
                            x.parentNode.insertBefore(s, x);
                            window.$MPC_loaded = true;
                        })();
                    }
                    window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
                })();
            </script>
            </div>
        </div>

        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Plan Anual</span>
            <div class="bg-transparent card-header pt-4 border-0">
            <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span class="price"> <?  
                  $sql="SELECT valor FROM plan where nombre='Anual' and producto='Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }

                ?></span><span class="h6 text-muted ml-2">/ por el año.</span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>Suscripcion Anual</li>
                    
                </ul>
                <a mp-mode="dftl" href="https://mpago.la/1A8qzt" name="MP-payButton" class='green-ar-m-rn-undefined'>Suscribirme Anualmente</a>
                <script type="text/javascript">
                    (function() {
                        function $MPC_load() {
                            window.$MPC_loaded !== true && (function() {
                                var s = document.createElement("script");
                                s.type = "text/javascript";
                                s.async = true;
                                s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                                var x = document.getElementsByTagName('script')[0];
                                x.parentNode.insertBefore(s, x);
                                window.$MPC_loaded = true;
                            })();
                        }
                        window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
                    })();
                    </script>
            </div>
        </div>
    </div>
</div>

<!-- FIN-->

<div class="container" align="center"> <h3 > Abonar con otros medios de Pago. </h3>
 <a href="https://servijus.com.ar/pagos/" class="myButton" align="center">Otros medios de Pago.</a>
</div>
<br>
<br>
<div class="container">
  <table class="table">
   <thead>
    <tr>
             <th>Prestaciones</th>
             <th>Plan Mensual</th>
             <th>Plan Anual</th>
             </tr>
           </thead>
           <tbody>
             <tr>
              <td>Bonificación dos cuotas</td>
              <td><i class="fa fa-times" style="font-size: 30px; color: red;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
           <tr>
              <td>Acceso ilimitado a Consultas Juridicas</td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitadas de Contratos</td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitada de Telegramas Laborales</td>
               <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitada de Cartas Documentos</td>
               <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitadas de Notas y Formularios</td>
               <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
</tbody>
</table>
</div>
<br>
<br>
<br>
<br>
<?
}
else {

      ////////// si esta habilitado buscar tema ///////////////////////////////////////
      if( isset($_GET['tema']) && !empty($_GET['tema']) )
       {  
         $id_tema=$_GET['tema'];
         
//////////////////////////////// datos del tema //////////////////////////////////////////
         $temas="SELECT
                tema.`contenido_principal` AS contenido_principal,
                tema.`contenido_secundario` AS contenido_secundario,
                tema.`contenido_destacado` AS contenido_destacado,
                tema.`foto` AS foto,
                tema.`recomendacion` AS recomendacion,
                tema.`relacion_temas` AS relacion_temas,
                tema.`requisitos` AS requisitos,
                tema.`resumen` AS resumen,
                tema.`subcategoria_id` AS subcategoria,
                tema.`titulo` AS titulo,
                tema.`tramite` AS tramite
                  FROM
                      `tema`
                        INNER JOIN `subcategoria` 
                          ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                          WHERE `tema`.`id`=$id_tema";
                       
          $listado=mysqli_query(conexion::obtenerInstancia(), $temas);
          while( $item = mysqli_fetch_assoc($listado))
          {


?>
  
          <div class="container">
            <div class="row">
               
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" class="img-responsive center-block" alt="nombre">

             <br>    
             <h2 class="text-primary">
              <?php echo utf8_decode($item['titulo']);?>
             </h2>

              <p class="text-justify">
                  <?php echo utf8_decode($item['contenido_principal']);?>
              </p>
               <br>
              <p class="text-justify">
                  <?php echo utf8_decode($item['contenido_secundario']);?>
              </p>
               <br>

            <?
             $sql="SELECT
                      tema.`titulo` AS tema_titulo,
                      tema.`id` AS tema_id,
                      video.`archivo` AS archivo,
                      video.`id` AS id,
                      video.`nombre` AS nombre
                      FROM
                          `video`
                          INNER JOIN `tema` 
                              ON (`video`.`tema_id` = `tema`.`id`)
                                          where `tema`.`id`=$id_tema";
                                         
                $videos=mysqli_query(conexion::obtenerInstancia(), $sql);
                while( $itemvideo = mysqli_fetch_assoc($videos))
                  {
                  ?>
                      <img src="https://www.asesoronline.servijus.com.ar/sjadmin/video_tema/<?php echo $itemvideo['archivo'];?>" class="img-responsive center-block" alt="infografia" >
                      <br>
                      <br>
                   <!--video src="img/<?php echo $itemvideo['video'];?>" controls> </video-->
                  <?
                  }
              ?>
           </div>
          </div> <!-- 1recuadro-->

            <div class="container-fluid" style="background: #ededed;">

            <div class="container">
                <div class="row">
                 <div class="col-md-12">
                  <br>
                   <h3 class="text-primary">QUE HAGO?</h3>
                    <p class="text-justify">
                        <?php echo utf8_decode($item['tramite']);?>
                    </p>
                    <br>
                </div>
             </div>   
            </div>
            
            </div> <!-- 2recuadro-->
            
            <div class="container">
               <div class="row">
               <div class="col-md-12"  >
               <br>
                 <h3 class="text-primary">QUE DEBO TENER EN CUENTA </h3>
                 <p class="text-justify">
                    <?php echo utf8_decode($item['requisitos']);?>
                </p>
            </div>

            
            <div class="col-md-12" >
              <!--h3>Documentos para descargar :</h3-->
              <ul>
              <?
                $sql="SELECT
                        tema.`titulo` AS tema_titulo,
                        tema.`id` AS tema_id,
                        documento.`archivo` AS archivo,
                        documento.`id` AS id,
                        documento.`nombre` AS nombre
                        FROM
                            `documento`
                            INNER JOIN `tema` 
                                ON (`documento`.`tema_id` = `tema`.`id`)
                                            where `tema`.`id`=$id_tema";
                $documentos=mysqli_query(conexion::obtenerInstancia(), $sql);
                while( $itemdoc = mysqli_fetch_assoc($documentos))
                {
                ?>
                  <li>
                    <!--a href="descargar.php?id=<? echo $itemdoc['archivo']; ?>" target="_blank">Descargar</a-->
                     <a href="sjadmin/documento_tema/<? echo $itemdoc['archivo']; ?>" target="_blank">
                      <h4>
                        <?php echo $itemdoc['nombre'];?> 
                        <span class="fa fa-download fa-sm" aria-hidden="true"></span> 
                      </h4>
                    </a>
                  </li>

                <?
                }
                ?>
              </ul> 
           </div>
          </div>
      </div>
   
      <div class="container-fluid" style="background: #ededed;">

      <div class="container">
         <div class="row">

      <div class="col-md-12" style="background: #ededed;" >
      <br>
      <h3 class="text-primary"> QUIEN PUEDE AYUDARME </h3>
      <br>
      <div class="row">
        <?
        // sacar estudio juridicos

        $sql="SELECT `id`,`nombre`, `domicilio`, `email`, `telefono`, `abogados`, `titular`, `telefonotitular`, `responsable`,
        `web`, `provincia_id` FROM `estudiojuridico` WHERE `provincia_id`=$usuarioprovincia";
                            
        $juridicos=mysqli_query(conexion::obtenerInstancia(), $sql);
        while( $item = mysqli_fetch_assoc($juridicos))
        {
        
        ?>
      
        <div class="col-md-4">
          <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Estudio Juridico</span>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                  <li>
                    <?php  echo utf8_decode($item['nombre']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['domicilio']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['telefono']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['email']);?>
                 </li>
                   
                </ul>
            </div>
        </div>
       </div>
       <? } ?>

       <?
        // sacar escribania

        $sql="SELECT `id`,`nombre`, `domicilio`, `email`, `telefono`, `abogados`, `titular`, `telefonotitular`, `responsable`,
        `web`, `provincia_id` FROM `escribania` WHERE `provincia_id`=$usuarioprovincia";
                            
        $escribania=mysqli_query(conexion::obtenerInstancia(), $sql);
        while( $item = mysqli_fetch_assoc($escribania))
        {
        
        ?>
      
        <div class="col-md-4">
          <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Escribania</span>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                  <li>
                    <?php  echo utf8_decode($item['nombre']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['domicilio']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['telefono']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['email']);?>
                 </li>
                   
                </ul>
            </div>
        </div>
       </div>
       <? } ?>

       <?
        // sacar estudio juridicos
        $sql="SELECT `id`,`nombre`, `domicilio`, `email`, `telefono`, `contadores`, `titular`, `telefonotitular`, `responsable`,
        `web`, `provincia_id` FROM `est_contable` WHERE `provincia_id`=$usuarioprovincia";
                            
        $contable=mysqli_query(conexion::obtenerInstancia(), $sql);
        while( $item = mysqli_fetch_assoc($contable))
        {
        
        ?>
      
        <div class="col-md-4">
          <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Estudio Contable</span>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                  <li>
                    <?php  echo utf8_decode($item['nombre']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['domicilio']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['telefono']);?>
                 </li>
                 <li>
                    <?php  echo utf8_decode($item['email']);?>
                 </li>
                   
                </ul>
            </div>
        </div>
       </div>
       <? } ?>
      
      </div>
      <br>
    </div>
  </div>
</div>
  </div> <!-- 3recuadro-->

 
    <div class="col-md-12" >
      <br>
        <div class="text-center">

          <a href="https://www.facebook.com/sharer/sharer.php?u=https://demo.servijus.com.ar/consulta.php?tema=<? echo $id_tema; ?>" class="btn btn-social-icon btn-facebook" ><i class="fa fa-facebook"></i></a>

          <a  class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>
        
          <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://demo.servijus.com.ar/consulta.php?tema=<? echo $id_tema; ?>/&amp;title=<? echo $item['titulo']; ?>&amp;summary=" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
        
          <a href="https://twitter.com/intent/tweet?text=<? echo $item['titulo']; ?>" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
        
        </div>
         <p>Recomenda el tema consultado a tus amigos.</p>
         <br>
    </div>
   



    <div class="container">
      <div class="row">
        <br>
       
    <div class="col-md-12">
       <h3 class="text-primary"> CONSULTAS RELACIONADAS CON EL TEMA </h3>
       <br>
      <div class="row">
        <?
          $relacion_tema="SELECT `id`,`tema_id`,`tema_rela_id` FROM `tema_relacionado` WHERE tema_id=$id_tema";
          $listado=mysqli_query(conexion::obtenerInstancia(), $relacion_tema);
          while( $item = mysqli_fetch_assoc($listado))
          {
            $rela_tema=$item['tema_rela_id']; 
            $temas="SELECT                        
                        subcategoria.`titulo` AS sc_titulo,
                        subcategoria.`id` AS sc_id,
                        tema.`foto` AS foto,
                        tema.`id` AS tema_id,
                        tema.`titulo` AS titulo,
                         tema.`resumen` AS resumen,  
                        tema.`habilitada` AS habilitada 
                        FROM
                            `subcategoria`
                            INNER JOIN `tema` 
                                ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                                    where  tema.`id`='$rela_tema' and tema.`habilitada`='SI' 
                                      order by tema.`orden` asc";

            $listado1=mysqli_query(conexion::obtenerInstancia(), $temas);
            while( $item1 = mysqli_fetch_assoc($listado1))
            {
            ?>
              <div class="col-md-3">
              <div class="card mb-3 box-shadow">
                 <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>" >
                    <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item1['foto'];?>" class="card-img-top" alt="nombre" >
                 </a>
                  <div class="card-body">
			    		<h4><a href="consulta.php?tema=<?php echo $item1['tema_id'];?>"><?php echo utf8_decode($item1['titulo']);?></a></h4>
			    		<p class="text-justify"><?php echo utf8_decode($item1['resumen']);?></p>
                      <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                    <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>" >Ver más ...</a>
                  </div>
                </div>
              </div>
             </div>
            </div>  
            <?
            } 
           }  
          ?>
        </div>
<?
  }//fin while
  }//fin buscar tema
?>
</div>
</div>
<br>
</div>
<?  
}
}  
include ("pie.php");
?>
