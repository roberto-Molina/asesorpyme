<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");
$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';
?>
<br>
<div class="acceso espacio gris">
<div class="container">
<h2>Mi Cuenta</h2>
<hr>

<?php
  ////////////////* verificar persona habilitada o activa*//////////////////////
  /*$datos="SELECT
          persona_pyme.`email` AS email,
          persona_pyme.`estado` AS estado,
          persona_pyme.`nombre` AS nombre,
          provincia.`nombre` AS provincia,
          provincia.`id` AS provincia_id
          FROM
              `provincia`
              INNER JOIN `persona_pyme` 
                  ON (`provincia`.`id` = `persona_pyme`.`provincia_id`)
                   where persona_pyme.`id`='$ID'";*/
    $datos="SELECT
          persona.`email` AS email,
          persona.`estado` AS estado,
          persona.`nombre` AS nombre,
          provincia.`nombre` AS provincia,
          provincia.`id` AS provincia_id,
          persona.`suscripcion` AS suscripcion
          FROM
              `provincia`
              INNER JOIN `persona` 
                  ON (`provincia`.`id` = `persona`.`provincia_id`)
                   where persona.`id`='$ID'";               
                   
$listado=mysqli_query(conexion::obtenerInstancia(), $datos);
while( $item = mysqli_fetch_assoc($listado))
{

?>

<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#datos">Mis Datos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#clave">Mi Clave</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#suscripcion">Mi Membresía</a>
  </li>
</ul>

<div class="tab-content" id="myTabContent">

  <div id="datos" class="tab-pane fade show active">
   <br>
    <!--h4>Mis Datos</h4-->

    <form class="form-horizontal" >
     <input type="hidden" name="id_suscriptor" id="id_suscriptor" value="<? echo $ID; ?>" /> 

     <div class="form-group">
     <label class="control-label col-sm-4" for="email">Email :</label>
     <div class="col-sm-6">
       <label class="form-control" > <?echo utf8_encode($item['email']); ?> </label>
     </div>
    </div> 
   
    <div class="form-group">
     <label class="control-label col-sm-2" for="email">Nombre :</label>
     <div class="col-sm-10">
      <input type="texto" class="form-control" id="nombre" name="nombre" value="<?echo utf8_encode($item['nombre']); ?>">
     </div>
    </div>

    
    <div class="form-group">
     <label class="control-label col-sm-2" for="pwd">Provincia :</label>
     <div class="col-sm-10"> 
       <select class="form-control" name="provincia" id="provincia" tabindex="1" required >
            <option>Seleccionar ...</option>
            <?php
            $sql="select id, nombre from provincia";
            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
            foreach($provincias as $p)
            {
              if ($item['provincia_id']==$p['id'])
              {
                echo "<option value='".$p['id']."' selected > ". utf8_encode($p['nombre'])." </option>";
              }
             else echo "<option value='".$p['id']."'> ". utf8_encode($p['nombre'])."</option>";
           }
        ?>
        </select>
      </div>
    </div>

    <div class="form-group">
     <div class="col-lg-offset-2 col-lg-10">
      <button id="guardar_datos" class="boton mt-3"><i class="fa fa-floppy-o"></i> Guardar</button>
    </div>
  </div>

</form>

</div> <!--tab datos-->

<div id="clave" class="tab-pane fade show ">
    <br>
    <p>Cambiar Mi Clave</p>
    <form class="form-horizontal" >
   
    <!--div class="form-group">
     <label class="control-label col-sm-2" > Clave Actual :</label>
     <div class="col-sm-10">
      <input type="texto" class="form-control" id="actual" name="actual" >
     </div>
    </div-->

    <div class="form-group">
     <label class="control-label col-sm-2" > Clave Nueva :</label>
     <div class="col-sm-10">
      <input type="texto" class="form-control" id="nueva" name="nueva" >
     </div>
    </div>
    
    <div class="form-group">
     <label class="control-label col-sm-2" > Confirme Clave :</label>
     <div class="col-sm-10">
      <input type="texto" class="form-control" id="repite" name="repite" >
     </div>
    </div>
  
    <div class="form-group"> 
     <div class="col-sm-offset-2 col-sm-10">
      <button id="cambiar" class="boton mt-3"><i class="fa fa-floppy-o"></i> Cambiar </button>
     </div>
    </div>

</form>
  </div><!-- tab clave-->
  
  <div id="suscripcion" class="tab-pane fade show ">
   <br>
    <h3>Datos de mi Membresía</h3>
    <br>
    <?php
            $sql="select suscripcion,fecha_ingreso, estado from persona where id=$ID";
            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);
            foreach($provincias as $p)
            {
                 
             echo "<label> Registro : '".$p['estado']."'</label> ";
             echo "<br>";
             echo "<label> Fecha de Ingreso : '".$p['fecha_ingreso']."'</label> ";
             echo "<br>";
             echo"<hr>";
            }
            
            /* antes if ($p['estado']=='Habilitado')
             {

              $sql="SELECT
                    plan.`nombre` AS plan,
                    suscripcion.`fecha_ingreso` AS fecha_ingreso,
                    suscripcion.`fecha_fin` AS fecha_fin
                    FROM
                        `plan`
                        INNER JOIN `suscripcion` 
                            ON (`plan`.`id` = `suscripcion`.`plan_id`)
                            where persona_pyme_id=$ID";
              $suscripcion = mysqli_query(conexion::obtenerInstancia(), $sql);;
              foreach($suscripcion as $p)
              {
               echo "<label> Plan de Suscripción : '".$p['plan']."'</label> ";
               echo "<br>";
               echo "<label> Fecha desde : '".$p['fecha_ingreso']."'</label> ";
               echo "<br>";
               echo "<label> Fecha hasta  : '".$p['fecha_fin']."'</label> ";
              echo "<br>";
             echo"<hr>";
              }

             }*/
             if ($suscripcion=='SI')
             {
              $sql="SELECT
              persona.`fecha_ingreso` AS fecha_ingreso,
              persona.`suscripcion` AS suscripcion,
              persona.`estado` AS persona_estado,
              plan.`nombre` AS plan_nombre,
              persona_plan.`estado` AS persona_plan_estado,
              persona_plan.`fecha_fin` AS persona_plan_fecha_fin,
              persona_plan.`fecha_ingreso` AS persona_plan_fecha_ingreso
              FROM
                  `persona_plan`
                  INNER JOIN `persona` 
                      ON (`persona_plan`.`persona_id` = `persona`.`id`)
                  INNER JOIN `plan` 
                      ON (`persona_plan`.`plan_id` = `plan`.`id`)
                      WHERE persona.`id`=$ID";

                $planes = mysqli_query(conexion::obtenerInstancia(), $sql);
                foreach($planes as $p)
                {
                   echo "<label><strong>Membresía :</strong> '".$p['plan_nombre']."'</label> ";
                   echo "<br>";
                   //date_format($date, 'Y-m-d H:i:s');
                   echo "<label> <strong>Fecha desde :</strong> '".date("d-m-Y",strtotime($p['persona_plan_fecha_ingreso'] )) ."'</label> ";
                   echo "<br>";
                   echo "<label> <strong>Fecha hasta  :</strong> '".date("d-m-Y",strtotime($p['persona_plan_fecha_fin']))."'</label> ";
                   echo "<br>";
                   echo"<hr>";

                  }
              }
               else echo 'No Tienes Suscripciones Activas.';
             
        ?>
   <br>
   <hr>
   <a href=""> Cancelar Membresía </a>
   
  </div><!-- tab suscripcion-->

</div> <!-- fin tab-->

<?
}//fin del while


?>
<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="_js/popper.min.js"></script>
<script src="_bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<script language="javascript">

$(document).ready(function(){

  // guardar datos
  $("#guardar_datos").click(function(evento)
  {
   evento.preventDefault();
   nombre=$("#nombre").val();
   provincia=$("#provincia").val();
   id_suscriptor=$("#id_suscriptor").val();

    $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: 'actualizar_datos.php', 
        data: {nombre: nombre, provincia:provincia, id_suscriptor: id_suscriptor},
        success: function(data){
          alert(data);
          recargar();
        }//fin success
      });//fin ajax//*/
  });


   // 2 cambiar clave
  $("#cambiar").click(function(evento)
  {
   evento.preventDefault();
   id_suscriptor=$("#id_suscriptor").val();
   nueva=$("#nueva").val();
   repetida=$("#repite").val();

   if (nueva == repetida)
   { 
    $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: 'cambiarclave.php', 
        data: {clave_nueva: nueva, clave_conf:repetida, id_suscriptor: id_suscriptor},
        success: function(data){
          alert(data);
          recargar();
        }//fin success
      });//fin ajax//*/
  }
  else alert('Las contraseñas no son iguales.')
  });//fin click
  

  $("#cancelar").click(function(evento){
           window.location="perfil.php";
  });//fin cancelar

  function recargar()
    {
      //window.location.reload()
    window.location="perfil.php";
    }
});

</script>
<?php
//include ("pie.html");
?>