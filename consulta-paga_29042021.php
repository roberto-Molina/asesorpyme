<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");

$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';

if( isset($_GET['tema']) && !empty($_GET['tema']) )
{   
  $id_tema=htmlspecialchars($_GET['tema'],ENT_QUOTES,'UTF-8');

  // registrar estadistica
  $fechahora = date("Y-m-d H:i:s");
  $reg="INSERT INTO `e_tema_pyme`
            (`usuario_id`,`fechahora`,`tema_id`)
            VALUES ('$ID','$fechahora','$id_tema')";
  $registrado=mysqli_query(conexion::obtenerInstancia(), $reg);

  $temas="SELECT
            tema.`contenido_principal` AS contenido_principal,
            tema.`contenido_secundario` AS contenido_secundario,
            tema.`contenido_destacado` AS contenido_destacado,
            tema.`foto` AS foto,
            tema.`recomendacion` AS recomendacion,
            tema.`relacion_temas` AS relacion_temas,
            tema.`requisitos` AS requisitos,
            tema.`resumen` AS resumen,
            tema.`lectura` AS lectura,
            tema.`titulo` AS titulo,
            tema.`tipo` AS tipo,
            tema.`tramite` AS tramite,
            subcategoria.`categoria_id` AS categoria_id,
            subcategoria.`id` AS subcategoria_id,
            subcategoria.`titulo` AS subcategoria,
            categoria.`id` AS categoria_id,
            categoria.`titulo` AS categoria
            FROM
                `tema`
                  INNER JOIN `subcategoria` 
                    ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                    INNER JOIN `categoria` 
			ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
                    WHERE `tema`.`id`=$id_tema";
    $listado=mysqli_query(conexion::obtenerInstancia(), $temas);
    while( $item = mysqli_fetch_assoc($listado))
    {
      $contenido_principal=$item['contenido_principal'];
      $contenido_secundario=$item['contenido_secundario'];
      $contenido_destacado=$item['contenido_destacado'];
      $foto=$item['foto'];
      $recomendacion=$item['recomendacion'];
      $relacion_temas=$item['relacion_temas'];
      $requisitos=$item['requisitos'];
      $resumen=$item['resumen'];
      $lectura=$item['lectura'];
      $subcategoria_id=$item['subcategoria_id'];
      $subcategoria=$item['subcategoria'];
      $titulo=$item['titulo'];
      $tramite=$item['tramite'];
      $categoria_id=$item['categoria_id'];
      $categoria=$item['categoria'];
      
    }
?>

<div class="consulta-header">
	<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $foto;?>" alt="" class="d-block w-100"/>
</div>

<div class="consulta-contenido espaciomin grisoscuro">
	<div class="container">
		
		<div class="row breadcrumbs">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-8">
				<a href="subcategoria.php?cat=<? echo $categoria_id?>" class="pill pillazul mr-1">
					<? echo "&larr;&nbsp;&nbsp;".$categoria;?>
				</a>

				<!--a href="tema.php?subcat=<?php echo $item['id'];?>&cat=<?php echo $id_cat;?>" class="pill pillazul mr-1">
					<? echo "&larr;&nbsp;&nbsp;".$subcategoria;?>
				</a-->
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
				<h1>
					<?php echo utf8_decode($titulo);?>
				</h1>
				<h2 class="mt-3">
					<?php echo utf8_decode($resumen);?>
				</h2>
				<br>
				<a class="pill pillblanco mr-1">Tiempo estimado de lectura: </a> <?php echo utf8_decode($lectura);?>

			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plananual comprar-consulta">
					<h4>
						Para acceso a lectura total,<br>obtené tu Membresía
					</h4>
					<h5>
						<!-- $250 -->
                        BENEFICIOS
					</h5>
					<ul>
						<li><span>Al obtener tu Membresía tendrás acceso a<br>todo el contenido de la plataforma</span></li>
						<li><span>Acceso de lectura a todas las consultas<br>jurídicas, contables, fiscales y de gestión</span></li>
						<li>Consultas ilimitadas a través del formulario<br>de consultas de la plataforma on demand</li>
						<li>Descarga ilimitadas de Contratos Tipos<br>Cartas Documentos y Telegramas Laborales</li>
						<li>Descarga ilimitadas de Notas, Contestaciones,<br>Presentaciones y Formularios tipos</li>
					</ul>

					<?php 
					// si esta registrado 
					if (!is_null($ID))
					 {
					?>
					  <a id="consulta<?echo $id_tema;?>" mp-mode="dftl" href="https://www.mercadopago.com.ar/checkout/v1/redirect?pref_id=96772022-aae07c10-f74b-4fe7-9637-12a08b7e6919" name="MP-payButton" class='orange-ar-m-ov-aron'>Pagar Consulta</a>
<script type="text/javascript">
(function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x =
document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
</script>	

					<?
					} 
					else 
                    {

					 ?>
					 <h4>Si ya eres Miembro de Servijus</h4>
					 <br>
					<a href="iniciar-sesion.php" class="boton">Inicia Sesión</a>
					<?
                    }
					?>

				</div>
			</div>
		</div>

	</div>
</div>

<div class="espacio gris">
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Si aún no eres Miembro</h3>
				<h2 class="mb-4">
					Elegí una de nuestras membresías y<br>disfrutá de todos los beneficios.
				</h2>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan planmensual">
					<h4>
						Membresía Mensual
					</h4>
					<h5>
						<!-- $250 -->
					<?  
                  $sql="SELECT valor FROM plan where nombre='Mensual Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    <h4>
                        <ul>
                            <li><span>Valor Mensual Bonificado</span></li>
                            <li><span>VALOR A PAGAR x 1/Mes $ 2850</span></li>
                            <li><span>VALOR REAL $ 3000</span></li>
                        </ul>
                    </h4>
					</h5>
					<ul>
						<li><span>Adquirila ahora con una Bonificación<br> del 5% en el valor de la cuota</span></li>
					</ul>
					
					<script
                    src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                    data-preference-id="96772022-9decb9a4-8f4b-443b-bae5-ea073bb7a762">
                    </script>
					<br>
					<br>
					<a href="https://www.servijus.com.ar/consultas" target="_blank" >Membresía para Cámaras Empresarias</a><br>
                    
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plansemestral">
					<h4>
						Membresía Semestral
					</h4>
					<h5>
							<!-- $250 -->
					<?  
                  $sql="SELECT valor FROM plan where nombre='Semestral Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    <h4>
                        <ul>
                            <li><span>Valor Mensual Bonificado</span></li>
                            <li><span>VALOR A PAGAR X 6/meses $ 15300</span></li>
                            <li><span>VALOR REAL $ 18000</span></li>
                        </ul>
                    </h4>
					</h5>
					<ul>
						<li><span>Adquiri 6 meses con una Bonificación<br> del 15% en el valor de la cuota</span></li>
						<li><span>MEMBRESIA MÁS REQUERIDA<br>POR NUESTROS CLIENTES</span></li>
					</ul>
					
					<script
                    src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                    data-preference-id="96772022-97276d24-0839-4277-ba11-f09fd193cf7d">
                    </script>
					<br>
					<br>
					<a href="https://www.servijus.com.ar/consultas" target="_blank" >Membresía para Cámaras Empresarias</a><br>
                    
				</div>
			</div>
			
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				<div class="plan plananual">
					<h4>
						Membresía Anual
					</h4>
					<h5>
						<?php  
                  $sql="SELECT valor FROM plan where nombre='Anual Pyme'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $sql);
                  while( $item = mysqli_fetch_assoc($listado))
                  {
                    echo '$ '.$item['valor'];
                  }?>
                    <h4>
                        <ul>
                            <li><span>Valor Mensual Bonificado</span></li>
                            <li><span>VALOR A PAGAR X 12/meses $ 27000</span></li>
                            <li><span>VALOR REAL $ 36000</span></li>
                        </ul>
                    </h4>
					</h5>
					<ul>
						<li><span>Adquiri 12 meses con una Bonificación<br>del 25% en el el valor de la cuota</span></li>
					</ul>
					<script
                    src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                    data-preference-id="96772022-2dce7bbd-c29e-4a4d-b24c-5bb2a8dce5f0">
                    </script>
					<br>
					<br>
					<a href="https://www.servijus.com.ar/consultas" target="_blank" >Membresía para Cámaras Empresarias</a><br>

				</div>
			</div>
	    </div>
    </div>
</div>

<div class="relativo espacio blanco border-top">
    <img class="redondel" src="_img/icono-relacionadas.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Consultas Jurídicas On Demand</h3>
				<h2 class="mb-4">Otros temas consultados</h2>
			</div>
		</div>
		
		<div class="row mt-5">
		<?
         $relacion_tema="SELECT `id`,`tema_id`,`tema_rela_id` FROM `tema_relacionado` WHERE tema_id=$id_tema";
         $listado=mysqli_query(conexion::obtenerInstancia(), $relacion_tema);
         while( $item = mysqli_fetch_assoc($listado))
          {
            $rela_tema=$item['tema_rela_id']; 
            $temas="SELECT                        
                        subcategoria.`titulo` AS sc_titulo,
                        subcategoria.`id` AS sc_id,
                        tema.`foto` AS foto,
                        tema.`id` AS tema_id,
                        tema.`titulo` AS titulo,
                         tema.`resumen` AS resumen, 
                         tema.`lectura` AS lectura, 
                         tema.`tipo` AS tipo, 
                        tema.`habilitada` AS habilitada 
                        FROM
                            `subcategoria`
                            INNER JOIN `tema` 
                                ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                                    where  tema.`id`='$rela_tema' and tema.`habilitada`='SI' 
                                      order by tema.`orden` asc";

            $listado1=mysqli_query(conexion::obtenerInstancia(), $temas);
            while( $item1 = mysqli_fetch_assoc($listado1))
            {
           
			if ($suscripcion =='SI')
            {
            ?>
              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                  <div class="caja subcategoria">
                      <figure class="limit">
                        <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                        <img src="sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
                        </a>
                      </figure>
                    
                      <div class="subcategoria-tipo">
                      </div>
                  
                      <div class="subcategoria-texto">
                        <h4><a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                          <?php echo utf8_decode($item1['titulo']);?></a>
                        </h4>
                        <p>
                          <?php echo utf8_decode($item1['resumen']);?>
                        </p>
                      </div>
                </div>
          </div>
           <?
            }

        if ($suscripcion == 'NO' )
        {
          if ($item1['tipo']=='PAGO')
           {
           ?>  	
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
			<div class="caja subcategoria">
				<figure class="limit">
					<a href="consulta-paga.php?tema=<?php echo $item1['tema_id'];?>">
					<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
				</a>
				</figure>

				<div class="subcategoria-tipo">
                    <div class="pill amarillo">
                      Pago
                   </div>
                </div>

				<div class="subcategoria-texto">  
				<h4>
		    		<a href="consulta-paga.php?tema=<?php echo $item1['tema_id'];?>">
		    			<?php echo utf8_decode($item1['titulo']);?></a>
		    	</h4>
		    	<p>
		    		<?php echo utf8_decode($item1['resumen']);?>
				</p>
		    	</div>
		    </div>
		    </div>	 
               <?
             }
              else
                {
                ?>	
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
						<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
					</a>
					</figure>

					<div class="subcategoria-tipo">
	                    <div class="pill verde">
	                      Gratis
	                   </div>
	                </div>

					<div class="subcategoria-texto">  
					<h4>
			    		<a href="consulta.php?tema=<?php echo $item1['tema_id'];?>"><?php echo utf8_decode($item1['titulo']);?></a>
			    	</h4>
			    	<p>
			    		<?php echo utf8_decode($item1['resumen']);?>
					</p>
			    	</div>
		    	</div>
		    </div>	 
            <? 
      		}// fin else
          }// fin if suscripcion
      }//fin del while
      }//fin del while

         ?>
			
			
		</div>
	</div>
</div>

<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function()
  {
    
    $("a[id^='mensualp']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(7,5);
        vplan_id=4;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_pyme_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='semestralp']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(9,5);
        vplan_id=6;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

    $("a[id^='anualp']").click(function(evento)
    {
    	evento.preventDefault();
        vid = this.id.substr(5,5);
        vplan_id=5;
        console.log(vid);
        $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: 'solicitar_suscripcion.php',
                data: { persona_id: vid, plan: vplan_id},
                success: function(data){
                    if (data)
                    {
                      //alert(data);
                       //location.reload(true);
                       console_log(data);
                    }
                }
                });//fin ajax
    });//fin

 });	
</script>

<?php
}
//include("contadortemas.php");
include("pie.html");
?>