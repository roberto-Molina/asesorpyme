<?php
$_SESSION['time'] = $_GET['hora_activo'];
if((time() - $_SESSION['time']) > 20)
{
				header('location: index.php');
			}
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
?>

<script language="javascript">
 //setTimeout(refrescar, 10);
 function refrescar(){
 window.location.href = "index.php";
 }
</script>
<?php
include ("menu.php");
$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';
if( isset($_GET['tema']) && !empty($_GET['tema']) )
{   
  $id_tema=htmlspecialchars($_GET['tema'],ENT_QUOTES,'UTF-8');

  // registrar estadistica
  $fechahora = date("Y-m-d H:i:s");
  $reg="INSERT INTO `e_tema_pyme`
            (`usuario_id`,`fechahora`,`tema_id`)
            VALUES ('$ID','$fechahora','$id_tema')";
  $registrado=mysqli_query(conexion::obtenerInstancia(), $reg);

  $temas="SELECT
            tema.`contenido_principal` AS contenido_principal,
            tema.`contenido_secundario` AS contenido_secundario,
            tema.`contenido_destacado` AS contenido_destacado,
            tema.`foto` AS foto,
            tema.`recomendacion` AS recomendacion,
            tema.`relacion_temas` AS relacion_temas,
            tema.`requisitos` AS requisitos,
            tema.`resumen` AS resumen,
            tema.`lectura` AS lectura,
            tema.`titulo` AS titulo,
            tema.`tipo` AS tipo,
            tema.`tramite` AS tramite,
            tema.`autor` AS autor,
            tema.`libro` AS libro,
            subcategoria.`categoria_id` AS categoria_id,
            subcategoria.`id` AS subcategoria_id,
            subcategoria.`titulo` AS subcategoria,
            categoria.`id` AS categoria_id,
            categoria.`titulo` AS categoria
            FROM
                `tema`
                  INNER JOIN `subcategoria` 
                    ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                    INNER JOIN `categoria` 
			ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
                    WHERE `tema`.`id`=$id_tema";
    $listado=mysqli_query(conexion::obtenerInstancia(), $temas);
    while( $item = mysqli_fetch_assoc($listado))
    {
      $contenido_principal=$item['contenido_principal'];
      $contenido_secundario=$item['contenido_secundario'];
      $contenido_destacado=$item['contenido_destacado'];
      $foto=$item['foto'];
      $recomendacion=$item['recomendacion'];
      $relacion_temas=$item['relacion_temas'];
      $requisitos=$item['requisitos'];
      $resumen=$item['resumen'];
      $lectura=$item['lectura'];
      $subcategoria_id=$item['subcategoria_id'];
      $subcategoria=$item['subcategoria'];
      $titulo=$item['titulo'];
      $tramite=$item['tramite'];
      $autor=$item['autor'];
      $libro=$item['libro'];
      $categoria_id=$item['categoria_id'];
      $categoria=$item['categoria'];
    }
?>

<div class="consulta-header">
	<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $foto;?>" alt="" class="d-block w-100"/>
</div>

<div class="consulta-contenido espaciomin gris">
	<div class="container">
		
		<div class="row breadcrumbs">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-8">
				<!--a href="#" class="pill pillblanco mr-1">&larr;&nbsp;&nbsp;Mi Familia( falta link categoria )</a-->
				<a href="subcategoria.php?cat=<? echo $categoria_id?>" class="pill pillblanco mr-1">
					<? echo "&larr;&nbsp;&nbsp;".$categoria;?>
				</a>
				<a href="tema.php?subcat=<?php echo $subcategoria_id;?>&cat=<?php echo $categoria_id;?>" class="pill pillazul mr-1">
					<? echo "&larr;&nbsp;&nbsp;".$subcategoria;?>
				</a>
			</div>
			<div class="text-right col-xl-6 col-lg-6 col-md-6 col-sm-6 col-4">
			<?
             if ($item['tipo']=='Pago')
             {
               ?>
                 <a href="#" class="pill pillamarillo">Pago</a>
               <?
             }
              else
                {
               ?>
                 <a href="#" class="pill pillverde">Gratis</a>
               <?
             }
             ?>
				
			</div>
		</div>
		
		<div class="row mt-5">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h1>
					<?php echo utf8_decode($titulo);?>
				</h1>
				<h2 class="mt-3">
					<?php echo utf8_decode($resumen);?>
				</h2>
				<br>
				<a class="pill pillblanco mr-1">Tiempo estimado de lectura: </a> <?php echo utf8_decode($lectura);?>

				<hr class="mt-5"/>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
				<h3>
					<?php echo utf8_decode($contenido_principal);?>
				</h3>
				<hr class="hrcorto"/>
				<p><?php echo utf8_decode($contenido_secundario);?></p>


        <? ///// ingresado por JOSE LUIS /////
        ?>
	
	     <?
       $sql="SELECT
                tema.`titulo` AS tema_titulo,
                tema.`id` AS tema_id,
                video.`archivo` AS archivo,
                video.`id` AS id,
                video.`nombre` AS nombre
                FROM
                    `video`
                    INNER JOIN `tema` 
                        ON (`video`.`tema_id` = `tema`.`id`)
                                    where `tema`.`id`=$id_tema";
                                  
        $videos=mysqli_query(conexion::obtenerInstancia(), $sql);
         
        while( $itemvideo = mysqli_fetch_assoc($videos))
          {
          ?>
 		<div class="row mt-5">  
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <img src="https://www.asesoronline.servijus.com.ar/sjadmin/video_tema/<?php echo $itemvideo['archivo'];?>" class="d-block w-100" alt="infografia" >
            </div>
        </div>
           <!--video src="img/<?php echo $itemvideo['video'];?>" controls> </video-->
          <?
          }
         
         ?>
			
			
				<hr class="hrcorto"/>
				<h4>
					¿Qué debo tener en cuenta?
				</h4>
				<p>
					<?php echo utf8_decode($requisitos);?>
				</p>
				
				<hr class="hrcorto"/>
				<h4>
					Bibliografía utilizada
				</h4>
				<p>
					<?php echo utf8_decode($libro);?>
				</p>

				<!--ol type="a">
					<li>Que los dos integrantes sean mayores de edad (mayores de 18 años);</li>
					<li>Que los convivientes no sean parientes;</li>
					<li>Que no estén unidos por matrimonio o por otra unión convivencial inscrita;</li>
					<li>Que la relación sea pública, notoria, estable y de carácter permanente;</li>
					<li>Que los convivientes mantengan una convivencia notoria durante un período no inferior a dos años.</li>
				</ol>
				<p>
					Te mostramos un modelo de Pacto de Convivencia, que puedes adaptarlo a tus necesidades y derechos que quieras proteger.
				</p-->

				<? /*if (documento) asociado*/

                $documento="select nombre,archivo from documento WHERE `tema_id`=$id_tema";
			    $listado=mysqli_query(conexion::obtenerInstancia(), $documento);
			    if ($listado)
			    {
			        while( $item = mysqli_fetch_assoc($listado))
			    	{
			      	
			      	$archivo_documento=$item['archivo'];
			      	$nombre_documento=$item['nombre'];
			      	?>
			      	<a href="https://www.asesoronline.servijus.com.ar/sjadmin/documento_tema/<? echo $archivo_documento?>" class="boton mt-4" target="_blank">
					<img class="mr-3" src="_img/icono-descargar.png" alt="" width="9"/>
					<? echo $nombre_documento?>				
					</a>
				<br>
				<br>
			      	<?
			    	
			    }
			  } 
				 ?>
			</div>
			
			     <div class="offset-xl-1 col-xl-4 offset-lg-1 col-lg-4 col-md-12 col-sm-12 col-12">

			    	<div class="widget widgetazul mt-5">
				    	<img class="redondel" src="_img/icono-express.png" alt="" width="60"/>
				    	<h5 class="d-block w-100 text-center">
				    		GUIA RAPIDA
					    </h5>
					    <p><? echo utf8_decode($tramite);?></p>
				    </div>

			        	<br>
			        	<br>
			    	<h4>
					Consulta escrita por:
			    	</h4>
			        	<p>
				    	<?php echo utf8_decode($autor);?>
				        </p>
			        	<br>
			        	<br>

			    	<div class="widget">
				    	<img class="redondel" src="_img/icono-bandera.png" alt="" width="60"/>
				    	<p>
				    	<? echo utf8_decode($contenido_destacado);?>
					    </p>
				    </div>

				    <div class="mt-5 mb-5 sharethis-inline-share-buttons">
	            	    <?
            		    // falta las redes sociales
  
                        }
          
                         ?>
                         <a href="https://wa.me/?text=https%3A//www.asesorpyme.servijus.com.ar/consulta.php?tema=<? echo $id_tema; ?>" target="-blank"><img src="_img/i-whatsapp.png" alt="" width="40"/><span> </span></a>
                         <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.asesorpyme.servijus.com.ar/consulta.php?tema=<? echo $id_tema; ?>>" target="-blank"><img src="_img/i-facebook.png" alt="" width="40"/><span> </span></a> 
                         <a href="https://twitter.com/intent/tweet?text=https%3A//www.asesorpyme.servijus.com.ar/consulta.php?tema=<? echo $id_tema; ?>" target="-blank"><img src="_img/i-gorjeo.png" alt="" width="40"/><span> </span></a>
                         <a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A//www.asesorpyme.servijus.com.ar/consulta.php?tema=<? echo $id_tema; ?>&title=<? echo $titulo; ?>&summary=&source=>" target="-blank"><img src="_img/i-linkedin.png" alt="" width="40"/><span> </span></a> 
          			</div>

          		</div>	
		</div>
	</div>
</div>



<div class="relativo espacio blanco border-top">
	<img class="redondel" src="_img/icono-relacionadas.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Consultas Jurídicas On Demand</h3>
				<h2 class="mb-4">Otros temas consultados</h2>
			</div>
		</div>
		<div class="row mt-5">
		<?
          
           $relacion_tema="SELECT `id`,`tema_id`,`tema_rela_id` FROM `tema_relacionado` WHERE tema_id=$id_tema";
          

          $listado=mysqli_query(conexion::obtenerInstancia(), $relacion_tema);
          
          while( $item = mysqli_fetch_assoc($listado))
          {
            
            $rela_tema=$item['tema_rela_id']; 
            
            $temas="SELECT                        
                        subcategoria.`titulo` AS sc_titulo,
                        subcategoria.`id` AS sc_id,
                        tema.`foto` AS foto,
                        tema.`id` AS tema_id,
                        tema.`titulo` AS titulo,
                         tema.`resumen` AS resumen, 
                         tema.`lectura` AS lectura, 
                         tema.`tipo` AS tipo, 
                        tema.`habilitada` AS habilitada 
                        FROM
                            `subcategoria`
                            INNER JOIN `tema` 
                                ON (`subcategoria`.`id` = `tema`.`subcategoria_id`)
                                    where  tema.`id`='$rela_tema' and tema.`habilitada`='SI' 
                                      order by tema.`orden` asc";

            $listado1=mysqli_query(conexion::obtenerInstancia(), $temas);
            while( $item1 = mysqli_fetch_assoc($listado1))
            {
           
			if ($suscripcion =='SI')
        {
        ?>
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
              <div class="caja subcategoria">
                   <figure class="limit">
                    <a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                         <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
                    </a>
                   </figure>
                  <!--div class="subcategoria-tipo">
                  </div-->
              
                  <div class="subcategoria-texto">
                    <h4><a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
                      <?php echo utf8_decode($item1['titulo']);?></a>
                    </h4>
                    <p>
                      <!--?php echo utf8_decode($item1['resumen']);?-->
                    </p>
                  </div>
            </div>
      </div>
       <?
        }

        if ($suscripcion == 'NO' )
        {
          if ($item1['tipo']=='PAGO')
            {
           ?>  	
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
			<div class="caja subcategoria">
				<figure class="limit">
					<a href="consulta-paga.php?tema=<?php echo $item1['tema_id'];?>">
					<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
				</a>
				</figure>

				<div class="subcategoria-tipo">
                    <div class="pill amarillo">
                      Pago
                   </div>
                </div>

				<div class="subcategoria-texto">  
				<h4>
		    		<a href="consulta-paga.php?tema=<?php echo $item1['tema_id'];?>">
		    			<?php echo utf8_decode($item1['titulo']);?></a>
		    	</h4>
		    	<p>
		    		<!--?php echo utf8_decode($item1['resumen']);?-->
				</p>
		    	</div>
		    </div>
		    </div>	 
               <?
             }
              else
                {
                ?>	
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<a href="consulta.php?tema=<?php echo $item1['tema_id'];?>">
						<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item1['foto'];?>" alt="" class="d-block w-100 grow"/>
					</a>
					</figure>

					<div class="subcategoria-tipo">
	                    <div class="pill verde">
	                      Gratis
	                   </div>
	                </div>

					<div class="subcategoria-texto">  
					<h4>
			    		<a href="consulta.php?tema=<?php echo $item1['tema_id'];?>"><?php echo utf8_decode($item1['titulo']);?></a>
			    	</h4>
			    	<p>
			    		<!--?php echo utf8_decode($item1['resumen']);?-->
					</p>
			    	</div>
		    	</div>
		    </div>	 
            <? 
      		}// fin else
          }// fin if suscripcion
      }//fin del while
      }//fin del while

         ?>
			
			
		</div>
	</div>
</div>


<?php
include("estudios.php");
include("pie-consulta.html");
?>
