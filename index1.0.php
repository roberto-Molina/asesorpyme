<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php")
?>

<div class="destacado">
  <div class="row">
    <div class="col-md-12">

	        	
      <img src="img/FOTO-PYME.jpg" alt="" class="d-block w-100"/> 
      </br>
      </br>
      
      		<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">SERVICIOS Y CONSULTORIA ON DEMAND</h3>
				<h1 class="mb-4">Servijus agrega valor a Tu Negocio</h1>
			</div>
    </div>
  </div>  
</div><!-- fin container-->

</br>
</br>
</div><!-- fin container--> 

<div class="container">
  <div class="row">
      
    <div class="col-md-4">
      <div class="card individual">
            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="_img/DIRECTOR_GENERAL.jpg"  data-holder-rendered="true">
            </a>  
                 <div class="card-body">
                 <p class="card-text"> </p>
		        		<h4 class="mb-4">DIRECCION GENERAL</h4>
                        <h3 class="volanta">CONSULTORIA ON DEMAND</h3>
                        <p class="text-justify"><?php echo "Integrar el conocimiento con la pasion por lo que haces, te hara lider de tu equipo y un gran referente en tu Pyme. Toma las mejores decisiones estando bien informado y actualizado.";?></p>
                       <!--button type="button"  onclick="location.href='contratos/nuevo.php';" >Imprimi Contratos</button-->
                      <a href="" class="" target="">  </a>
                 </div>
             </div>
         </div>

    <div class="col-md-4">
      <div class="card mb-4 box-shadow">
            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="_img/RELACIONES_LABORALES.jpg"  data-holder-rendered="true">
            </a>
                 <div class="card-body">
                 <p class="card-text"></p>
		        		<h4 class="mb-4">RELACIONES LABORALES</h4>
                        <h3 class="volanta">CONTENIDO LEGAL ON DEMAND</h3>
                        <p class="text-justify"><?php echo "Conoce las infinitas relaciones que la empresa genera con los empleados y con los sindicatos. Encuentra en este seccion toda la asistencia legal para tomar las decisiones correctas.";?></p>
                         <a href="" class="" target="">  </a>
                 </div>
             </div>
         </div>
 
    <div class="col-md-4">
      <div class="card mb-4 box-shadow">
            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="_img/RECURSOS_HUMANOS.jpg"  data-holder-rendered="true">
            </a>
                 <div class="card-body">
                 <p class="card-text"></p>
 		        		<h4 class="mb-4">RECURSOS HUMANOS</h4>
                        <h3 class="volanta">CONTENIDO LEGAL ON DEMAND</h3>
                        <p class="text-justify"><?php echo "La eleccion correcta al momento de la seleccion de personal, hara de tu organizacion una estructura solida. Tus RRHH son un activo que debe estar en sintonia con tus vision y valores";?></p>
                         <a href="" class="" target="">  </a>
 
                </div>
              </div>
            </div>
         </div>  

</div><!-- fin container-->

<div class="container">
  <div class="row">
      
    <div class="col-md-4">
      <div class="card mb-4 box-shadow">
            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="_img/COMUNICACION.jpg"  data-holder-rendered="true">
            </a>  
                 <div class="card-body">
                 <p class="card-text"> </p>
		        		<h4 class="mb-4">COMUNICACION</h4>
                        <h3 class="volanta">CONSULTORIA ON DEMAND</h3>
                        <p class="text-justify"><?php echo "La imagen corporativa de tu Pyme, es el fiel reflejo de lo que comunicas, de la identidad empresaria de tu negocio y del comportamiento interno y externo de todos tus recursos.";?></p>
                       <!--button type="button"  onclick="location.href='contratos/nuevo.php';" >Imprimi Contratos</button-->
                      <a href="" class="" target="">  </a>
                 </div>
             </div>
         </div>

    <div class="col-md-4">
      <div class="card mb-4 box-shadow">
            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="_img/EMPRESAS_FAMILIARES.jpg"  data-holder-rendered="true">
            </a>
                 <div class="card-body">
                 <p class="card-text"></p>
		        		<h4 class="mb-4">EMPRESAS FAMILIARES</h4>
                        <h3 class="volanta">CONTENIDO LEGAL ON DEMAND</h3>
                        <p class="text-justify"><?php echo "Una familia empresaria que profesionaliza sus miembros, supera todos los obstaculos que se presentan entre las generaciones que la dirigen. Encuentra en esta seccion como hacerlo.";?></p>
                         <a href="" class="" target="">  </a>
                 </div>
             </div>
         </div>
 
    <div class="col-md-4">
      <div class="card mb-4 box-shadow">
            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="_img/NEGOCIACION.jpg"  data-holder-rendered="true">
            </a>
                 <div class="card-body">
                 <p class="card-text"></p>
 		        		<h4 class="mb-4">NEGOCIACION Y VENTAS</h4>
                        <h3 class="volanta">CONSULTORIA ON DEMAND</h3>
                        <p class="text-justify"><?php echo "Negociar y vender, son procesos muy similares pero no lo mismo. Encuentra en esta seccion la forma de obtener beneficios sin afectar las relaciones con tus clientes y proveedores.";?></p>
                         <a href="" class="" target="">  </a>
 
                </div>
              </div>
            </div>
         </div>  

</div><!-- fin container-->
<br>
<br>
<br>
 <script type="text/javascript">
 $(document).ready(function()
  {
    v_boton ="INDEX Pyme";
    $.post("registrar_boton_pyme.php", {boton: v_boton}, function(mensaje) {
        });//fin post
  });
</script>




<div class="container">
  <div class="row">
 
    <div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">NO TE MUEVAS DE TU EMPRESA</h3>
				<h1 class="mb-4">Disfruta de todos nuestros <br> servicios en un click</h1>
			</div>
 
                <div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
             
               <a class="boton" href="planes.php" role="button">SUSCRIBITE AHORA</a>
      
              
    </div>
  </div>
</div>      
<br>
<br>
<br>


<?
include ("pie.html");
?>      

   <!--p>servicios gratuitos</p-->




<!-- TABLA PRECIOS-->
<!-- <div class="container mb-5 mt-5">
    <div class="pricing card-deck flex-column flex-md-row mb-3">
        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">DIRECCION GENERAL</span>
            <div class="card-body pt-0">
            <img src="_img/DIRECTOR_GENERAL.jpg" alt="" class="d-block w-100"/>
            </div>
       </div>

        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">RELACIONES LABORALES</span>
            <div class="card-body pt-0">
            <img src="_img/RELACIONES_LABORALES.jpg" alt="" class="d-block w-100"/>
            </div>
       </div>

        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">RECURSOS HUMANOS</span>
            <div class="card-body pt-0">
            <img src="_img/RECURSOS_HUMANOS.jpg" alt="" class="d-block w-100"/>
            </div>
       </div>
    </div>
</div>

<div class="container mb-5 mt-5">
    <div class="pricing card-deck flex-column flex-md-row mb-3">
        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">COMUNICACION Y MARKETING</span>
            <div class="card-body pt-0">
            <img src="_img/COMUNICACION.jpg" alt="" class="d-block w-100"/>
            </div>
       </div>

        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">EMPRESAS FAMILIARES</span>
            <div class="card-body pt-0">
            <img src="_img/EMPRESAS_FAMILIARES.jpg" alt="" class="d-block w-100"/>
            </div>
       </div>

        <div class="card card-pricing text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">CAPACITACION A MEDIDA</span>
            <div class="card-body pt-0">
            <img src="_img/CAPACITACION_A_MEDIDA.jpg" alt="" class="d-block w-100"/>
            </div>
       </div>
    </div>
</div>
<!-- FIN-->



<!--<div class="container">
     <table class="table">
     <thead>
       <tr>
             <th>Prestaciones</th>
             <th>Plan Mensual</th>
             <th>Plan Semestral</th>
             <th>Plan Anual</th>
             </tr>
           </thead>
           <tbody>
            <tr>
              <td>Bonificación dos cuotas</td>
              <td><i class="fa fa-times" style="font-size: 30px; color: red;"></i></td>
              <td><i class="fa fa-times" style="font-size: 30px; color: red;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              
              <td>Bonificación 20% en cuota</td>
              <td><i class="fa fa-times" style="font-size: 30px; color: red;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
           <tr>
              <td>Acceso ilimitado a Consultas Juridicas</td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitadas de Contratos</td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitada de Telegramas Laborales</td>
               <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitada de Cartas Documentos</td>
               <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
            <tr>
              <td>Descarga ilimitadas de Notas y Formularios</td>
               <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
              <td><i class="fa fa-check" style="font-size: 30px; color: green;"></i></td>
            </tr>
    </tbody>
    </table>
     <hr>
</div>
<br>
<br>



  
