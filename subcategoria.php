<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");

if( isset($_GET['cat']) && !empty($_GET['cat']) )
{   
  
  $id_cat=htmlspecialchars($_GET['cat'],ENT_QUOTES,'UTF-8');
  
  ///////////////// registrar estadistica de categoria ////////////////////////
  $fechahora = date("Y-m-d H:i:s");
  $reg="INSERT INTO `e_categoria_pyme`
            (`usuario_id`,`fechahora`,`categoria_id`)
            VALUES ('$ID','$fechahora','$id_cat')";
  $registrado=mysqli_query(conexion::obtenerInstancia(), $reg);

  /////////////////// traer datos categorias //////////////////////////////
  $navegacion="SELECT categoria.`id` AS cat_id, categoria.`titulo` AS cat_titulo
  FROM `categoria` WHERE `categoria`.`id`=$id_cat";

  $listado=mysqli_query(conexion::obtenerInstancia(), $navegacion);
  while( $item = mysqli_fetch_assoc($listado))
    {
      $cat_id=utf8_decode($item['cat_id']);
      $cat_titulo=utf8_decode($item['cat_titulo']);
 
    }  
    
      $categorias="SELECT titulo from categoria WHERE `id`='$id_cat'";
     $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
	while( $item = mysqli_fetch_assoc($listado))
    {
      $nombre_categoria=$item['titulo'];
     }

?>

<div class="container">
    <div class="row">
        
        <div class="row mt-5 breadcrumbs">
		    <div cass="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<a href="index.php" class="pill pillblanco mr-1">&larr;&nbsp;&nbsp;Volver al Directorio</a>
			
					<!--a href="index.php?categorias=<?php echo $cat_id;?>&cat=<?php echo $cat_id;?>" class="pill pillblanco mr-1">
					<? echo "&larr;&nbsp;&nbsp;".$nombre_categoria;?>
				    </a-->

			
		    </div>
		</div>
	</div>
	    <div class="row mt-5">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		    	<h2 class="mb-4"><?php echo $nombre_categoria;?></h2>
			    <h3 class="volanta">Temas Generales de Consultas</h3>
			</div>
		</div>
		
	<div class="row mt-5">

    <?
    $categorias="SELECT subcategoria.`titulo` AS titulo, subcategoria.`id` AS id, subcategoria.`resumen` AS resumen, subcategoria.`foto` AS foto,
    subcategoria.`habilitada` AS habilitada FROM `subcategoria` INNER JOIN `categoria` ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
    WHERE `categoria`.`id`='$id_cat' AND subcategoria.`habilitada`='SI'  order by subcategoria.`orden` asc";

      $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
      while( $item = mysqli_fetch_assoc($listado))
          {
        ?>
        <div class="text-center col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="caja subcategoria">
                <figure class="limit">
                    <a href="tema.php?subcat=<?php echo $item['id'];?>&cat=<?php echo $id_cat;?>" >
                    <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top" alt="nombre" >
                    </a>
                </figure>
             
          <div class="subcategoria-tipo">
          </div>
          
                <div class="subcategoria-texto">
                  <h4><a href="tema.php?subcat=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a></h4>
                  <p class="text-justify"><?php echo utf8_decode($item['resumen']);?></p>
                
                          <a href="tema.php?subcat=<?php echo $item['id'];?>&cat=<?php echo $id_cat;?>"class ="boton botonazul">Ver Consultas</a>
					
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <?
             }
           }//fin del if
           else{
            // ver todas las categorias
            $categorias="SELECT subcategoria.`titulo` AS titulo, subcategoria.`id` AS id, subcategoria.`resumen` AS resumen, subcategoria.`foto` AS foto,
 subcategoria.`habilitada` AS habilitada FROM `subcategoria` INNER JOIN `categoria` ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
 WHERE subcategoria.`habilitada`='SI'  order by subcategoria.`orden` asc";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
           ?>
              <div class="col-md-3">
                <div class="card mb-3 box-shadow">

                <a href="tema.php?id=<?php echo $item['id'];?>" class="btn btn-primary btn-block"  role="button">
                    <img src="sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top" alt="nombre" >
                </a>

                <div class="card-body">
                  <p class="text-justify"><?php echo utf8_decode($item['resumen']);?></p>

                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     <a href="tema.php?id=<?php echo $item['id'];?>" class="btn btn-primary btn-block"  role="button">Ver más</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <?
             }
          }
          ?>
  </div><!--row-->
<br>
  			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-8">
  			    <a href="index.php" class="pill pillblanco mr-1">&larr;&nbsp;&nbsp;Volver al inicio</a>
            </div>
<br>
<br>
<br>
</div>
<?php
include ("pie.html");
?>

    