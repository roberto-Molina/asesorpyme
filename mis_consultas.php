<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
//$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 0;
?>
<div class="relativo espacio gris">
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Servicios Jurídicos Online</h3>
				<h2 class="mb-4">
					Mis Consultas Jurídicas 
				</h2>
			</div>
		</div>
		<div class="row mt-5">
		<?
       $temas="SELECT
				tema.`foto` AS foto,
                tema.`id` AS tema_id,
                tema.`titulo` AS titulo,
                tema.`resumen` AS resumen,  
                tema.`habilitada` AS habilitada,
                tema.`tipo` AS tipo,
                persona_tema.`id_persona` AS persona,
                persona_tema.`estado` AS estado
			FROM
			    `persona_tema`
			    INNER JOIN `tema` 
			        ON (`persona_tema`.`id_tema` = `tema`.`id`)
			         WHERE persona_tema.`id_persona`=$ID and persona_tema.`estado`='Habilitado';";
                                     
      $listado=mysqli_query(conexion::obtenerInstancia(), $temas);

      if ($listado)
      {

      while( $item = mysqli_fetch_assoc($listado))
      {
        
        ?>
        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
          <div class="caja subcategoria">
          <figure class="limit">
            <a href="consulta.php?tema=<?php echo $item['tema_id'];?>">
            <img src="sjadmin/imagenes/<?php echo $item['foto'];?>" alt="" class="d-block w-100 grow"/>
          </a>
          </figure>
          <div class="subcategoria-tipo">
          </div>
      
          <div class="subcategoria-texto">
            <h4><a href="consulta.php?tema=<?php echo $item['tema_id'];?>">
              <?php echo utf8_decode($item['titulo']);?></a>
            </h4>
            <p>
              <?php echo utf8_decode($item['resumen']);?>
            </p>
          </div>
        </div>
      	</div>
		  <?
      }//while
    } else { echo "No hay consultas para mostrar.";}
      ?>



      </div>
      </div>
	</div>
	<?php
include("pie.html");
?>