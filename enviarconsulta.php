<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");

if( isset($_POST['categoria_id']) && !empty($_POST['categoria_id']) )
{ 
   $categoria_id=$_POST['categoria_id'];
   $subcategoria_id=$_POST['subcategoria_id'];
   $correo=$_POST['correo'];
   $mensaje=$_POST['mensaje'];

   // validar si existe el email
   $sql="SELECT
        categoria.titulo AS categoria,
        subcategoria.titulo AS subcategoria
        FROM
            `categoria`
            INNER JOIN `subcategoria` 
                ON (`categoria`.`id` = `subcategoria`.`categoria_id`)
                WHERE `categoria`.`id` =$categoria_id and  `subcategoria`.`id`=$subcategoria_id ;";

   $consulta=mysqli_query(conexion::obtenerInstancia(), $sql);
   while( $item = mysqli_fetch_assoc($consulta))
            //foreach($categorias as $item)
            {
            $categoria=$item['categoria'];
            $subcategoria=$item['subcategoria'];
            }

   if(mysqli_num_rows($consulta) > 0)
   { 
       /*si hay email generar una clave nueva para enviar */
      /* $longitud = 8; // longitud del password  
       $password = substr(rand(),0,$longitud); */
      
       /*enviar email*/
        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        
        $from = "correo@servijus.com.ar";//envia
        
        $to = "urgencia.legal@servijus.com.ar";//el que recibe $email;
        
        $subject = "Consulta Urgente Enviada desde Asesor Pyme";
        $message ='
            <!DOCTYPE html>
            <html lang="es">
            <head>
                <title>Asesor Pyme - Consulta -</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link type="text/css" rel="stylesheet" href="css/bootstrap_mat.css">
            </head>
            <body>
            <div class="container">
             <table class="table" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
             <tr>
              <td aling="center" style="color:blue;">
              <h3>SERVIJUS Consultoría Pyme</h3>
              </td>
             </tr>
             <tr>
                <td bgcolor="#ffffff">
                   <p style="font-size: 16px">
                  Consulta Enviada por:
                  </p>
                  <p style="font-size: 18px; color: blue ; ">
                  '.$correo.'
                  </p>
                  
                </td>
              </tr>
             <tr>
                <td bgcolor="#ffffff">
                   <p style="font-size: 16px">
                  Categoria:
                  </p
                  <p style="font-size: 18px; color: blue ; ">
                  '.$categoria.'
                  </p>
                  
                </td>
              </tr>

              <tr>
                <td bgcolor="#ffffff">
                   <p style="font-size: 16px">
                  Subcategoria:
                  </p>
                  <br>
                  <p style="font-size: 18px; color: blue ; ">
                  '.$subcategoria.'
                  </p>
                  
                </td>
              </tr>

              <tr>
                <td bgcolor="#ffffff">
                   <p style="font-size: 16px">
                  Consulta:
                  </p>
                  <p style="font-size: 18px; color: blue ; ">
                  '.$mensaje.'
                  </p>
                </td>
              </tr>

            </table>
            </div>  
            </body>
            </html>';
            $headers = "From:" . $from;
            $headers .= "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
           
            mail($to,$subject,$message, $headers);
            
            echo "<script language=Javascript> alert('Su consulta fue enviada con exito. En breve estará disponible para su lectura. ¡REGISTRE GRATIS SU EMAIL!'); </script>";
                    echo "<script language=Javascript> location.href=\"index.php\"; </script>";
                    exit();
        }
        }
        else {
          ?>

          <div class="acceso espacio gris">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <h2 class="mb-4">
        Hacé Tu Consulta ahora!

        </h2>
      </div>
      <div class="text-right col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <span class="text-dark mr-3"><strong>¿Nuevo en Servijus?</strong></span>
        <a href="planes.php" class="boton">Obtené tu Membresia</a>
      </div>
    </div>
      <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <hr/>
      </div>
    </div>
    <div class="row mt-4">
      <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
            <h3 class="volanta">Para hacer una consulta, has tu pregunta en este formulario. <br>Para recibir detalles de tu consulta, obtené tu membresia. </h3>
             <form class="d-block mt-5" method="post"action="enviarconsulta.php" >

        <div class="form-group">
            <label for="exampleInputEmail1"><strong>Temas de Consultas</strong></label>
            <h3 class="volanta">(Seleccioná uno de los temas)</h3>

            <select class="form-control" name="categoria_id" id="categoria_id" autofocus tabindex="1" required >
             <option>Seleccionar ...</option>
                  <?
                  $categorias="SELECT * FROM categoria where habilitada='SI'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
                  while( $item = mysqli_fetch_assoc($listado))
                  //foreach($categorias as $item)
                  {
                  echo "<option value='".$item['id']."'> ". utf8_decode($item['titulo'])."</option>";
                  }
                  ?>
            </select>
        </div>
            
      <div class="form-group">
            <label for="exampleInputEmail1"><strong>Subtemas de Consultas</strong></label>
            <h3 class="volanta">(Seleccioná uno de los subtemas)</h3>
            <select class="form-control" name="subcategoria_id" id="subcategoria_id" tabindex="2" required >
          <div id="div_dinamico"></div>

        </select>
      </div>

      <div class="form-group">
            <label for="exampleInputEmail1"><strong>Escribe tu E-mail</strong> </label>
            <textarea name="correo" rows="1" cols="25"class="form-control" tabindex="3" required>  </textarea>
      </div>
      
      <div class="form-group">
            <label for="exampleInputEmail1"><strong>¿Cuál es tu pregunta?</strong> </label>
            <textarea name="mensaje" rows="4" cols="25"class="form-control" tabindex="4" required>  </textarea>
      </div>
       <div class="row mt-4">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
              <button class="botonHacerConsulta" type="submit">Envía tu Consulta</button>
          </div>
      </div>
     </form>
</div>
      <div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
        <img src="_img/consultar.jpg" alt="" class="d-block w-100"/>
        <br>
      </div>
    </div>
  </div>
</div>

<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function()
  {
   
  // 02/05/2019 llenar combo temas
  //buscar por tema
  $('#categoria_id').on('change',function()
  {
        vid = $(this).val()
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'lista_subcategoria.php',
          data: { cat: vid},
          success: function(data){
            if (data)
            {
                $('#subcategoria_id').html(data);
               //$('#div_dinamico').html(data);
            }
           }
       });//fin ajax
     });//fin    
 });
</script>
 <? 
  }
 
include("contadortemas.php");
//include("pie.html");
?> 