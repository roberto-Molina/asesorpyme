<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
//include("menu.php");
?>
<div id="nav">
	<nav class="container navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="index.php">
			<img src="_img/ASESORPYMES.jpg" alt="Servijus" width="240"/>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		
	</nav>
</div>
<div class="acceso espacio gris">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<h2 class="mb-4">
					Registrate para acceder a tu MEMBRESIA DE OBSEQUIO
				</h2>
			</div>
			<div class="text-right col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<span class="text-dark mr-3"><strong>¿Ya sos Miembro SERVIJUS?</strong></span>
				<a href="iniciar-sesion.php" class="boton botonazul">Inicia Sesión</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<hr/>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
		        
		        <h3 class="volanta">Para disfrutar de todos los servicios de la plataforma de consultas, <br>registra tu email y tu provincia. Revisa tu cuenta de e-mail, <br>te enviamos tu clave para que puedas Iniciar Sesion.</h3>

				<form class="d-block mt-5" id="form" method="post" action="nuevo.php">

				  <input type="hidden" name="plan" value="9">
				  <div class="form-group">
				    <label for="exampleInputEmail1"><strong>Correo electrónico:</strong></label>
				    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" autofocus>
				  </div>
				  <div class="form-group">
					<label for="exampleFormControlSelect1"><strong>Provincia:</strong></label>
					     <select class="form-control" name="provincia" id="provincia" tabindex="1" required >
			            <option>Seleccionar ...</option>
			            <?php
			            $sql="select id, nombre from provincia";
			            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
			              foreach($provincias as $item)
			              {
			                echo "<option value='".$item['id']."'> ". utf8_encode($item['nombre'])."</option>";
			              }
			        ?>
			        </select>
				  </div>
				  <button type="submit" class="boton mt-3">Registrate</button>
				</form>
				
			</div>
			<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
				<img src="_img/acerca.jpg" alt="" class="d-block w-100"/>
			</div>
		</div>
	</div>
</div>

<?php
include ("contadortemas.php");

?>
<footer id="footer" class="azuloscuro espaciomin">
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<a href="https://www.servijus.com.ar/" class="d-block mt-5">
					<img src="_img/servijus-blanco.png" alt="Servijus" width="120"/>
				</a>
			</div>
		</div>
	</div>
	<br>
</footer>
</body>
</html>