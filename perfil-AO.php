<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>

<div class="acceso espacio gris">
<div class="container">
	<h3 class="volanta">Mi Perfil</h3>
	<?php
  
 ////////* verificar persona habilitada o activa*//////////////////////
  $datos="SELECT
          persona_pyme.`email` AS email,
          persona_pyme.`estado` AS estado,
          persona_pyme.`nombre` AS nombre,
          provincia.`nombre` AS provincia,
          provincia.`id` AS provincia_id
          FROM
              `provincia`
              INNER JOIN `persona_pyme` 
                  ON (`provincia`.`id` = `persona_pyme`.`provincia_id`)
                   where persona_pyme.`id`='$ID'";
$listado=mysqli_query(conexion::obtenerInstancia(), $datos);
while( $item = mysqli_fetch_assoc($listado))
{
?>

	<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
	
				<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			    <li class="nav-item">
			    <a class="boton botonazul mb-2" id="pills-home-tab" data-toggle="pill" href="#pills-datos" role="tab" aria-controls="pills-home" aria-selected="true">Mis datos</a>
			    </li>
			    <li class="nav-item">
			    <a class="boton botonazul mb-2" id="pills-profile-tab" data-toggle="pill" href="#pills-contra" role="tab" aria-controls="pills-profile" aria-selected="false">Cambiar contraseña</a>
			    </li>
			    <li class="nav-item">
			    <a class="boton botonazul mb-2" id="pills-contact-tab" data-toggle="pill" href="#pills-suscri" role="tab" aria-controls="pills-contact" aria-selected="false">Mi suscripción</a>
			   </li>
			   </ul>
			</div> 
		</div>	


		<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<hr/>
			</div>
		</div>
			


  <div class="tab-content" id="pills-tabContent">
  
  <div class="tab-pane fade show active" id="pills-datos" role="tabpanel" aria-labelledby="pills-home-tab">

  	<div class="row">
  	<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">

  	<form class="d-block mt-4" >
    <input type="hidden" name="id_suscriptor" id="id_suscriptor" value="<? echo $ID; ?>" /> 

     <div class="form-group">
     <label for="email"><strong>Correo electrónico :</strong></label>
     <label class="form-control" > <?echo utf8_encode($item['email']); ?> </label>
     </div>
      
    <div class="form-group">
     <label for="email"><strong>Nombre :</strong></label>
      <input type="texto" class="form-control" id="nombre" name="nombre" value="<?echo utf8_encode($item['nombre']); ?>">
     </div>
        
    <div class="form-group">
     <label for="pwd"><strong>Provincia :</strong></label>
     <select class="form-control" name="provincia" id="provincia" tabindex="1" required >
            <option>Seleccionar ...</option>
            <?php
            $sql="select id, nombre from provincia";
            $provincias = mysqli_query(conexion::obtenerInstancia(), $sql);;
            foreach($provincias as $p)
            {
              if ($item['provincia_id']==$p['id'])
              {
                echo "<option value='".$p['id']."' selected > ". utf8_encode($p['nombre'])." </option>";
              }
             else echo "<option value='".$p['id']."'> ". utf8_encode($p['nombre'])."</option>";
           }
        ?>
        </select>
      </div>
      <button id="guardar_datos" class="boton mt-3"> Guardar</button>
   
		</form>
		</div>
		<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
				<img src="_img/contenido/perfil.jpg" alt="" class="d-block w-100"/>
		</div>	

  	</div>
  			

  </div>

  <div class="tab-pane fade" id="pills-contra" role="tabpanel" aria-labelledby="pills-profile-tab">
   <div class="row">
  		<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">

  		<form class="form-horizontal" >
   
        <div class="form-group">
   		  <label > <strong>Clave Nueva :</strong></label>
          <input type="texto" class="form-control" id="nueva" name="nueva" >
        </div>
    
        <div class="form-group">
         <label > <strong>Confirme Clave :</strong></label>
         <input type="texto" class="form-control" id="repite" name="repite" >
        </div>
  
      <button id="cambiar" class="boton mt-3"> Cambiar </button>
    
      </form>
		</div>
		<div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
				<img src="_img/contenido/perfil.jpg" alt="" class="d-block w-100"/>
		</div>	

  	</div>

  </div>
  <div class="tab-pane fade" id="pills-suscri" role="tabpanel" aria-labelledby="pills-contact-tab">
  	  <div class="row">
  	  	<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
  	  	<div class="form-group">
  		<?php
            /*$sql="SELECT
persona.`fecha_ingreso` AS fecha_ingreso,
persona.`suscripcion` AS suscripcion,
persona.`estado` AS persona_estado,
plan.`nombre` AS plan_nombre,
persona_plan.`estado` AS persona_plan_estado,
persona_plan.`fecha_fin` AS persona_plan_fecha_fin,
persona_plan.`fecha_ingreso` AS persona_plan_fecha_ingreso
FROM
    `persona_plan`
    INNER JOIN `persona` 
        ON (`persona_plan`.`persona_id` = `persona`.`id`)
    INNER JOIN `asesor_online`.`plan` 
        ON (`persona_plan`.`plan_id` = `plan`.`id`)
        WHERE persona.`id`=$ID";*/

           $sql="select fecha_ingreso, estado,suscripcion from persona_pyme where id=$ID";
           
           $respuesta = mysqli_query(conexion::obtenerInstancia(), $sql);
            foreach($respuesta as $p)
            {
             echo "<label><strong> Registro :</strong> '".$p['estado']."'</label> ";
             echo "<br>";
             echo "<br>";
             echo "<label><strong> Fecha de Ingreso : </strong>'".date("d-m-Y",strtotime($p['fecha_ingreso']))."'</label> ";
             echo "<br>";
             echo "<br>";
             echo"<hr>";
             echo "<br>";
            
            
             if ($p['suscripcion']=='SI')
             {
               
               $sql="SELECT
              persona_pyme.`fecha_ingreso` AS fecha_ingreso,
              persona_pyme.`suscripcion` AS suscripcion,
              persona_pyme.`estado` AS persona_pyme_estado,
              plan.`nombre` AS plan_nombre,
              persona_pyme.`estado` AS persona_pyme_estado,
              persona_pyme.`fecha_fin` AS persona_pyme_fecha_fin,
              persona_pyme.`fecha_ingreso` AS persona_pyme_fecha_ingreso
              FROM
                  `persona_pyme`
                  INNER JOIN `persona_pyme` 
                      ON (`persona_pyme`.`persona_pyme_id` = `persona_pyme`.`id`)
                  INNER JOIN `plan` 
                      ON (`persona_pyme`.`plan_id` = `plan`.`id`)
                      WHERE persona_pyme.`id`=$ID";

                $planes = mysqli_query(conexion::obtenerInstancia(), $sql);
                foreach($planes as $p)
                {

               echo "<label><strong> Plan de Suscripción :</strong> '".$p['plan_nombre']."'</label> ";
               echo "<br>";
               //date_format($date, 'Y-m-d H:i:s');

               echo "<label> <strong>Fecha desde :</strong> '".date("d-m-Y",strtotime($p['persona_pyme_fecha_ingreso'] )) ."'</label> ";
               echo "<br>";
                echo "<label> <strong>Fecha hasta  :</strong> '".date("d-m-Y",strtotime($p['persona_pyme_fecha_fin']))."'</label> ";
              echo "<br>";
             echo"<hr>";

              ?>
               <a href=""> Cancelar Suscripción </a>
            <?
              }
              }
               else echo 'No Tienes Suscripciones Activas.';
             }
             
        ?>
    	

     </div> 
 </div>
     <div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-6 col-md-6 col-sm-12 col-12">
			<img src="_img/contenido/perfil.jpg" alt="" class="d-block w-100"/>
		</div>	

  	</div>

</div>
</div>
</div>
</div>

<?php
}

?>

<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script language="javascript">

$(document).ready(function(){

  // guardar datos
  $("#guardar_datos").click(function(evento)
  {
   evento.preventDefault();
   nombre=$("#nombre").val();
   provincia=$("#provincia").val();
   id_suscriptor=$("#id_suscriptor").val();
    $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: 'actualizar_datos.php', 
        data: {nombre: nombre, provincia:provincia, id_suscriptor: id_suscriptor},
        success: function(data){
          alert(data);
          recargar();
        }//fin success
      });//fin ajax//*/
  });


   // 2 cambiar clave
  $("#cambiar").click(function(evento)
  {
   evento.preventDefault();
   id_suscriptor=$("#id_suscriptor").val();
   nueva=$("#nueva").val();
   repetida=$("#repite").val();

   if (nueva == repetida)
   { 
    $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: 'cambiarclave.php', 
        data: {clave_nueva: nueva, clave_conf:repetida, id_suscriptor: id_suscriptor},
        success: function(data){
          alert(data);
          recargar();
        }//fin success
      });//fin ajax//*/
  }
  else alert('Las contraseñas no son iguales.')
  });//fin click
  

  $("#cancelar").click(function(evento){
           window.location="perfil.php";
  });//fin cancelar

  function recargar()
    {
      //window.location.reload()
    window.location="perfil.php";
    }
});

</script>
<?
include("contadortemas.php");
//include("pie.html");
?>