<?php
include("sjadmin/bd/conexion.php");
include("cabecera.html");
include("menu.php");
?>

<div class="espacio gris">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Acerca</h3>
				<h2 class="mb-4">Servijus</h2>
				<h4 class="mt-5 titulo-sans">
					SERVIJUS es una consultora integrada por distintos profesionales que brinda servicios y consultoría online a particulares, pymes, empresas familiares, asociaciones civiles, fundaciones, escuelas, colegios, e institutos de formación.</p>
                    <p>Los servicios de asesoría legal y contable, de escribanía, los servicios inmobiliarios y de capacitación de alta dirección están disponibles las 24 horas, los 365 días, mediante distintas plataformas digitales on demand. Ello permite a todos nuestros clientes, consultar sus derechos en cualquier momento y lugar, para tomar decisiones correctas en el momento indicado.</p>
				</h4>
				<hr class="hrcorto mt-5 mb-5"/>
				<img src="_img/acerca.jpg" alt="" class="d-block w-100"/>
				<hr class="hrcorto mt-5 mb-5"/>
				<p>Nuestro Isotipo, representa dos principios jurídicos fundamentales en las relaciones humanas. Uno basado en la igualdad que dice: <strong>"todos somos iguales ante la ley"</strong>, y el otro basado en nuestra propia desición personal, que dice: <strong>"primero en el tiempo, primero en el derecho"</strong>.</p>
				<p>El servicio de consultoría on demand, descanza en este último principio, ya que permite conocer nuestros derechos cuando y donde lo necesitamos, ya sea desde la comodidad de nuestro hogar o desde nuestro trabajo. Quien conoce primero sus derechos, puede ejercerlos primero en el tiempo.</p>
				
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="caja cajanegra blanco border text-center p-5 mb-3">
					<h4>
						Visión
					</h4>
					<p>
						Ser la mayor consultora global que brinde servicios de consultoría online a la pequeña y mediana empresa, a profesionales y a particulares, asesorando cada momento de la vida empresarial, profesional y personal.
					</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="caja cajanegra blanco border text-center p-5">
					<h4>
						Misión
					</h4>
					<p>
						Brindar servicios de consultoría en forma digital, poniendo online toda la información legal en forma ordenada y accesible a nuestros clientes, para que disponga de ella en cualquier momento de su vida, y en tiempo oportuno.
					</p>
				</div>
			</div>
		</div>
		<div class="row mt-5">
		</div>
	</div>
</div>

<div class="relativo espacio blanco border-top">
	<img class="redondel" src="_img/icono-valores.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h2 class="mb-4">Valores</h2>
			</div>
		</div>
		<div class="row mt-5">
			
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
				<div class="caja text-center">
					<h4>Compromiso Social</h4>
					<p>
						Asumimos un compromiso con las generaciones futuras, mediante prácticas empresariales medioambientales sostenibles. Usamos plataformas digitales de asesoramiento en línea.
					</p>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
				<div class="caja text-center">
					<h4>Respeto Ambiental</h4>
					<p>
						Nuestros servicios se brindan de forma remota, se consumen desde el hogar o desde el trabajo, sin necesidad desplazamiento. Nosotros no usamos papel y vos no usas combustible.
					</p>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
				<div class="caja text-center">
					<h4>Profesionalidad</h4>
					<p>
						Estamos comprometidos con una sólida ética profesional, y con el cumplimiento de la legislación aplicable en cada uno de los países de la región. Somos sujetos comprometido con los derechos individuales.
					</p>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php
include("contadortemas.php");
include("pie.html");
?>