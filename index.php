<?php
include("sjadmin/bd/conexion.php");
include ("cabecera.html");
include ("menu.php");

$suscripcion = ((isset($SUSCRI)) && (!empty($SUSCRI))) ? $SUSCRI : 'NO';

if($_GET["mensaje"] == "fallo") {
         ?>
                                <div class="alert bg-danger text-white fade show rounded-0" role="alert">
                                    <div class="container d-flex">
                                        <div class="alert__icon mr-3">
                                            <i class="fas fa-minus-circle"></i>
                                        </div>
                                        <div class="align-self-center mr-3">Hubo algun error procesando el pago. Intente nuevamente</div>
                                        <div class="ml-auto">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
  

?>
<div class="destacadop">
    
      <img src="img/FOTO-PYME2.jpg" alt="" class="d-block w-100"/> 
        
</div><!-- fin container-->


<div class="espacio gris">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h2 class="mb-4">Asistencia Legal Online</h2>
			</div>
		</div>    
    
   <div id="buscador" class="container h-100">
      <div class="d-flex justify-content-center h-100">
        <div class="searchbar">
          <input class="input-busqueda" type="text" name="" placeholder="Buscar el tema de tu consulta..." id="valor_buscado">
          <a href="#" class="search_icon"><i class="fas fa-search"></i></a>
        </div>
      </div>

      <div id="listado_temas">
        
      </div>
    </div> 
<br>
<br>
<div class="container">
    <div class="row">
      		<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">Plataforma de Información, Consultas Legales y Servicios On Demand</h3>
				<h4 class="mb-4">Una solución para tu empresa, ¡estés donde estés!</h4> 
				<br>
			</div>

     <?php
          $categorias="SELECT * FROM categoria where habilitada='SI' and pyme='si' order by orden asc";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
    ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
				    <div class="caja categoria">
                        <a href="subcategoria.php?cat=<?php echo $item['id'];?>" class="categoria-imagen"> 
                        <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top"  alt="nombre" >
                        </a>
						    <div class="categoria-texto">
						    <h4><a href="subcategoria.php?cat=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a></h4>
                            <p class="text-justify"><?php echo utf8_decode($item['resumen']);?>
                            </p>
						 <a href="subcategoria.php?cat=<?php echo $item['id'];?>" class ="boton botonazul">Ver Temas</a>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">

                        </div>
                    </div>
                    </div>
                   </div>
                </div>

          <?
             }//fin del while
          ?>
  
</div><!--row-->

        <br>
        <br>
        <br>
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta"> <font color="DODGERBLUE"> Que tu vida privada no afecte tu negocio, conoce tus derechos. </font></h3>
				<h2 class="mb-4"> <font color="DARKBLUE">Soluciones Legales en Temas Personales</font></h2>
			</div>
        <br>
<hr>
  <div class="row">
  <?php
         /* verificar persona habilitada o activa*/
          $categorias="SELECT * FROM categoria where habilitada='SI' and personal='si' order by orden asc";
          $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
          while( $item = mysqli_fetch_assoc($listado))
          {
           ?>
          <div class="text-center col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
             <div class="caja subcategoria">
                <figure class="limit">
                    <a href="subcategoria.php?cat=<?php echo $item['id'];?>" >
                      <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['foto'];?>" class="card-img-top"  alt="nombre" >
                    </a>
                </figure>
          <div class="subcategoria-tipo">
          </div>
 
          <div class="subcategoria-texto">
                   <h4><a href="subcategoria.php?cat=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a></h4>
                      <p class="text-justify"><?php echo utf8_decode($item['resumen']);?>
                      </p>
                   
						 <a href="subcategoria.php?cat=<?php echo $item['id'];?>" class ="boton botonazul">Ver Temas</a>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">

                        </div>
                    </div>
                    </div>
                   </div>
                </div>
          <?
             }//fin del while
          ?>
  </div>
  
    </div> 
</div><!--row-->

<br>
<br>



<div class="relativo espacio blanco border-top">
	<img class="redondel" src="_img/icono-recientes.png" alt="" width="60"/>
	<div class="container">
		<div class="row">
			<div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 class="volanta">PLATAFORMA PYME ON DEMAND</h3>
				<h2 class="mb-4">Ultimas consultas realizadas</h2>
			</div>
		</div>
		<div class="row mt-5">

		<?php
		
            // corregir tema de pyme que aparece entre los de asesoronine. ////

             //$ultimos_temas="SELECT id, foto as fotoprincipal, titulo, resumen, tipo  FROM tema order by id desc";
             
             $ultimos_temas="SELECT
                            tema.id AS id,
                            tema.foto AS fotoprincipal,
                            tema.titulo AS titulo,
                            tema.resumen AS resumen,
                            tema.tipo AS tipo
                            FROM
                                `subcategoria`
                                INNER JOIN `categoria` 
                                    ON (`subcategoria`.`categoria_id` = `categoria`.`id`)
                                INNER JOIN `tema` 
                                    ON (`tema`.`subcategoria_id` = `subcategoria`.`id`)
                                    WHERE categoria.pyme='si' or categoria.personal='si' order by id desc;";
             
             $listado=mysqli_query(conexion::obtenerInstancia(), $ultimos_temas);

             $i=1;
         while(( $item = mysqli_fetch_assoc($listado) and ($i<=4)))
          {
             $i++;
            if ($suscripcion =='SI')
            {
                
          
            ?>
              <div class="text-center col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
              <div class="caja subcategoria">
              <figure class="limit">
                <a href="consulta.php?tema=<?php echo $item['id'];?>">
                <img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['fotoprincipal'];?>" alt="" class="d-block w-100 grow"/>
              </a>
              </figure>
              
              <div class="subcategoria-tipo">
              </div>
          
              <div class="subcategoria-texto">
                <h4><a href="consulta.php?tema=<?php echo $item['id'];?>">
                  <?php echo utf8_decode($item['titulo']);?></a>
                </h4>
                <p>
                  <!--?php echo utf8_decode($item['resumen']);? -->
                </p>
              </div>
            </div>
          </div>
       <?php
        }

        if ($suscripcion == 'NO' )
        {
          if ($item['tipo']=='PAGO')
            {
           ?>  	
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
			<div class="caja subcategoria">
				<figure class="limit">
					<a href="consulta-paga.php?tema=<?php echo $item['id'];?>">
					<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['fotoprincipal'];?>" alt="" class="d-block w-100 grow"/>
				</a>
				</figure>

				<div class="subcategoria-tipo">
                    <div class="pill amarillo">
                      Pago
                   </div>
                </div>

				<div class="subcategoria-texto">  
				<h4>
		    		<a href="consulta-paga.php?tema=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a>
		    	</h4>
		    	<p>
		    		<!--?php echo utf8_decode($item['resumen']);?-->
				</p>
		    	</div>
		    </div>
		    </div>	 
               <?
             }
              else
                {
                ?>	
               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="caja subcategoria">
					<figure class="limit">
						<a href="consulta.php?tema=<?php echo $item['id'];?>">
						<img src="https://www.asesoronline.servijus.com.ar/sjadmin/imagenes/<?php echo $item['fotoprincipal'];?>" alt="" class="d-block w-100 grow"/>
					</a>
					</figure>

					<div class="subcategoria-tipo">
	                    <div class="pill verde">
	                      Gratis
	                   </div>
	                </div>

					<div class="subcategoria-texto">  
					<h4>
			    		<a href="consulta.php?tema=<?php echo $item['id'];?>"><?php echo utf8_decode($item['titulo']);?></a>
			    	</h4>
			    	<p>
			    		<!--?php echo utf8_decode($item['resumen']);?-->
					</p>
			    	</div>
		    	</div>
		    </div>	 
            <? 
      		}// fin else
          }// fin if suscripcion
      }//fin del while
         ?>
		</div>  
	</div>
</div>


<!-- servicios empresariales -->

<!--div id="cifras" class="azul espaciomin"-->
	    <div class="text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            	<img class="redondel" src="_img/ayuda-utiliza.png" alt="" width="60"/>
            	<br>
            	<br>
            	<br>
				<h2 class="mb-4"> <a>Servicios Empresariales</a></h2>
			</div>
<br>
<div class="container">
  <div class="row">
 
    <div class="col-md-6">
      <div class="row">

    <div class="col-md-6">
        <div class="caja subcategoria">
            <div class="card mb-4 box-shadow">
              <a href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=20" target="">
                  <img class="card-img-top" src="_img/shoping.jpg" >
              </a>
                <div class="card-body">
                     <p class="card-text"></p>
                        <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <h4><a href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=20" target="">Centros Comerciales</a></h4>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
 
    <div class="col-md-6">
        <div class="caja subcategoria">
            <div class="card mb-4 box-shadow">
              <a href="agenda.php" target=""> 
                  <img class="card-img-top" src="img/agenda.jpg" >
                     </a>  
                    <div class="card-body">
                    <p class="card-text"></p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <h4><a href="agenda.php" target="">Agenda Empresaria</a></h4>
                      </div>
                    </div>
              </div>
            </div>
        </div>
      </div>

    <div class="col-md-6">
        <div class="caja subcategoria">
            <div class="card mb-4 box-shadow">
              <a href="https://capacitacion.servijus.com.ar/index.php" target="_blank">
                  <img class="card-img-top" src="img/capacitacion_a_medida.jpg" >
              </a>
              <div class="card-body">
                <p class="card-text"></p>
                  <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <h4><a href="https://capacitacion.servijus.com.ar/index.php" target="_blank">Capacitación a Medida</a></h4>
                      </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
 
      <div class="col-md-6">
        <div class="caja subcategoria">
        <div class="card mb-4 box-shadow">
              <a href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=11" target=""> 
                  <img class="card-img-top" src="img/MI-ASOCIACION.jpg" >
              </a>  
              <div class="card-body">
                    <p class="card-text"></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <h4><a href="https://www.asesorpyme.servijus.com.ar/subcategoria.php?cat=11" target="">Asociaciones Civiles</a></h4>
                      </div>
                    </div>
              </div>
            </div>
        </div>
      </div>
     </div>
    </div>


    <div class="col-md-6">
      <form method="post" action="enviarconsulta.php" >
        <div class="col-md-12">
            <br>
            <h4><a>Formulario de Consultas</a></h4> 
            <br>
            <label for="exampleInputEmail1"><a>Seleccioná un tema de consulta</a></label>

            <select class="form-control" name="categoria_id" id="categoria_id"  tabindex="1" required >
             <option>Seleccionar ...</option>
                  <?
                  $categorias="SELECT * FROM categoria where habilitada='SI'";
                  $listado=mysqli_query(conexion::obtenerInstancia(), $categorias);
                  while( $item = mysqli_fetch_assoc($listado))
                  //foreach($categorias as $item)
                  {
                  echo "<option value='".$item['id']."'> ". utf8_decode($item['titulo'])."</option>";
                  }
                  ?>
            </select>
           </div>
              
            <div class="col-md-12">
            <br>
                <label for="exampleInputEmail1"><a>Seleccioná un subtema de consulta</a></label>
                <select class="form-control" name="subcategoria_id" id="subcategoria_id" tabindex="2" required >
                <div id="div_dinamico"></div>
                </select>
            </div>

            <div class="col-md-12">
            <br>
                <label for="exampleInputEmail1"><a>Escribe tu E-mail</a> </label>
                <input type="email" name="correo" class="form-control" tabindex="3" required/>
            </div>

            <div class="col-md-12">
            <br>
            <label for="exampleInputEmail1"><a>¿Cuál es tu pregunta o tramite?</a> </label>
            <textarea name="mensaje" rows="4" cols="25"class="form-control" tabindex="4" required>  </textarea>
        </div>
        <br>
        <br>
        <div class="col-md-12 align-items-center">
                <button class="boton" type="submit">Enviar Consulta</button>
<br>
<br>
        </div>
       </form>

   
    </div>

  </div>  
</div><!-- fin container-->

<br>
<br>

<script src="sjadmin/vendor/jquery/jquery.js"></script>
<script src="sjadmin/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function()
  
    {
   
  // 02/05/2019 llenar combo temas
  //buscar por tema
  $('#categoria_id').on('change',function()
  {
        vid = $(this).val()
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'lista_subcategoria.php',
          data: { cat: vid},
          success: function(data){
            if (data)
            {
                $('#subcategoria_id').html(data);
               //$('#div_dinamico').html(data);
            }
           }
       });//fin ajax
     });//fin  
     
      $('#valor_buscado').on('keyup',function()
     {
    
      var MIN_LENGTH = 3;
      var texto = $("#valor_buscado").val();
      //alert(vkeyword);
      if (texto.length >= MIN_LENGTH)
      {
       //$('#botonMensaje').attr("disabled", false);
       $.get("buscarportema.php", { textotema: texto } )
        .done(function( data ) {
           //$("#capa").show();
           $('#listado_temas').html(data);
        });
      }

    });



 });
</script>
</div><!--content-->
<?php
include ("contadortemas.php");
include ("pie.html");
?>